<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page isELIgnored="false" %>
<select name="shape" id="shape" class="form-control" required
	<c:if test="${actionMode == 'view' || param.actionMode == 'view'}">disabled</c:if>>
	<option value="-1" selected="selected">Select Shape</option>
	<option value="Round"
		<c:if test="${stock.shape == 'Round'}"> selected="selected" </c:if>>Round</option>
	<option value="Pear"
		<c:if test="${stock.shape == 'Pear'}"> selected="selected" </c:if>>Pear</option>
	<option value="Emerald"
		<c:if test="${stock.shape == 'Emerald'}"> selected="selected" </c:if>>Emerald</option>
	<option value="Marquise"
		<c:if test="${stock.shape == 'Marquise'}"> selected="selected" </c:if>>Marquise</option>
	<option value="Oval"
		<c:if test="${stock.shape == 'Oval'}"> selected="selected" </c:if>>Oval</option>
	<option value="Radiant"
		<c:if test="${stock.shape == 'Radiant'}"> selected="selected" </c:if>>Radiant</option>
	<option value="Princess"
		<c:if test="${stock.shape == 'Princess'}"> selected="selected" </c:if>>Princess</option>
	<option value="Trilliant"
		<c:if test="${stock.shape == 'Trilliant'}"> selected="selected" </c:if>>Trilliant</option>
	<option value="Heart"
		<c:if test="${stock.shape == 'Heart'}"> selected="selected" </c:if>>Heart</option>
	<option value="European Cut"
		<c:if test="${stock.shape == 'European Cut'}"> selected="selected" </c:if>>European
		Cut</option>
	<option value="Old Miner"
		<c:if test="${stock.shape == 'Old Miner'}"> selected="selected" </c:if>>Old
		Miner</option>
	<option value="Flanders"
		<c:if test="${stock.shape == 'Flanders'}"> selected="selected" </c:if>>Flanders</option>
	<option value="Cushion"
		<c:if test="${stock.shape == 'Cushion'}"> selected="selected" </c:if>>Cushion</option>
	<option value="Asscher"
		<c:if test="${stock.shape == 'Asscher'}"> selected="selected" </c:if>>Asscher</option>
	<option value="Baguette"
		<c:if test="${stock.shape == 'Baguette'}"> selected="selected" </c:if>>Baguette</option>
	<option value="Kite"
		<c:if test="${stock.shape == 'Kite'}"> selected="selected" </c:if>>Kite</option>
	<option value="Star"
		<c:if test="${stock.shape == 'Star'}"> selected="selected" </c:if>>Star</option>
	<option value="Other"
		<c:if test="${stock.shape == 'Other'}"> selected="selected" </c:if>>Other</option>
	<option value="Half Moon"
		<c:if test="${stock.shape == 'Half Moon'}"> selected="selected" </c:if>>Half
		Moon</option>
	<option value="Trapezoid"
		<c:if test="${stock.shape == 'Trapezoid'}"> selected="selected" </c:if>>Trapezoid</option>
	<option value="Bullets"
		<c:if test="${stock.shape == 'Bullets'}"> selected="selected" </c:if>>Bullets</option>
	<option value="Hexagonal"
		<c:if test="${stock.shape == 'Hexagonal'}"> selected="selected" </c:if>>Hexagonal</option>
	<option value="Lozenge"
		<c:if test="${stock.shape == 'Lozenge'}"> selected="selected" </c:if>>Lozenge</option>
	<option value="Pentagonal"
		<c:if test="${stock.shape == 'Pentagonal'}"> selected="selected" </c:if>>Pentagonal</option>
	<option value="Rose"
		<c:if test="${stock.shape == 'Rose'}"> selected="selected" </c:if>>Rose</option>
	<option value="Shield"
		<c:if test="${stock.shape == 'Shield'}"> selected="selected" </c:if>>Shield</option>
	<option value="Square"
		<c:if test="${stock.shape == 'Square'}"> selected="selected" </c:if>>Square</option>
	<option value="Triangular"
		<c:if test="${stock.shape == 'Triangular'}"> selected="selected" </c:if>>Triangular</option>
	<option value="Square Emerald"
		<c:if test="${stock.shape == 'Square Emerald'}"> selected="selected" </c:if>>Square
		Emerald</option>
	<option value="Cushion Modified"
		<c:if test="${stock.shape == 'Cushion Modified'}"> selected="selected" </c:if>>Cushion
		Modified</option>
	<option value="Briolette"
		<c:if test="${stock.shape == 'Briolette'}"> selected="selected" </c:if>>Briolette</option>
	<option value="Octagonal"
		<c:if test="${stock.shape == 'Octagonal'}"> selected="selected" </c:if>>Octagonal</option>
	<option value="Circular"
		<c:if test="${stock.shape == 'Circular'}"> selected="selected" </c:if>>Circular</option>
</select>
