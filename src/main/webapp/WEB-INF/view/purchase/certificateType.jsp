<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page isELIgnored="false" %>
<select name="certificateType" id="certificateType" class="form-control"
	<c:if test="${actionMode == 'view' || param.actionMode == 'view'}">disabled</c:if>>
	<option value="-1" selected="selected">Select Certificate Type</option>
	<option value="WithOut Certificate"
		<c:if test="${stock.certificateType == 'WithOut Certificate'}"> selected="selected" </c:if>>WithOut
		Certificate</option>
	<option value="GIA"
		<c:if test="${stock.certificateType == 'GIA'}"> selected="selected" </c:if>>GIA</option>
	<option value="IGI"
		<c:if test="${stock.certificateType == 'IGI'}"> selected="selected" </c:if>>IGI</option>
	<option value="CGL"
		<c:if test="${stock.certificateType == 'CGL'}"> selected="selected" </c:if>>CGL</option>
	<option value="HRD"
		<c:if test="${stock.certificateType == 'HRD'}"> selected="selected" </c:if>>HRD</option>
	<option value="AGS"
		<c:if test="${stock.certificateType == 'AGS'}"> selected="selected" </c:if>>AGS</option>
	<option value="EGL"
		<c:if test="${stock.certificateType == 'EGL'}"> selected="selected" </c:if>>EGL</option>
</select>