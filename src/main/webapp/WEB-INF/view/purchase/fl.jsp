<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page isELIgnored="false" %>
<select name="fl" id="fl" class="form-control"
	<c:if test="${actionMode == 'view' || param.actionMode == 'view'}">disabled</c:if>>
	<option value="-1" selected="selected">Select FL</option>
	<option value="None"
		<c:if test="${stock.fl == 'None'}"> selected="selected" </c:if>>None</option>
	<option value="VerySlight"
		<c:if test="${stock.fl == 'VerySlight'}"> selected="selected" </c:if>>VerySlight</option>
	<option value="Slight"
		<c:if test="${stock.fl == 'Slight'}"> selected="selected" </c:if>>Slight</option>
	<option value="Faint"
		<c:if test="${stock.fl == 'Faint'}"> selected="selected" </c:if>>Faint</option>
	<option value="Medium"
		<c:if test="${stock.fl == 'Medium'}"> selected="selected" </c:if>>Medium</option>
	<option value="Strong"
		<c:if test="${stock.fl == 'Strong'}"> selected="selected" </c:if>>Strong</option>
	<option value="VeryStrong"
		<c:if test="${stock.fl == 'VeryStrong'}"> selected="selected" </c:if>>VeryStrong</option>
</select>