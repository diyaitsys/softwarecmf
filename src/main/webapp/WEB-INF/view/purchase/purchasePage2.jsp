<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" lang="">
<!--<![endif]-->

<head>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<jsp:include page="../header.jsp"></jsp:include>
</head>

<body>
	<jsp:include page="../menu.jsp"></jsp:include>
	<!-- 	MAIN CODE START HERE -->
	<div id="right-panel" class="right-panel">

		<!-- Header-->
		<jsp:include page="../topHeader.jsp"></jsp:include>
		<!-- Header-->

		<div class="content mt-3">
			<div class="animated">
				<div class="row">
					<div class="col-lg-12">
						<div class="card">
							<div class="card-header">
								<strong>Party Details</strong>
							</div>
							<c:if test="${error != null && error != ''}">
								<div
									class="alert with-close alert-danger alert-dismissible fade show">
									<span class="badge badge-pill badge-danger">Error</span>${error}
									<button type="button" class="close" data-dismiss="alert"
										aria-label="Close">
										<span aria-hidden="true">&times;</span>
									</button>
								</div>
							</c:if>
							<div class="card-body card-block">
								<form action="/purchasePage/addUpdatePurchase" method="post"
									class="form-horizontal">
									<input type="hidden" name="actionMode" value="${actionMode}" />
									<c:if test="${actionMode != 'add'}">
										<input type="hidden" name="purchaseMaster.id" value="${purchaseMaster.id}" />
									</c:if>
									<div class="row form-group">
										<div class="col-lg-6">
											<label for="text-input" class=" form-control-label">Party
												Name <span class="required">*</span>
											</label> <select id="partyId" name="purchaseMaster.partyId"
												data-placeholder="Select Party" required
												class="standardSelect" tabindex="1"
												<c:if
											 test="${actionMode == 'view'}">
												readonly
												</c:if>>
												<option value=""></option>
												<c:forEach items="${partyList}" var="party">
													<option value="${party.id}">${party.name}</option>
												</c:forEach>
											</select>
										</div>
										<div class="col-lg-6">
											<div class="form-group">
												<label for="company" class=" form-control-label">Remark</label>
												<input type="text" id="remarks" name="purchaseMaster.remarks"
													placeholder="Enter remark" class="form-control">
											</div>
										</div>

									</div>
									<div class="row form-group">

										<div class="col col-lg-3">
											<label for="text-input" class=" form-control-label">InvoiceNo
												<span class="required">*</span>
											</label> <input type="text" id="invoiceNo" name="purchaseMaster.invoiceNo"
												placeholder="Invoice No" required
												value="${purchaseMaster.invoiceNo}" class="form-control"
												<c:if test="${actionMode == 'view'}"> readonly
											</c:if>>
										</div>
										<div class="col col-lg-3">
											<label for="text-input" class=" form-control-label">Purchase
												Date <span class="required">*</span>
											</label> <input type="text" id="purchaseDate" name="purchaseMaster.purchaseDate"
												required placeholder="Purchase Date"
												value="${purchaseMaster.purchaseDate}"
												class="form-control dtPicker"
												<c:if test="${actionMode == 'view'}"> readonly
											</c:if>>
										</div>
										<div class="col-lg-3">
											<div class="form-group">
												<label for="company" class=" form-control-label">Terms
													(Days) <span class="required">*</span>
												</label> <input type="text" id="terms" name="purchaseMaster.termsDays"
													onChange="setDueDate()" required placeholder="Enter Terms"
													class="form-control">
											</div>
										</div>
										<div class="col col-lg-3">
											<label for="text-input" class=" form-control-label">Due
												Date </label> <input type="text" id="dueDate" name="purchaseMaster.dueDate"
												placeholder="Due Date" readonly
												value="${purchaseMaster.dueDate}" class="form-control"
												<c:if test="${actionMode == 'view'}"> readonly
											</c:if>>
										</div>
									</div>
									<div class="row form-group">

										<div class="col-lg-2">
											<div class="form-group">
												<label for="company" class=" form-control-label">Buy
													Currency <span class="required">*</span>
												</label> <select id="buyCurrencyUnit" name="purchaseMaster.buyCurrencyUnit"
													data-placeholder="Select Currency" required
													onChange="buyCurrencyChange()" class="form-control"
													<c:if test="${actionMode == 'view'}">
														readonly
													</c:if>>
													<c:forEach items="${currencyMap}" var="currency">
														<option value="${currency.key}">${currency.value}</option>
													</c:forEach>
												</select>
											</div>
										</div>
										<div class="col-lg-2">
											<div class="form-group">
												<label for="company" class=" form-control-label">Convert
													Rate <span class="required">*</span>
												</label> <input type="text" id="conversionRate"
													name="purchaseMaster.conversionRate" required placeholder="Convert Rate"
													class="form-control">
											</div>
										</div>
										<div class="col-lg-2">
											<div class="form-group">
												<label for="company" class=" form-control-label">My
													Currency <span class="required">*</span>
												</label> <select id="myCurrencyUnit" name="purchaseMaster.myCurrencyUnit"
													class="form-control" data-placeholder="Select Currency"
													onChange="myCurrencyChange()" required
													<c:if test="${actionMode == 'view'}">
														readonly
													</c:if>>
													<c:forEach items="${currencyMap}" var="currency">
														<option value="${currency.key}">${currency.value}</option>
													</c:forEach>
												</select>
											</div>
										</div>
									</div>

									<div class="row form-group">
										<div class="col col-md-4">
											<button type="button" id="addStockBtn"
												class="btn btn-secondary mb-1" data-toggle="modal" data-target="#largeModal"
												>Add
												Stock</button>
										</div>
									</div>
								</form>
							</div>

							<!-- 		 Model Start -->
							<div class="modal fade" id="largeModal" tabindex="-1"
								role="dialog" aria-labelledby="largeModalLabel"
								aria-hidden="true">
								<div class="modal-dialog modal-lg" role="document"
									style="margin-left: 100px;">
									<div class="modal-content" style="width: 1150px;">
										<div class="modal-header">
											<h5 class="modal-title" id="largeModalLabel">Add Stock
												Modal</h5>
											<button type="button" class="close" data-dismiss="modal"
												aria-label="Close">
												<span aria-hidden="true">&times;</span>
											</button>
										</div>
										<div class="modal-body">
											<div class="row">
												<div class="col-lg-12">
													<form action="/stock/addUpdateStock" method="post"
														enctype="multipart/form-data" class="form-horizontal">
														<input type="hidden" name="actionMode"
															value="${actionMode}" />
														<div class="row form-group">
															<div class="col col-lg-2">
																<label for="text-input" class=" form-control-label">Lot
																	No <span class="required">*</span>
																</label> <input type="text" id="lotNo" name="lotNo" required
																	value="${stock.lotNo}" placeholder="Lot No"
																	class="form-control">
															</div>



															<div class="col col-md-2">
																<label for="select" class=" form-control-label">Shape
																	<span class="required">*</span>
																</label>
																<jsp:include page="shape.jsp"></jsp:include>
															</div>

															<div class="col col-md-2">
																<label for="text-input" class=" form-control-label">Carret
																	<span class="required">*</span>
																</label> <input type="text" id="carret" name="carret" required
																	onChange="calTotalPrice()" value=""
																	placeholder="Carret" class="form-control">
															</div>

															<div class="col col-md-2">
																<label for="select" class=" form-control-label">Color
																	<span class="required">*</span>
																</label>
																<jsp:include page="color.jsp"></jsp:include>
															</div>

															<div class="col col-md-2">
																<label for="select" class=" form-control-label">Clarity
																	<span class="required">*</span>
																</label>
																<jsp:include page="clarity.jsp"></jsp:include>
															</div>


														</div>

														<div class="row form-group">
															<div class="col col-md-2">
																<label for="select" class=" form-control-label">Cut
																</label>
																<jsp:include page="cut.jsp"></jsp:include>
															</div>

															<div class="col col-md-2">
																<label for="select" class=" form-control-label">Polish</label>
																<jsp:include page="polish.jsp"></jsp:include>
															</div>

															<div class="col col-md-2">
																<label for="select" class=" form-control-label">Symmetry</label>
																<jsp:include page="symmetry.jsp"></jsp:include>
															</div>

															<div class="col col-md-2">
																<label for="select" class=" form-control-label">FL
																	Color </label>
																<jsp:include page="fluorescenceColor.jsp"></jsp:include>
															</div>

															<div class="col col-md-2">
																<label for="select" class=" form-control-label">FL</label>
																<jsp:include page="fl.jsp"></jsp:include>
															</div>

															<div class="col col-lg-2">
																<label for="text-input" class=" form-control-label">Diameter
																</label>
																<div class="input-group">
																	<input type="text" id="diameter" name="diameter"
																		required value="" placeholder="Diameter"
																		class="form-control">
																	<div class="input-group-addon">MM</div>
																</div>
															</div>
														</div>

														<div class="row form-group">

															<div class="col col-md-3">
																<label for="select" class=" form-control-label">Certificate
																	Type </label>
																<jsp:include page="certificateType.jsp"></jsp:include>
															</div>

															<div class="col col-lg-3">
																<label for="text-input" class=" form-control-label">Certificate
																	No </label> <input type="text" id="certificateNo"
																	name="certificateNo" value=""
																	placeholder="Enter Certificate No" class="form-control">
															</div>

															<div class="col col-md-4">
																<label class=" form-control-label">WithRap</label>
																<div class="form-check-inline form-check">
																	<input id="withRap" name="withRap" value="1" checked
																		onChange="withRapClicked()" class="form-check-input"
																		type="checkbox">
																</div>
															</div>
														</div>

														<div class="row form-group">
															<div class="col col-md-2">
																<button id="rapPriceBtn" type="button"
																	onClick="getRapPriceOnline()"
																	class="btn btn-primary btn-sm">
																	<i class="fa fa-dot-circle-o"></i> Get Rap Price
																</button>
																<div class="input-group">
																	<input type="text" id="rapPrice" name="rapPrice"
																		onChange="calTotalPrice()" required value=""
																		placeholder="Rap Price" class="form-control">
																	<div id="rapPriceUnit" class="input-group-addon">USD</div>
																</div>
															</div>

															<div class="col col-md-2">
																<label class=" form-control-label">Disc/Prem</label>
																<div class="form-check">
																	<div class="radio">
																		<label for="radio1" class="form-check-label">
																			<input id="discountRadio" name="withPremium"
																			onChange="calTotalPrice()" value="discount"
																			class="form-check-input" type="radio"
																			checked="checked" onClick="checkDiscPrem()">Discount
																		</label>
																	</div>
																	<div class="radio">
																		<label for="radio2" class="form-check-label ">
																			<input id="premiumRadio" name="withPremium"
																			onChange="calTotalPrice()" onClick="checkDiscPrem()"
																			value="premium" class="form-check-input" type="radio">Premium
																		</label>
																	</div>
																</div>
															</div>

															<div class="col col-lg-2">
																<label id="discPremLabel" for="text-input"
																	class=" form-control-label">Discount </label>
																<div class="input-group">
																	<input id="discount" name="discount"
																		onChange="calTotalPrice()" placeholder="Discount"
																		class="form-control" type="text">
																	<div class="input-group-addon">%</div>
																</div>
															</div>

															<div class="col col-lg-2">
																<label for="text-input" class=" form-control-label">Add
																	Discount </label>
																<div class="input-group">
																	<input id="addDiscount" name="addDiscount"
																		onChange="calTotalPrice()" placeholder="Additional"
																		class="form-control" type="text">
																	<div class="input-group-addon">%</div>
																</div>
															</div>

															<div class="col col-lg-2">
																<label for="text-input" class=" form-control-label">Vol
																	Disc </label>
																<div class="input-group">
																	<input id="volDiscount" name="volDiscount"
																		onChange="calTotalPrice()" placeholder="Volume"
																		class="form-control" type="text">
																	<div class="input-group-addon">%</div>
																</div>
															</div>

															<div class="col col-lg-2">
																<label for="text-input" class=" form-control-label">Cash
																	Discount </label>
																<div class="input-group">
																	<input id="cashDiscount" name="cashDiscount"
																		onChange="calTotalPrice()" placeholder="Cash"
																		class="form-control" type="text">
																	<div class="input-group-addon">%</div>
																</div>
															</div>
														</div>

														<div class="row form-group">
															<div class="col col-lg-3">
																<label for="text-input" class=" form-control-label">Final
																	Price(CRT) </label>
																<div class="input-group">
																	<input type="text" id="finalPricePerCrt"
																		name="finalPricePerCrt" readonly value=""
																		placeholder="Final Price" class="form-control">
																	<div id="finalPriceUnit" class="input-group-addon">USD</div>
																</div>
															</div>

															<div class="col col-lg-3">
																<label for="text-input" class=" form-control-label">Total
																	Price </label>
																<div class="input-group">
																	<input type="text" id="totalPrice" name="totalPrice"
																		readonly value="" placeholder="Total Price"
																		class="form-control">
																	<div id="totalPriceUnit" class="input-group-addon">USD</div>
																</div>
															</div>
														</div>
													</form>
												</div>
											</div>
										</div>
										<div class="modal-footer">
											<button type="button" class="btn btn-secondary"
												 onClick="modelClose();">Cancel</button>
											<button type="button" class="btn btn-primary"
												onClick="modelSubmit();return false;">Confirm</button>
										</div>
									</div>
								</div>
							</div>
							<!-- 		 Model End -->
						</div>
						<!-- .animated -->
					</div>

					<!-- 		Datatable Start -->
					<div class="content mt-3">
						<div class="animated fadeIn">
							<div class="row">
								<div class="col-md-12">
									<div class="card">
										<div class="card-header">
											<strong class="card-title">Stock Details</strong>
										</div>
										<div class="card-body">
											<table id="bootstrap-data-table"
												class="table table-striped table-bordered">
												<thead>
													<tr>
														<th>Lot No</th>
														<th>Shape</th>
														<th>Carret</th>
														<th>Color</th>
														<th>Clarity</th>
														<th>Rap Price</th>
														<th>Back</th>
														<th>Add. Disc</th>
														<th>Total Price</th>
														<th></th>
													</tr>
												</thead>
												<tbody>
													<!-- 													<tr> -->
													<!-- 														<td>123</td> -->
													<!-- 														<td>50</td> -->
													<!-- 														<td>Red</td> -->
													<!-- 														<td>Diamond</td> -->
													<!-- 														<td>123</td> -->
													<!-- 														<td>50</td> -->
													<!-- 														<td>123</td> -->
													<!-- 														<td>50</td> -->
													<!-- 														<td>5000</td> -->
													<!-- 														<td class="text-center"> -->
													<!-- 															<button type="button" -->
													<!-- 																class="btn btn-outline-success btn-sm">View</button> -->
													<!-- 															<button type="button" -->
													<!-- 																class="btn btn-outline-primary btn-sm">Update</button> -->
													<!-- 															<button type="button" -->
													<!-- 																class="btn btn-outline-danger btn-sm">Delete</button> -->
													<!-- 														</td> -->
													<!-- 													</tr> -->
												</tbody>
											</table>
										</div>
									</div>
								</div>
							</div>
						</div>
						<!-- .animated -->
					</div>

					<!-- 		Datatable End -->

					<!-- 		Bottom Page Start -->
					<div class="content mt-3">
						<div class="animated fadeIn">
							<div class="card">
								<div class="card-body">
									<div class="row form-group">
										<div class="col-lg-3">
											<div class="form-group">
												<label for="company" class=" form-control-label">Net
													Price (Buy Currency) </label>
												<div class="input-group">
													<input type="text" id="netPriceBuyCurrency"
														name="purchaseMaster.netPriceBuyCurrency" readonly
														placeholder="Net Price" class="form-control">
													<div id="netPriceBuyCurrentUnit" class="input-group-addon">USD</div>
												</div>
											</div>
										</div>
										<div class="col-lg-3">
											<div class="form-group">
												<label for="company" class=" form-control-label">Net
													Price (My Currency) </label>
												<div class="input-group">
													<input type="text" id="netPriceMyCurrency"
														name="purchaseMaster.netPriceMyCurrency" readonly placeholder="Net Price"
														class="form-control">
													<div id="netPriceMyCurrencyUnit" class="input-group-addon">USD</div>
												</div>
											</div>
										</div>
									</div>
									<div class="row form-group">
										<div class="col-lg-4">
											<div class="form-group">
												<label for="company" class=" form-control-label">Broker
													Name </label> <select name="purchaseMaster.brokerId"
													data-placeholder="Select Broker" required
													class="standardSelect" tabindex="1"
													<c:if test="${actionMode == 'view'}">
								readonly
								</c:if>>
													<option value=""></option>
													<c:forEach items="${brokerList}" var="broker">
														<option value="${broker.id}">${broker.name}</option>
													</c:forEach>
												</select>
											</div>
										</div>
										<div class="col-lg-2">
											<div class="form-group">
												<label for="company" class=" form-control-label">Brokerage
												</label>
												<div class="input-group">
													<input type="text" id="brokerage" placeholder="Brokerage"
														class="form-control">
													<div class="input-group-addon">%</div>
												</div>
											</div>
										</div>
										<div class="col-lg-4">
											<div class="form-group">
												<label for="company" class=" form-control-label">Brokerage
													Amount (My Current)</label>
												<div class="input-group">
													<input type="text" id="brokerageAmount"
														name="purchaseMaster.brokerageAmount" placeholder="Brokerage Amount"
														class="form-control">
													<div id="brokerageAmountUnit" class="input-group-addon">USD</div>
												</div>
											</div>
										</div>
									</div>
									<div class="row form-group">
										<div class="col-lg-2">
											<div class="form-group">
												<label for="company" class=" form-control-label">Tax
													Details </label> <select name="purchaseMaster.taxDetails" id="taxDetails"
													onChange="taxChange()" class="form-control">
													<option value="0">Without Tax</option>
													<option value="1">IGST</option>
													<option value="2">CGST/SGST</option>
												</select>
											</div>
										</div>
										<div class="col-lg-2" id="IGSTDiv" style="display: none">
											<div class="form-group">
												<label for="company" class=" form-control-label">IGST</label>
												<input type="text" id="igst" placeholder="Enter IGST"
													class="form-control">

											</div>
										</div>
										<div class="col-lg-2" id="CGSTDiv" style="display: none">
											<div class="form-group">
												<label for="company" class=" form-control-label">CGST</label>
												<input type="text" id="cgst" placeholder="Enter CGST"
													class="form-control">
											</div>
										</div>
										<div class="col-lg-2" id="SGSTDiv" style="display: none">
											<div class="form-group">
												<label for="company" class=" form-control-label">SGST</label>
												<input type="text" id="sgst" placeholder="Enter SGST"
													class="form-control">
											</div>
										</div>
									</div>
									<div class="row form-group">
										<div class="col-lg-4">
											<div class="form-group">
												<label for="company" class=" form-control-label">Bill
													Amount (Buying Currency)</label>
												<div class="input-group">
													<input type="text" id="billAmountBuyCurrency"
														name="purchaseMaster.billAmountBuyCurrency" readonly
														placeholder="Bill Amount" class="form-control">
													<div id="billAmountBuyCurrencyUnit"
														class="input-group-addon">USD</div>
												</div>
											</div>
										</div>
										<div class="col-lg-4">
											<div class="form-group">
												<label for="company" class=" form-control-label">Bill
													Amount (My Currency)</label>
												<div class="input-group">
													<input type="text" id="billAmountMyCurrency"
														name="purchaseMaster.billAmountMyCurrency" readonly
														placeholder="Bill Amount" class="form-control">
													<div id="billAmountMyCurrencyUnit"
														class="input-group-addon">USD</div>
												</div>

											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="card-footer">
						<c:if test="${actionMode != 'view'}">
							<button type="submit" class="btn btn-primary btn-sm">
								<i class="fa fa-dot-circle-o"></i> Submit
							</button>
						</c:if>
						<button type="button" class="btn btn-danger btn-sm"
							onClick="javascript:closeBtn();">
							<i class="fa fa-dot-circle-o"></i> Close
						</button>
					</div>
				</div>
			</div>
		</div>
		<!-- 		Bottom Page End -->
		<!-- .content -->
	</div>
	<!-- 	MAIN CODE END HERE -->
	<jsp:include page="../footer.jsp"></jsp:include>
	<script>
		$(document).ready(function() {
 			$("#addStockBtn").click(function() {
 				document.getElementById('carret').value = "";
 			});

		});
		function closeBtn() {
			window.history.back();
		}

		function getRapPriceOnline() {
			var shape = document.getElementById('shape').value;
			var carret = document.getElementById('carret').value;
			var color = document.getElementById('color').value;
			var clarity = document.getElementById('clarity').value;

			if (shape == -1) {
				alert("Please Select Shape");
				return;
			}
			if (color == -1) {
				alert("Please Select Color");
				return;
			}
			if (clarity == -1) {
				alert("Please Select Purity");
				return;
			}
			if (carret == '') {
				alert("Please Select Carret");
				return;
			}

			document.getElementById('rapPrice').value = "";
			$.ajax({
				type : 'GET',
				url : '/rapnet/getRapPriceOnline?shape=' + shape + "&color="
						+ color + "&clarity=" + clarity + "&size=" + carret,

				success : function(data, status) {
					//alert(data);
					if (data == "ERROR") {
						alert("ERROR!!! Please Check RapNet Username/Password")
					} else {
						document.getElementById('rapPrice').value = data;
						calTotalPrice();
					}
				}
			});

			//$("#rapPrice").val()
		}

		function modelSubmit() {
			var lotNo = document.getElementById('lotNo').value;
			if (lotNo == "") {
				alert("Please Enter LotNo");
				return false;
			}
			modelClose();
			var shape = document.getElementById('shape').value;
			if (shape == "") {
				alert("Please Enter shape");
				return;
			}
			var carret = document.getElementById('carret').value;
			if (carret == "") {
				alert("Please Enter carret");
				return;
			}
			var color = document.getElementById('color').value;
			if (color == "") {
				alert("Please Enter color");
				return;
			}
			var clarity = document.getElementById('clarity').value;
			if (clarity == "") {
				alert("Please Enter clarity");
				return;
			}
			var rapPrice = document.getElementById('rapPrice').value;
			if (rapPrice == "") {
				alert("Please Enter rapPrice");
				return;
			}
			var totalPrice = document.getElementById('totalPrice').value;
			if (totalPrice == "") {
				alert("Please Enter totalPrice");
				return;
			}
			alert("ALL FINE");
			var addRow =  "<tr><td></td></tr>"
			$("table tbody").append(markup);
		}

		function openModel() {
			//$("button[data-toggle=\"modal\" data-target=\"#largeModal\"]").click();
			//$('body').css('padding-right', '');
			//$("#largeModal").model();
			//$(".modal").show();
			//$('#largeModal').appendTo("body").modal('show');
		}
		function modelClose() {
			//$("button[data-dismiss=\"modal\"]").click();
			$(".modal").removeClass("in");
			$(".modal-backdrop").remove();
			$('body').removeClass('modal-open');
			$('body').css('padding-right', '');
			$(".modal").hide();
		}

		function calTotalPrice() {
			var carret = document.getElementById('carret').value;
			if (carret == "") {
				carret = 0;
			}
			var rapPrice = document.getElementById('rapPrice').value;
			if (rapPrice == "") {
				rapPrice = 0;
			}
			var isDiscount = 1;
			if (!document.getElementById("discountRadio").checked) {
				isDiscount = 0;
			}

			var addDiscount = document.getElementById('addDiscount').value;
			if (addDiscount == "") {
				document.getElementById('addDiscount').value = 0;
				addDiscount = 0;
			}

			var volumnDiscount = document.getElementById('volDiscount').value;
			if (volumnDiscount == "") {
				document.getElementById('volDiscount').value = 0;
				volumnDiscount = 0;
			}

			var discount = document.getElementById('discount').value;
			if (discount == "") {
				document.getElementById('discount').value = 0;
				discount = 0;
			}

			var cashDiscount = document.getElementById('cashDiscount').value;
			if (cashDiscount == "") {
				document.getElementById('cashDiscount').value = 0;
				cashDiscount = 0;
			}

			var finalPricePerCrt = 0;
			if (isDiscount == 1) {
				finalPricePerCrt = parseFloat(rapPrice)
						- parseFloat(rapPrice * discount / 100);
			} else {
				finalPricePerCrt = parseFloat(rapPrice)
						+ parseFloat((rapPrice * discount) / 100);
			}

			finalPricePerCrt = parseFloat(finalPricePerCrt)
					- parseFloat(finalPricePerCrt * addDiscount / 100);
			finalPricePerCrt = parseFloat(finalPricePerCrt)
					- parseFloat(finalPricePerCrt * volumnDiscount / 100);
			finalPricePerCrt = parseFloat(finalPricePerCrt)
					- parseFloat(finalPricePerCrt * cashDiscount / 100);

			document.getElementById('finalPricePerCrt').value = parseFloat(
					finalPricePerCrt).toFixed(2);
			document.getElementById('totalPrice').value = parseFloat(
					finalPricePerCrt * carret).toFixed(2);
		}

		function formatDate(date) {
			var d = new Date(date), month = '' + (d.getMonth() + 1), day = ''
					+ d.getDate(), year = d.getFullYear();

			if (month.length < 2)
				month = '0' + month;
			if (day.length < 2)
				day = '0' + day;

			return [ year, month, day ].join('-');
		}

		function setDueDate() {
			var date = new Date($("#purchaseDate").val());
			var days = parseInt($("#terms").val(), 10);

			if (!isNaN(date.getTime())) {
				date.setDate(date.getDate() + days);

				$("#dueDate").val(formatDate(date));
			} else {
				alert("Invalid Date");
			}
		}

		function checkDiscPrem() {
			if (document.getElementById("discountRadio").checked) {
				document.getElementById("discPremLabel").innerHTML = "Discount";
			} else {
				document.getElementById("discPremLabel").innerHTML = "Premium";
			}
		}
		function buyCurrencyChange() {
			var buyCurrencyUnitVal = $("#buyCurrencyUnit").val();
			document.getElementById("finalPriceUnit").innerHTML = buyCurrencyUnitVal;
			document.getElementById("totalPriceUnit").innerHTML = buyCurrencyUnitVal;
			document.getElementById("rapPriceUnit").innerHTML = buyCurrencyUnitVal;
			document.getElementById("netPriceBuyCurrentUnit").innerHTML = buyCurrencyUnitVal;
			document.getElementById("billAmountBuyCurrencyUnit").innerHTML = buyCurrencyUnitVal;
		}

		function myCurrencyChange() {
			var myCurrencyUnitVal = $("#myCurrencyUnit").val();
			document.getElementById("netPriceMyCurrencyUnit").innerHTML = myCurrencyUnitVal;
			document.getElementById("billAmountMyCurrencyUnit").innerHTML = myCurrencyUnitVal;
			document.getElementById("brokerageAmountUnit").innerHTML = myCurrencyUnitVal;
		}

		function withRapClicked() {
			if (document.getElementById("withRap").checked) {
				document.getElementById('rapPriceBtn').style.display = '';
			} else {
				document.getElementById('rapPriceBtn').style.display = 'none';
			}
		}
		function taxChange() {
			var valueSelected = $('#taxDetails').val();
			//var myPurchasePrice = $('#myPurchasePrice').val();
			//var IGSTPer = parseFloat($('#IGSTPer').val());
			//var CGSTPer = parseFloat($('#CGSTPer').val());
			//var SGSTPer = parseFloat($('#SGSTPer').val());

			if (valueSelected == 0) {
				$('#IGSTDiv').hide();
				$('#SGSTDiv').hide();
				$('#CGSTDiv').hide();

				$('#SGST').val("0.0");
				$('#CGST').val("0.0");
				$('#IGST').val("0.0");
				//$("#netPriceAfterTax").val(
				//		parseFloat(myPurchasePrice).toFixed(2));
			} else if (valueSelected == 1) {
				$('#IGSTDiv').show();
				$('#SGSTDiv').hide();
				$('#CGSTDiv').hide();

				$('#SGST').val("0.0");
				$('#CGST').val("0.0");

				//$('#IGST').val(
				//		parseFloat(myPurchasePrice * IGSTPer / 100).toFixed(2));
				//$("#netPriceAfterTax").val(
				//		parseFloat(
				//				parseFloat($('#IGST').val())
				//						+ parseFloat(myPurchasePrice)).toFixed(
				//				2));
			} else if (valueSelected == 2) {
				$('#IGSTDiv').hide();
				$('#SGSTDiv').show();
				$('#CGSTDiv').show();

				$('#IGST').val("0.0");
				//$('#SGST').val("0.0");
				//$('#CGST').val("0.0");

				// 				$('#CGST').val(
				// 						parseFloat(myPurchasePrice * CGSTPer / 100).toFixed(2));
				// 				$('#SGST').val(
				// 						parseFloat(myPurchasePrice * SGSTPer / 100).toFixed(2));
				// 				$("#netPriceAfterTax").val(
				// 						parseFloat(
				// 								parseFloat($('#SGST').val())
				// 										+ parseFloat($('#CGST').val())
				// 										+ parseFloat(myPurchasePrice)).toFixed(
				// 								2));
			}
		}
	</script>
</body>

</html>