<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page isELIgnored="false" %>
<select name="clarity" id="clarity" class="form-control" required
	<c:if test="${actionMode == 'view' || param.actionMode == 'view'}">disabled</c:if>>
	<option value="-1" selected="selected">Select clarity</option>
	<option value="IF"
		<c:if test="${stock.clarity == 'IF'}"> selected="selected" </c:if>>IF</option>
	<option value="VVS1"
		<c:if test="${stock.clarity == 'VVS1'}"> selected="selected" </c:if>>VVS1</option>
	<option value="VVS2"
		<c:if test="${stock.clarity == 'VVS2'}"> selected="selected" </c:if>>VVS2</option>
	<option value="VS1"
		<c:if test="${stock.clarity == 'VS1'}"> selected="selected" </c:if>>VS1</option>
	<option value="VS2"
		<c:if test="${stock.clarity == 'VS2'}"> selected="selected" </c:if>>VS2</option>
	<option value="SI1"
		<c:if test="${stock.clarity == 'SI1'}"> selected="selected" </c:if>>SI1</option>
	<option value="SI2"
		<c:if test="${stock.clarity == 'SI2'}"> selected="selected" </c:if>>SI2</option>
	<option value="SI3"
		<c:if test="${stock.clarity == 'SI3'}"> selected="selected" </c:if>>SI3</option>
	<option value="I1"
		<c:if test="${stock.clarity == 'I1'}"> selected="selected" </c:if>>I1</option>
	<option value="I2"
		<c:if test="${stock.clarity == 'I2'}"> selected="selected" </c:if>>I2</option>
	<option value="I3"
		<c:if test="${stock.clarity == 'I3'}"> selected="selected" </c:if>>I3</option>
</select>