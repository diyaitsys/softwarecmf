<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" lang="">
<!--<![endif]-->

<head>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page isELIgnored="false"%>
<jsp:include page="../header.jsp"></jsp:include>
</head>

<body>
	<!-- 	MAIN CODE START HERE -->
	<div id="right-panel" class="right-panel">

		<!-- Header-->
		<!-- Header-->

		<div class="content mt-3">
			<div class="animated">
				<div class="row">
					<div class="col-lg-12">
						<div class="card">
							<div class="card-header">
								<strong>Purchase Details</strong>
							</div>
							<div class="card-body card-block">
								<div class="row form-group">
									<div class="col-lg-6">
										<label for="text-input" class=" form-control-label">Party
											Name <span class="required">*</span>
										</label> <select id="partyId" name="purchaseMaster.partyId"
											data-placeholder="Select Party" class="standardSelect"
											tabindex="1"
											<c:if
											 test="${actionMode == 'view'}">
												
												</c:if>>
											<option value=""></option>
											<c:forEach items="${partyList}" var="party">
												<option value="${party.id}"
													<c:if test="${purchaseMaster.partyId == party.id}">selected</c:if>>
													${party.name}</option>
											</c:forEach>
										</select>
									</div>
									<div class="col-lg-6">
										<div class="form-group">
											<label for="company" class=" form-control-label">Remark</label>
											<input type="text" id="remarks" name="purchaseMaster.remarks"
												placeholder="Enter remark" value="${purchaseMaster.remarks}"
												class="form-control">
										</div>
									</div>

								</div>
								<div class="row form-group">

									<div class="col col-lg-3">
										<label for="text-input" class=" form-control-label">InvoiceNo
											<span class="required">*</span>
										</label> <input type="text" id="invoiceNo"
											name="purchaseMaster.invoiceNo" placeholder="Invoice No"
											required value="${purchaseMaster.invoiceNo}"
											class="form-control">
									</div>
									<div class="col col-lg-3">
										<label for="text-input" class=" form-control-label">Purchase
											Date <span class="required">*</span>
										</label> <input type="text" id="purchaseDate"
											name="purchaseMaster.purchaseDate"
											placeholder="Purchase Date"
											value="${purchaseMaster.purchaseDate}"
											class="form-control dtPicker">
									</div>
									<div class="col-lg-3">
										<div class="form-group">
											<label for="company" class=" form-control-label">Terms
												(Days) <span class="required">*</span>
											</label> <input type="text" id="terms"
												value="${purchaseMaster.termsDays}" onChange="setDueDate()"
												required placeholder="Enter Terms" class="form-control">
										</div>
									</div>
									<div class="col col-lg-3">
										<label for="text-input" class=" form-control-label">Due
											Date </label> <input type="text" id="dueDate"
											name="purchaseMaster.dueDate" placeholder="Due Date"
											value="${purchaseMaster.dueDate}" readonly
											class="form-control">
									</div>
								</div>
								<div class="row form-group">

									<div class="col-lg-2">
										<div class="form-group">
											<label for="company" class=" form-control-label">Buy
												Currency <span class="required">*</span>
											</label> <select id="buyCurrencyUnit"
												name="purchaseMaster.buyCurrencyUnit"
												data-placeholder="Select Currency" required
												class="form-control"
												<c:if test="${actionMode == 'view'}">
														
													</c:if>>
												<c:forEach items="${currencyMap}" var="currency">
													<option value="${currency.key}"
														<c:if test="${purchaseMaster.buyCurrencyUnit== currency.key}">selected</c:if>>${currency.value}</option>
												</c:forEach>
											</select>
										</div>
									</div>
									<div class="col-lg-2">
										<div class="form-group">
											<label for="company" class=" form-control-label">Convert
												Rate <span class="required">*</span>
											</label> <input type="text" id="conversionRate"
												value="${purchaseMaster.conversionRate}" required
												placeholder="Convert Rate" class="form-control" value="1">
										</div>
									</div>
									<div class="col-lg-2">
										<div class="form-group">
											<label for="company" class=" form-control-label">My
												Currency <span class="required">*</span>
											</label> <select id="myCurrencyUnit"
												name="purchaseMaster.myCurrencyUnit" class="form-control"
												data-placeholder="Select Currency"
												<c:if test="${actionMode == 'view'}">
														
													</c:if>>
												<c:forEach items="${currencyMap}" var="currency">
													<option value="${currency.key}"
														<c:if test="${purchaseMaster.myCurrencyUnit== currency.key}">selected</c:if>>${currency.value}</option>
												</c:forEach>
											</select>
										</div>
									</div>
								</div>
								
								<div class="row form-group">
										<div class="col col-md-4">
											<button type="button" id="addStockBtn"
												class="btn btn-secondary mb-1" data-toggle="modal"
												data-target="#largeModal">Add Stock</button>
										</div>
										<!-- 										 -->
									</div>

									<!-- 		 Model Start -->
									<div class="modal fade" id="largeModal" tabindex="-1"
										role="dialog" aria-labelledby="largeModalLabel"
										aria-hidden="true">
										<div class="modal-dialog modal-lg" role="document"
											style="margin-left: 100px;">
											<div class="modal-content" style="width: 1150px;">
												<div class="modal-header">
													<h5 class="modal-title" id="largeModalLabel">Add Stock
														Modal</h5>
													<button type="button" class="close" data-dismiss="modal"
														aria-label="Close">
														<span aria-hidden="true">&times;</span>
													</button>
												</div>
												<div class="modal-body">
													<div class="row">
														<div class="col-lg-12">
															<div class="row form-group">
																<div class="col col-lg-2">
																	<label for="text-input" class=" form-control-label">Lot
																		No <span class="required">*</span>
																	</label> <input type="text" id="lotNo" value="${stock.lotNo}"
																		placeholder="Lot No" class="form-control">
																</div>
																<div class="col col-md-2">
																	<label for="select" class=" form-control-label">Shape
																		<span class="required">*</span>
																	</label>
																	<jsp:include page="shape.jsp"></jsp:include>
																</div>

																<div class="col col-md-2">
																	<label for="text-input" class=" form-control-label">Carat
																		<span class="required">*</span>
																	</label> <input type="text" id="carret"
																		onChange="calTotalPrice()" value=""
																		placeholder="Carat" class="form-control">
																</div>

																<div class="col col-md-2">
																	<label for="select" class=" form-control-label">Color
																		<span class="required">*</span>
																	</label>
																	<jsp:include page="color.jsp"></jsp:include>
																</div>

																<div class="col col-md-2">
																	<label for="select" class=" form-control-label">Clarity
																		<span class="required">*</span>
																	</label>
																	<jsp:include page="clarity.jsp"></jsp:include>
																</div>
															</div>

															<div class="row form-group">
																<div class="col col-md-2">
																	<label for="select" class=" form-control-label">Cut
																	</label>
																	<jsp:include page="cut.jsp"></jsp:include>
																</div>

																<div class="col col-md-2">
																	<label for="select" class=" form-control-label">Polish</label>
																	<jsp:include page="polish.jsp"></jsp:include>
																</div>

																<div class="col col-md-2">
																	<label for="select" class=" form-control-label">Symmetry</label>
																	<jsp:include page="symmetry.jsp"></jsp:include>
																</div>

																<div class="col col-md-2">
																	<label for="select" class=" form-control-label">FL</label>
																	<jsp:include page="../purchase/fl.jsp"></jsp:include>
																</div>

																<div class="col col-md-2">
																	<label for="select" class=" form-control-label">FL
																		Color </label>
																	<jsp:include page="../purchase/fluorescenceColor.jsp"></jsp:include>
																</div>

																<div class="col col-lg-2">
																	<label for="text-input" class=" form-control-label">Diameter
																	</label>
																	<div class="input-group">
																		<input type="text" id="diameter" name="diameter"
																			value="" placeholder="Diameter" class="form-control">
																		<div class="input-group-addon">MM</div>
																	</div>
																</div>
															</div>

															<div class="row form-group">

																<div class="col col-md-3">
																	<label for="select" class=" form-control-label">Certificate
																		Type </label>
																	<jsp:include page="certificateType.jsp"></jsp:include>
																</div>

																<div class="col col-lg-3">
																	<label for="text-input" class=" form-control-label">Certificate
																		No </label> <input type="text" id="certificateNo"
																		name="certificateNo" value=""
																		placeholder="Enter Certificate No"
																		class="form-control">
																</div>

																<div class="col col-md-4">
																	<label class=" form-control-label">WithRap</label>
																	<div class="form-check-inline form-check">
																		<input id="withRap" name="withRap" value="1" checked
																			onChange="withRapClicked()" class="form-check-input"
																			type="checkbox">
																	</div>
																</div>
															</div>

															<div class="row form-group">
																<div class="col col-md-2">
																	<button id="rapPriceBtn" type="button"
																		onClick="getRapPriceOnline()"
																		class="btn btn-primary btn-sm">
																		<i class="fa fa-dot-circle-o"></i> Get Rap Price
																	</button>
																	<div class="input-group">
																		<input type="text" id="rapPrice" name="rapPrice"
																			onChange="calTotalPrice()" value=""
																			placeholder="Rap Price" class="form-control">
																		<div id="rapPriceUnit" class="input-group-addon">USD</div>
																	</div>
																</div>

																<div class="col col-md-2">
																	<label class=" form-control-label">Disc/Prem</label>
																	<div class="form-check">
																		<div class="radio">
																			<label for="radio1" class="form-check-label">
																				<input id="discountRadio" name="withPremium"
																				onChange="calTotalPrice()" value="discount"
																				class="form-check-input" type="radio"
																				checked="checked" onClick="checkDiscPrem()">Discount
																			</label>
																		</div>
																		<div class="radio">
																			<label for="radio2" class="form-check-label ">
																				<input id="premiumRadio" name="withPremium"
																				onChange="calTotalPrice()" onClick="checkDiscPrem()"
																				value="premium" class="form-check-input"
																				type="radio">Premium
																			</label>
																		</div>
																	</div>
																</div>
															</div>

															<div class="row form-group">
																<div class="col col-lg-2">
																	<label id="discPremLabel" for="text-input"
																		class=" form-control-label">Discount </label>
																	<div class="input-group">
																		<input id="discount" name="discount"
																			onBlur="calTotalPrice()" placeholder="Discount"
																			class="form-control" type="text">
																		<div class="input-group-addon">%</div>
																	</div>
																</div>

																<div class="col col-lg-2">
																	<label for="text-input" class=" form-control-label">Delivery
																		Disc.</label>
																	<div class="input-group">
																		<input id="addDiscount" name="addDiscount"
																			onBlur="calTotalPrice()" placeholder="Delivery"
																			class="form-control" type="text">
																		<div class="input-group-addon">%</div>
																	</div>
																</div>

																<div class="col col-lg-2">
																	<label for="text-input" class=" form-control-label">Blind
																		Disc.</label>
																	<div class="input-group">
																		<input id="blindDiscount" name="blindDiscount"
																			onBlur="calTotalPrice()" placeholder="Blind"
																			class="form-control" type="text">
																		<div class="input-group-addon">%</div>
																	</div>
																</div>

																<div class="col col-lg-2">
																	<label for="text-input" class=" form-control-label">No
																		Broker Disc.</label>
																	<div class="input-group">
																		<input id="noBrokerDiscount" name="noBrokerDiscount"
																			onBlur="calTotalPrice()" placeholder="No Broker"
																			class="form-control" type="text">
																		<div class="input-group-addon">%</div>
																	</div>
																</div>

																<div class="col col-lg-2">
																	<label for="text-input" class=" form-control-label">Vol
																		Disc.</label>
																	<div class="input-group">
																		<input id="volDiscount" name="volDiscount"
																			onBlur="calTotalPrice()" placeholder="Volume"
																			class="form-control" type="text">
																		<div class="input-group-addon">%</div>
																	</div>
																</div>

																<div class="col col-lg-2">
																	<label for="text-input" class=" form-control-label">Adv.
																		Pay Disc. </label>
																	<div class="input-group">
																		<input id="cashDiscount" name="cashDiscount"
																			onBlur="calTotalPrice()" placeholder="Cash"
																			class="form-control" type="text">
																		<div class="input-group-addon">%</div>
																	</div>
																</div>
															</div>

															<div class="row form-group">
																<div class="col col-lg-3">
																	<label for="text-input" class=" form-control-label">Final
																		Price(CRT) </label>
																	<div class="input-group">
																		<input type="text" id="finalPricePerCrt"
																			name="finalPricePerCrt" readonly value=""
																			placeholder="Final Price" class="form-control">
																		<div id="finalPriceUnit" class="input-group-addon">USD</div>
																	</div>
																</div>

																<div class="col col-lg-3">
																	<label for="text-input" class=" form-control-label">Total
																		Price </label>
																	<div class="input-group">
																		<input type="text" id="totalPrice" name="totalPrice"
																			readonly value="" placeholder="Total Price"
																			class="form-control">
																		<div id="totalPriceUnit" class="input-group-addon">USD</div>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
												<div class="modal-footer">
													<button type="button" class="btn btn-secondary" data-dismiss="modal" data-backdrop="false" aria-hidden="true"
														">Cancel</button>
													<button type="button" class="btn btn-primary"
														onClick="modelSubmit();return false;">Confirm</button>
												</div>
											</div>
										</div>
									</div>
									<!-- 		 Model End -->

								<!-- 		Datatable Start -->
								<div class="content mt-3">
									<div class="animated fadeIn">
										<div class="row">
											<div class="col-md-12">
												<div class="card">
													<div class="card-header">
														<strong class="card-title">Stock Details</strong>
													</div>
													<div class="card-body">
														<table id="dataTable"
															class="table table-striped table-bordered">
															<thead>
																<tr>
																	<th>Lot No</th>
																	<th>Carat</th>
																	<th>Shape</th>
																	<th>Color</th>
																	<th>Clarity</th>
																	<th>Total Price In (${purchaseMaster.buyCurrencyUnit})</th>
																	<th>Total Price In (${purchaseMaster.myCurrencyUnit})</th>
																	<th></th>
																</tr>
															</thead>
															<tbody>
																<c:forEach items="${stockList}" var="stock">
																	<tr>
																		<td>${stock.lotNo}</td>
																		<td>${stock.carret}</td>
																		<td>${stock.shape}</td>
																		<td>${stock.color}</td>
																		<td>${stock.clarity}</td>
																		<td><fmt:formatNumber type="number"
																				maxFractionDigits="2"
																				value="${stock.totalPrice}" />
																			</td>
																		<td><fmt:formatNumber type="number"
																				maxFractionDigits="2"
																				value="${stock.totalPrice * purchaseMaster.conversionRate}" />
																			</td>
																		<td>
																			<button type="button"
																				onClick="viewStock(${stock.id})"
																				class="btn btn-outline-success btn-sm">Details</button>
																			<button type="button"
																				onClick="editStock('${stock.lotNo}')"
																				class="btn btn-outline-danger btn-sm">Edit</button>
																			<button type="button"
																				onClick="deleteStock('${stock.lotNo}')"
																				class="btn btn-outline-danger btn-sm">Delete</button>
																		</td>
																	</tr>
																</c:forEach>
															</tbody>
															<tfoot>
																<tr>
																	<th>Lot No</th>
																	<th>Carat</th>
																	<th>Shape</th>
																	<th>Color</th>
																	<th>Clarity</th>
																	<th>Total Price</th>
																	<th>Total Price</th>
																	<th></th>
																</tr>
															</tfoot>
														</table>
													</div>
												</div>
											</div>
										</div>
									</div>
									<!-- .animated -->
								</div>

								<!-- 		Datatable End -->

								<!-- 		Bottom Page Start -->
								<div class="content mt-3">
									<div class="animated fadeIn">
										<div class="card">
											<div class="card-body">
												<div class="row form-group">
													<div class="col-lg-3">
														<div class="form-group">
															<label for="company" class=" form-control-label">Net
																Price (Buy Currency) </label>
															<div class="input-group">
																<input type="text" id="netPriceBuyCurrency"
																	value="${purchaseMaster.netPriceBuyCurrency}"
																	placeholder="Net Price" class="form-control">
																<div id="netPriceBuyCurrentUnit"
																	class="input-group-addon">${purchaseMaster.buyCurrencyUnit}</div>
															</div>
														</div>
													</div>
													<div class="col-lg-3">
														<div class="form-group">
															<label for="company" class=" form-control-label">Net
																Price (My Currency) </label>
															<div class="input-group">
																<input type="text" id="netPriceMyCurrency"
																	value="${purchaseMaster.netPriceMyCurrency}"
																	placeholder="Net Price" class="form-control">
																<div id="netPriceMyCurrencyUnit"
																	class="input-group-addon">${purchaseMaster.myCurrencyUnit}</div>
															</div>
														</div>
													</div>
												</div>
												<div class="row form-group">
													<div class="col-lg-4">
														<div class="form-group">
															<label for="company" class=" form-control-label">Broker
																Name </label> <select name="purchaseMaster.brokerId"
																data-placeholder="Select Broker" class="standardSelect"
																tabindex="1"
																<c:if test="${actionMode == 'view'}">
												
												</c:if>>
																<option value=""></option>
																<c:forEach items="${brokerList}" var="broker">
																	<option value="${broker.id}"
																		<c:if test="${purchaseMaster.brokerId == broker.id}">selected</c:if>>${broker.name}</option>
																</c:forEach>
															</select>
														</div>
													</div>
													<div class="col-lg-2">
														<div class="form-group">
															<label for="company" class=" form-control-label">Brokerage
															</label>
															<div class="input-group">
																<input type="text" id="brokerage"
																	placeholder="Brokerage" class="form-control"
																	value="${purchaseMaster.brokeragePercentage}">
																<div class="input-group-addon">%</div>
															</div>
														</div>
													</div>
													<div class="col-lg-4">
														<div class="form-group">
															<label for="company" class=" form-control-label">Brokerage
																Amount (My Current)</label>
															<div class="input-group">
																<input type="text" id="brokerageAmount"
																	value="${purchaseMaster.brokerageAmount}"
																	placeholder="Brokerage Amount" class="form-control">
																<div id="brokerageAmountUnit" class="input-group-addon">${purchaseMaster.myCurrencyUnit}</div>
															</div>
														</div>
													</div>
												</div>
												<div class="row form-group">
													<div class="col-lg-2">
														<div class="form-group">
															<label for="company" class=" form-control-label">Tax
																Details </label> <select name="purchaseMaster.taxDetails"
																id="taxDetails" onChange="taxChange()"
																class="form-control">
																<option value="0"
																	<c:if test="${purchaseMaster.taxDetails == 0}">selected</c:if>>Without
																	Tax</option>
																<option value="1"
																	<c:if test="${purchaseMaster.taxDetails == 1}">selected</c:if>>IGST</option>
																<option value="2"
																	<c:if test="${purchaseMaster.taxDetails == 2}">selected</c:if>>CGST/SGST</option>
															</select>
														</div>
													</div>
													<c:if test="${purchaseMaster.IGST != 0}">
														<div class="col-lg-2" id="IGSTDiv">
															<div class="form-group">
																<label for="company" class=" form-control-label">IGST</label>
																<input type="text" id="igst" placeholder="Enter IGST"
																	value="${purchaseMaster.IGST}" class="form-control">

															</div>
														</div>
													</c:if>
													<c:if test="${purchaseMaster.CGST != 0}">
														<div class="col-lg-2" id="CGSTDiv">
															<div class="form-group">
																<label for="company" class=" form-control-label">CGST</label>
																<input type="text" id="cgst" placeholder="Enter CGST"
																	value="${purchaseMaster.CGST}" class="form-control">
															</div>
														</div>
													</c:if>
													<c:if test="${purchaseMaster.SGST != 0}">
														<div class="col-lg-2" id="SGSTDiv">
															<div class="form-group">
																<label for="company" class=" form-control-label">SGST</label>
																<input type="text" id="sgst"
																	value="${purchaseMaster.SGST}" placeholder="Enter SGST"
																	class="form-control">
															</div>
														</div>
													</c:if>
												</div>
												<div class="row form-group">
													<div class="col-lg-4">
														<div class="form-group">
															<label for="company" class=" form-control-label">Bill
																Amount (Buying Currency)</label>
															<div class="input-group">
																<input type="text" id="billAmountBuyCurrency"
																	value="${purchaseMaster.billAmountBuyCurrency}"
																	placeholder="Bill Amount" class="form-control">
																<div id="billAmountBuyCurrencyUnit"
																	class="input-group-addon">${purchaseMaster.buyCurrencyUnit}</div>
															</div>
														</div>
													</div>
													<div class="col-lg-4">
														<div class="form-group">
															<label for="company" class=" form-control-label">Bill
																Amount (My Currency)</label>
															<div class="input-group">
																<input type="text" id="billAmountMyCurrency"
																	value="${purchaseMaster.billAmountMyCurrency}"
																	placeholder="Bill Amount" class="form-control">
																<div id="billAmountMyCurrencyUnit"
																	class="input-group-addon">${purchaseMaster.myCurrencyUnit}</div>
															</div>

														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="card-footer">
									<button type="button" class="btn btn-danger btn-sm"
										onClick="javascript:window.close();">
										<i class="fa fa-dot-circle-o"></i> Close
									</button>
								</div>
								<div id="hiddenStock"></div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- 		Bottom Page End -->
			<!-- .content -->
		</div>
		<!-- 	MAIN CODE END HERE -->
		<jsp:include page="../footer.jsp"></jsp:include>
		<script type="text/javascript" src="https://cdn.datatables.net/plug-ins/1.10.19/api/fnFindCellRowIndexes.js"></script>
		<script>
			//chirag
			var totalStock = 0;
			<c:forEach items="${stockList}" var="stock">
				$("#hiddenStock")
						.html(
								$("#hiddenStock").html()
										+ "<input type='hidden' name='stockLst[" + totalStock + "].lotNo' value='${stock.lotNo}'/>");
			
				$("#hiddenStock")
						.html(
								$("#hiddenStock").html()
										+ "<input type='hidden' name='stockLst[" + totalStock + "].shape' value='${stock.shape}'/>");

				$("#hiddenStock")
						.html(
								$("#hiddenStock").html()
										+ "<input type='hidden' name='stockLst[" + totalStock + "].carret' value='${stock.carret}'/>");

				$("#hiddenStock")
						.html(
								$("#hiddenStock").html()
										+ "<input type='hidden' name='stockLst[" + totalStock + "].color' value='${stock.color}'/>");

				$("#hiddenStock")
						.html(
								$("#hiddenStock").html()
										+ "<input type='hidden' name='stockLst[" + totalStock + "].clarity' value='${stock.clarity}'/>");

				$("#hiddenStock")
						.html(
								$("#hiddenStock").html()
										+ "<input type='hidden' name='stockLst[" + totalStock + "].cut' value='${stock.cut}'/>");

				$("#hiddenStock")
						.html(
								$("#hiddenStock").html()
										+ "<input type='hidden' name='stockLst[" + totalStock + "].polish' value='${stock.polish}'/>");

				$("#hiddenStock")
						.html(
								$("#hiddenStock").html()
										+ "<input type='hidden' name='stockLst[" + totalStock + "].symmetry' value='${stock.symmetry}'/>");

				$("#hiddenStock")
						.html(
								$("#hiddenStock").html()
										+ "<input type='hidden' name='stockLst[" + totalStock + "].flColor' value='${stock.flColor}'/>");

				$("#hiddenStock")
						.html(
								$("#hiddenStock").html()
										+ "<input type='hidden' name='stockLst[" + totalStock + "].fl' value='${stock.fl}'/>");

				$("#hiddenStock")
						.html(
								$("#hiddenStock").html()
										+ "<input type='hidden' name='stockLst[" + totalStock + "].diameter' value='${stock.diameter}'/>");


				$("#hiddenStock")
						.html(
								$("#hiddenStock").html()
										+ "<input type='hidden' name='stockLst[" + totalStock + "].certificateType' value='${stock.certificateType}'/>");


				$("#hiddenStock")
						.html(
								$("#hiddenStock").html()
										+ "<input type='hidden' name='stockLst[" + totalStock + "].certificateNo' value='${stock.certificateNo}'/>");


				$("#hiddenStock")
						.html(
								$("#hiddenStock").html()
										+ "<input type='hidden' name='stockLst[" + totalStock + "].withRap' value='${stock.withRap}'/>");


				$("#hiddenStock")
						.html(
								$("#hiddenStock").html()
										+ "<input type='hidden' name='stockLst[" + totalStock + "].rapPrice' value='${stock.rapPrice}'/>");


				$("#hiddenStock")
						.html(
								$("#hiddenStock").html()
										+ "<input type='hidden' name='stockLst[" + totalStock + "].withPremium' value='${stock.withPremium}'/>");


				$("#hiddenStock")
						.html(
								$("#hiddenStock").html()
										+ "<input type='hidden' name='stockLst[" + totalStock + "].backPrice' value='${stock.backPrice}'/>");


				$("#hiddenStock")
						.html(
								$("#hiddenStock").html()
										+ "<input type='hidden' name='stockLst[" + totalStock + "].addDiscount' value='${stock.addDiscount}'/>");


				$("#hiddenStock")
						.html(
								$("#hiddenStock").html()
										+ "<input type='hidden' name='stockLst[" + totalStock + "].volDiscount' value='${stock.volDiscount}'/>");


				$("#hiddenStock")
						.html(
								$("#hiddenStock").html()
										+ "<input type='hidden' name='stockLst[" + totalStock + "].cashDiscount' value='${stock.cashDiscount}'/>");


				$("#hiddenStock")
						.html(
								$("#hiddenStock").html()
										+ "<input type='hidden' name='stockLst[" + totalStock + "].finalPricePerCrt' value='${stock.finalPricePerCrt}'/>");


				$("#hiddenStock")
						.html(
								$("#hiddenStock").html()
										+ "<input type='hidden' name='stockLst[" + totalStock + "].totalPrice' value='${stock.totalPrice}'/>");


				$("#hiddenStock")
						.html(
								$("#hiddenStock").html()
										+ "<input type='hidden' name='stockLst[" + totalStock + "].noBrokerDiscount' value='${stock.noBrokerDiscount}'/>");


				$("#hiddenStock")
						.html(
								$("#hiddenStock").html()
										+ "<input type='hidden' name='stockLst[" + totalStock + "].blindDiscount' value='${stock.blindDiscount}'/>");
				totalStock++;
			</c:forEach>
			//chirag end
		
		
			var totalStock = 0;
			var table = $('.table').DataTable();
			
			 $('.table tfoot th:nth-last-child(n+2)').each( function () {
			     var title = $(this).text();
			     $(this).html( '<input type="text" size="5" placeholder="Search" />' );
			     //$(this).html( '<input type="text" placeholder="Search '+title+'" />' );
			 } );
		   
		   table.columns().every( function () {
			     var that = this;
			     $( 'input', this.footer() ).on( 'keyup change', function () {
			         if ( that.search() !== this.value ) {
			             that
			                 .search( this.value )
			                 .draw();
			         }
			     } );
			 } );

			function viewStock(id)
			{
				window.open("/stock/getStockDetailsById?id=" + id, "_blank");
			}

			function editStock(id)
			{
				if(confirm('Are you sure to edit this lot?'))
				{
					window.open("/stock/getStockDetailsById?id=" + id, "_blank");
				}
			}
			
			function deleteStock(id)
			{
				if(confirm('Are you sure to delete this lot?'))
				{
					var netPriceBuyCurrency = 0;
					var indexes = table.rows().eq( 0 ).filter( function (rowIdx) {
					    return table.cell( rowIdx, 0 ).data() === id ? true : false;
					} );
					
					table.row(indexes).remove().draw(
							false);
					
					table.rows().eq( 0 ).filter( function (rowIdx) {
						netPriceBuyCurrency = netPriceBuyCurrency + table.cell( rowIdx, 5 ).data();
					} );
					
					var findHiddenStockIndex;
					$("input[name$='.lotNo").each(function(index) {
						console.log($( this ).attr('name'));
						if($( this ).val() == id)
						{
							findHiddenStockIndex = $( this ).attr('name').replace("stockLst[","").replace("].lotNo","");
							return;
						}
					});

					$("input[name*='stockLst["+findHiddenStockIndex+"]']" ).remove();
					$("#netPriceBuyCurrency").val(netPriceBuyCurrency);

					$("#netPriceMyCurrency").val($("#netPriceBuyCurrency").val() * $("#conversionRate").val())
				}
			}


			function formatDate(date) {
				var d = new Date(date), month = '' + (d.getMonth() + 1), day = ''
						+ d.getDate(), year = d.getFullYear();

				if (month.length < 2)
					month = '0' + month;
				if (day.length < 2)
					day = '0' + day;

				return [ year, month, day ].join('-');
			}
			
			function setDueDate() {
				var date = new Date($("#purchaseDate").val());
				var days = parseInt($("#terms").val(), 10);

				if (!isNaN(date.getTime())) {
					date.setDate(date.getDate() + days);

					$("#dueDate").val(formatDate(date));
				} else {
					alert("Invalid Date");
				}
			}
		</script>
</body>

</html>