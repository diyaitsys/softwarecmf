<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" lang="">
<!--<![endif]-->

<head>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page isELIgnored="false"%>
<jsp:include page="../header.jsp"></jsp:include>
</head>

<body>
	<jsp:include page="../menu.jsp"></jsp:include>
	<!-- 	MAIN CODE START HERE -->
	<div id="right-panel" class="right-panel">

		<!-- Header-->
		<jsp:include page="../topHeader.jsp"></jsp:include>
		<!-- Header-->

		<div class="content mt-3">
			<div class="animated">
				<div class="row">
					<div class="col-lg-12">
						<div class="card">
							<div class="card-header">
								<strong>Purchase Invoice Details</strong>
							</div>
							<c:if test="${error != null && error != ''}">
								<div
									class="alert with-close alert-danger alert-dismissible fade show">
									<span class="badge badge-pill badge-danger">Error</span>${error}
									<button id="closeModal" type="button" class="close"
										data-dismiss="alert" aria-label="Close">
										<span aria-hidden="true">&times;</span>
									</button>
								</div>
							</c:if>
							<c:if test="${message != null && message != ''}">
								<div
									class="alert with-close alert-success alert-dismissible fade show">
									<span class="badge badge-pill badge-success">Success</span>${message}
									<button type="button" class="close" data-dismiss="alert"
										aria-label="Close">
										<span aria-hidden="true">&times;</span>
									</button>
								</div>
							</c:if>
							<div class="card-body card-block">
								<form action="/purchase/addUpdatePurchase" method="post"
									id="frm" class="form-horizontal">
									<input type="hidden" name="actionMode" value="${actionMode}" />
									<c:if test="${actionMode != 'add'}">
										<input type="hidden" name="purchaseMaster.id"
											value="${purchaseMaster.id}" />
									</c:if>
									<div class="row form-group">
										<div class="col-lg-6">
											<label for="text-input" class=" form-control-label">Party
												Name <span class="required">*</span>
											</label> <select id="partyId" name="purchaseMaster.partyId"
												data-placeholder="Select Party" required
												class="standardSelect" tabindex="1"
												<c:if
											 test="${actionMode == 'view'}">
												readonly
												</c:if>>
												<option value=""></option>
												<c:forEach items="${partyList}" var="party">
													<option value="${party.id}">${party.name}</option>
												</c:forEach>
											</select>
										</div>
										<div class="col-lg-6">
											<div class="form-group">
												<label for="company" class=" form-control-label">Remark</label>
												<input type="text" id="remarks"
													name="purchaseMaster.remarks" placeholder="Enter remark"
													class="form-control">
											</div>
										</div>

									</div>
									<div class="row form-group">

										<div class="col col-lg-3">
											<label for="text-input" class=" form-control-label">InvoiceNo
												<span class="required">*</span>
											</label> <input type="text" id="invoiceNo"
												name="purchaseMaster.invoiceNo" placeholder="Invoice No"
												required value="${purchaseMaster.invoiceNo}"
												class="form-control"
												<c:if test="${actionMode == 'view'}"> readonly
											</c:if>>
										</div>
										<div class="col col-lg-3">
											<label for="text-input" class=" form-control-label">Purchase
												Date <span class="required">*</span>
											</label> <input type="text" id="purchaseDate"
												name="purchaseMaster.purchaseDate" required
												placeholder="Purchase Date"
												value="${purchaseMaster.purchaseDate}"
												class="form-control dtPicker"
												<c:if test="${actionMode == 'view'}"> readonly
											</c:if>>
										</div>
										<div class="col-lg-3">
											<div class="form-group">
												<label for="company" class=" form-control-label">Terms
													(Days) <span class="required">*</span>
												</label> <input type="text" id="terms"
													name="purchaseMaster.termsDays" onChange="setDueDate()"
													required placeholder="Enter Terms" class="form-control">
											</div>
										</div>
										<div class="col col-lg-3">
											<label for="text-input" class=" form-control-label">Due
												Date </label> <input type="text" id="dueDate"
												name="purchaseMaster.dueDate" placeholder="Due Date"
												readonly value="${purchaseMaster.dueDate}"
												class="form-control"
												<c:if test="${actionMode == 'view'}"> readonly
											</c:if>>
										</div>
									</div>
									<div class="row form-group">

										<div class="col-lg-2">
											<div class="form-group">
												<label for="company" class=" form-control-label">Buy
													Currency <span class="required">*</span>
												</label> <select id="buyCurrencyUnit"
													name="purchaseMaster.buyCurrencyUnit"
													data-placeholder="Select Currency" required
													onChange="buyCurrencyChange()" class="form-control"
													<c:if test="${actionMode == 'view'}">
														readonly
													</c:if>>
													<c:forEach items="${currencyMap}" var="currency">
														<option value="${currency.key}">${currency.value}</option>
													</c:forEach>
												</select>
											</div>
										</div>
										<div class="col-lg-2">
											<div class="form-group">
												<label for="company" class=" form-control-label">Convert
													Rate <span class="required">*</span>
												</label> <input type="text" id="conversionRate"
													name="purchaseMaster.conversionRate" onblur="calNetPrice()"
													required placeholder="Convert Rate" class="form-control"
													value="1">
											</div>
										</div>
										<div class="col-lg-2">
											<div class="form-group">
												<label for="company" class=" form-control-label">My
													Currency <span class="required">*</span>
												</label> <select id="myCurrencyUnit"
													name="purchaseMaster.myCurrencyUnit" class="form-control"
													data-placeholder="Select Currency"
													onChange="myCurrencyChange()" required
													<c:if test="${actionMode == 'view'}">
														readonly
													</c:if>>
													<c:forEach items="${currencyMap}" var="currency">
														<option value="${currency.key}">${currency.value}</option>
													</c:forEach>
												</select>
											</div>
										</div>
									</div>

									<div class="row form-group">
										<div class="col col-md-4">
											<button type="button" id="addStockBtn"
												class="btn btn-secondary mb-1" data-toggle="modal"
												data-target="#largeModal">Add Stock</button>
										</div>
										<!-- 										 -->
									</div>

									<!-- 		 Model Start -->
									<div class="modal fade" id="largeModal" tabindex="-1"
										role="dialog" aria-labelledby="largeModalLabel"
										aria-hidden="true">
										<div class="modal-dialog modal-lg" role="document"
											style="margin-left: 100px;">
											<div class="modal-content" style="width: 1150px;">
												<div class="modal-header">
													<h5 class="modal-title" id="largeModalLabel">Add Stock
														Modal</h5>
													<button type="button" class="close" data-dismiss="modal"
														aria-label="Close">
														<span aria-hidden="true">&times;</span>
													</button>
												</div>
												<div class="modal-body">
													<div class="row">
														<div class="col-lg-12">
															<div class="row form-group">
																<div class="col col-lg-2">
																	<label for="text-input" class=" form-control-label">Lot
																		No <span class="required">*</span>
																	</label> <input type="text" id="lotNo" value="${stock.lotNo}"
																		placeholder="Lot No" class="form-control">
																</div>
																<div class="col col-md-2">
																	<label for="select" class=" form-control-label">Shape
																		<span class="required">*</span>
																	</label>
																	<jsp:include page="shape.jsp"></jsp:include>
																</div>

																<div class="col col-md-2">
																	<label for="text-input" class=" form-control-label">Carat
																		<span class="required">*</span>
																	</label> <input type="text" id="carret"
																		onChange="calTotalPrice()" value=""
																		placeholder="Carat" class="form-control">
																</div>

																<div class="col col-md-2">
																	<label for="select" class=" form-control-label">Color
																		<span class="required">*</span>
																	</label>
																	<jsp:include page="color.jsp"></jsp:include>
																</div>

																<div class="col col-md-2">
																	<label for="select" class=" form-control-label">Clarity
																		<span class="required">*</span>
																	</label>
																	<jsp:include page="clarity.jsp"></jsp:include>
																</div>
															</div>

															<div class="row form-group">
																<div class="col col-md-2">
																	<label for="select" class=" form-control-label">Cut
																	</label>
																	<jsp:include page="cut.jsp"></jsp:include>
																</div>

																<div class="col col-md-2">
																	<label for="select" class=" form-control-label">Polish</label>
																	<jsp:include page="polish.jsp"></jsp:include>
																</div>

																<div class="col col-md-2">
																	<label for="select" class=" form-control-label">Symmetry</label>
																	<jsp:include page="symmetry.jsp"></jsp:include>
																</div>

																<div class="col col-md-2">
																	<label for="select" class=" form-control-label">FL</label>
																	<jsp:include page="../purchase/fl.jsp"></jsp:include>
																</div>

																<div class="col col-md-2">
																	<label for="select" class=" form-control-label">FL
																		Color </label>
																	<jsp:include page="../purchase/fluorescenceColor.jsp"></jsp:include>
																</div>

																<div class="col col-lg-2">
																	<label for="text-input" class=" form-control-label">Diameter
																	</label>
																	<div class="input-group">
																		<input type="text" id="diameter" name="diameter"
																			value="" placeholder="Diameter" class="form-control">
																		<div class="input-group-addon">MM</div>
																	</div>
																</div>
															</div>

															<div class="row form-group">

																<div class="col col-md-3">
																	<label for="select" class=" form-control-label">Certificate
																		Type </label>
																	<jsp:include page="certificateType.jsp"></jsp:include>
																</div>

																<div class="col col-lg-3">
																	<label for="text-input" class=" form-control-label">Certificate
																		No </label> <input type="text" id="certificateNo"
																		name="certificateNo" value=""
																		placeholder="Enter Certificate No"
																		class="form-control">
																</div>

																<div class="col col-md-4">
																	<label class=" form-control-label">WithRap</label>
																	<div class="form-check-inline form-check">
																		<input id="withRap" name="withRap" value="1" checked
																			onChange="withRapClicked()" class="form-check-input"
																			type="checkbox">
																	</div>
																</div>
															</div>

															<div class="row form-group">
																<div class="col col-md-2">
																	<button id="rapPriceBtn" type="button"
																		onClick="getRapPriceOnline()"
																		class="btn btn-primary btn-sm">
																		<i class="fa fa-dot-circle-o"></i> Get Rap Price
																	</button>
																	<div class="input-group">
																		<input type="text" id="rapPrice" name="rapPrice"
																			onChange="calTotalPrice()" value=""
																			placeholder="Rap Price" class="form-control">
																		<div id="rapPriceUnit" class="input-group-addon">USD</div>
																	</div>
																</div>

																<div class="col col-md-2">
																	<label class=" form-control-label">Disc/Prem</label>
																	<div class="form-check">
																		<div class="radio">
																			<label for="radio1" class="form-check-label">
																				<input id="discountRadio" name="withPremium"
																				onChange="calTotalPrice()" value="discount"
																				class="form-check-input" type="radio"
																				checked="checked" onClick="checkDiscPrem()">Discount
																			</label>
																		</div>
																		<div class="radio">
																			<label for="radio2" class="form-check-label ">
																				<input id="premiumRadio" name="withPremium"
																				onChange="calTotalPrice()" onClick="checkDiscPrem()"
																				value="premium" class="form-check-input"
																				type="radio">Premium
																			</label>
																		</div>
																	</div>
																</div>
															</div>

															<div class="row form-group">
																<div class="col col-lg-2">
																	<label id="discPremLabel" for="text-input"
																		class=" form-control-label">Discount </label>
																	<div class="input-group">
																		<input id="discount" name="discount"
																			onBlur="calTotalPrice()" placeholder="Discount"
																			class="form-control" type="text">
																		<div class="input-group-addon">%</div>
																	</div>
																</div>

																<div class="col col-lg-2">
																	<label for="text-input" class=" form-control-label">Delivery
																		Disc.</label>
																	<div class="input-group">
																		<input id="addDiscount" name="addDiscount"
																			onBlur="calTotalPrice()" placeholder="Delivery"
																			class="form-control" type="text">
																		<div class="input-group-addon">%</div>
																	</div>
																</div>

																<div class="col col-lg-2">
																	<label for="text-input" class=" form-control-label">Blind
																		Disc.</label>
																	<div class="input-group">
																		<input id="blindDiscount" name="blindDiscount"
																			onBlur="calTotalPrice()" placeholder="Blind"
																			class="form-control" type="text">
																		<div class="input-group-addon">%</div>
																	</div>
																</div>

																<div class="col col-lg-2">
																	<label for="text-input" class=" form-control-label">No
																		Broker Disc.</label>
																	<div class="input-group">
																		<input id="noBrokerDiscount" name="noBrokerDiscount"
																			onBlur="calTotalPrice()" placeholder="No Broker"
																			class="form-control" type="text">
																		<div class="input-group-addon">%</div>
																	</div>
																</div>

																<div class="col col-lg-2">
																	<label for="text-input" class=" form-control-label">Vol
																		Disc.</label>
																	<div class="input-group">
																		<input id="volDiscount" name="volDiscount"
																			onBlur="calTotalPrice()" placeholder="Volume"
																			class="form-control" type="text">
																		<div class="input-group-addon">%</div>
																	</div>
																</div>

																<div class="col col-lg-2">
																	<label for="text-input" class=" form-control-label">Adv.
																		Pay Disc. </label>
																	<div class="input-group">
																		<input id="cashDiscount" name="cashDiscount"
																			onBlur="calTotalPrice()" placeholder="Cash"
																			class="form-control" type="text">
																		<div class="input-group-addon">%</div>
																	</div>
																</div>
															</div>

															<div class="row form-group">
																<div class="col col-lg-3">
																	<label for="text-input" class=" form-control-label">Final
																		Price(CRT) </label>
																	<div class="input-group">
																		<input type="text" id="finalPricePerCrt"
																			name="finalPricePerCrt" readonly value=""
																			placeholder="Final Price" class="form-control">
																		<div id="finalPriceUnit" class="input-group-addon">USD</div>
																	</div>
																</div>

																<div class="col col-lg-3">
																	<label for="text-input" class=" form-control-label">Total
																		Price </label>
																	<div class="input-group">
																		<input type="text" id="totalPrice" name="totalPrice"
																			readonly value="" placeholder="Total Price"
																			class="form-control">
																		<div id="totalPriceUnit" class="input-group-addon">USD</div>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
												<div class="modal-footer">
													<button type="button" class="btn btn-secondary"
														data-dismiss="modal" data-backdrop="false"
														aria-hidden="true">Cancel</button>
													<button type="button" class="btn btn-primary"
														onClick="modelSubmit();return false;">Confirm</button>
												</div>
											</div>
										</div>
									</div>
									<!-- 		 Model End -->
									<!-- .animated -->

									<!-- 		Datatable Start -->
									<div class="content mt-3">
										<div class="animated fadeIn">
											<div class="row">
												<div class="col-md-12">
													<div class="card">
														<div class="card-header">
															<strong class="card-title">Stock Details</strong>
														</div>
														<div class="card-body">
															<table id="dataTable"
																class="table table-striped table-bordered">
																<thead>
																	<tr>
																		<th>Lot No</th>
																		<th>Shape</th>
																		<th>Carat</th>
																		<th>Color</th>
																		<th>Clarity</th>
																		<th>Cut</th>
																		<th>Polish</th>
																		<th>Symmetry</th>
																		<th>FL Color</th>
																		<th>FL</th>
																		<th>Diameter</th>
																		<th>Certificate Type</th>
																		<th>Certificate No</th>
																		<th>WithRap</th>
																		<th>Rap Price</th>
																		<th>isDiscount</th>
																		<th>Back</th>
																		<th>Add. Disc</th>
																		<th>Vol Disc</th>
																		<th>Cash Discount</th>
																		<th>Final Price(CRT)</th>
																		<th>Total Price</th>
																		<th>No Broker</th>
																		<th>Blind Disc</th>
																		<th></th>
																	</tr>
																</thead>
															</table>
														</div>
													</div>
												</div>
											</div>
										</div>
										<!-- .animated -->
									</div>

									<!-- 		Datatable End -->

									<!-- 		Bottom Page Start -->
									<div class="content mt-3">
										<div class="animated fadeIn">
											<div class="card">
												<div class="card-body">
													<div class="row form-group">
														<div class="col-lg-3">
															<div class="form-group">
																<label for="company" class=" form-control-label">Net
																	Price (Buy Currency) </label>
																<div class="input-group">
																	<input type="text" id="netPriceBuyCurrency"
																		name="purchaseMaster.netPriceBuyCurrency" readonly
																		placeholder="Net Price" class="form-control">
																	<div id="netPriceBuyCurrentUnit"
																		class="input-group-addon">USD</div>
																</div>
															</div>
														</div>
														<div class="col-lg-3">
															<div class="form-group">
																<label for="company" class=" form-control-label">Net
																	Price (My Currency) </label>
																<div class="input-group">
																	<input type="text" id="netPriceMyCurrency"
																		name="purchaseMaster.netPriceMyCurrency" readonly
																		placeholder="Net Price" class="form-control">
																	<div id="netPriceMyCurrencyUnit"
																		class="input-group-addon">USD</div>
																</div>
															</div>
														</div>
													</div>
													<div class="row form-group">
														<div class="col-lg-4">
															<div class="form-group">
																<label for="company" class=" form-control-label">Broker
																	Name </label> <select name="purchaseMaster.brokerId"
																	data-placeholder="Select Broker" class="standardSelect"
																	tabindex="1"
																	<c:if test="${actionMode == 'view'}">
												readonly
												</c:if>>
																	<option value=""></option>
																	<c:forEach items="${brokerList}" var="broker">
																		<option value="${broker.id}">${broker.name}</option>
																	</c:forEach>
																</select>
															</div>
														</div>
														<div class="col-lg-2">
															<div class="form-group">
																<label for="company" class=" form-control-label">Brokerage
																</label>
																<div class="input-group">
																	<input type="text" id="brokerage"
																		placeholder="Brokerage" class="form-control"
																		name="purchaseMaster.brokeragePercentage"
																		onBlur="calBrokerage()">
																	<div class="input-group-addon">%</div>
																</div>
															</div>
														</div>
														<div class="col-lg-4">
															<div class="form-group">
																<label for="company" class=" form-control-label">Brokerage
																	Amount (My Current)</label>
																<div class="input-group">
																	<input type="text" id="brokerageAmount"
																		name="purchaseMaster.brokerageAmount"
																		placeholder="Brokerage Amount" class="form-control"
																		readonly>
																	<div id="brokerageAmountUnit" class="input-group-addon">USD</div>
																</div>
															</div>
														</div>
													</div>
													<div class="row form-group">
														<div class="col-lg-2">
															<div class="form-group">
																<label for="company" class=" form-control-label">Tax
																	Details </label> <select name="purchaseMaster.taxDetails"
																	id="taxDetails" onChange="taxChange()"
																	class="form-control">
																	<option value="0">Without Tax</option>
																	<option value="1">IGST</option>
																	<option value="2">CGST/SGST</option>
																</select>
															</div>
														</div>
														<div class="col-lg-2" id="IGSTDiv" style="display: none">
															<div class="form-group">
																<label for="company" class=" form-control-label">IGST</label>
																<input type="text" id="igst" placeholder="Enter IGST"
																	name="purchaseMaster.IGST" class="form-control"
																	readonly>

															</div>
														</div>
														<div class="col-lg-2" id="CGSTDiv" style="display: none">
															<div class="form-group">
																<label for="company" class=" form-control-label">CGST</label>
																<input type="text" id="cgst" placeholder="Enter CGST"
																	name="purchaseMaster.CGST" class="form-control"
																	readonly>
															</div>
														</div>
														<div class="col-lg-2" id="SGSTDiv" style="display: none">
															<div class="form-group">
																<label for="company" class=" form-control-label">SGST</label>
																<input type="text" id="sgst" name="purchaseMaster.SGST"
																	placeholder="Enter SGST" class="form-control" readonly>
															</div>
														</div>
													</div>
													<div class="row form-group">
														<div class="col-lg-4">
															<div class="form-group">
																<label for="company" class=" form-control-label">Bill
																	Amount (Buying Currency)</label>
																<div class="input-group">
																	<input type="text" id="billAmountBuyCurrency"
																		name="purchaseMaster.billAmountBuyCurrency" readonly
																		placeholder="Bill Amount" class="form-control">
																	<div id="billAmountBuyCurrencyUnit"
																		class="input-group-addon">USD</div>
																</div>
															</div>
														</div>
														<div class="col-lg-4">
															<div class="form-group">
																<label for="company" class=" form-control-label">Bill
																	Amount (My Currency)</label>
																<div class="input-group">
																	<input type="text" id="billAmountMyCurrency"
																		name="purchaseMaster.billAmountMyCurrency" readonly
																		placeholder="Bill Amount" class="form-control">
																	<div id="billAmountMyCurrencyUnit"
																		class="input-group-addon">USD</div>
																</div>

															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="card-footer">
										<c:if test="${actionMode != 'view'}">
											<button type="submit" class="btn btn-primary btn-sm">
												<i class="fa fa-dot-circle-o"></i> Submit
											</button>
										</c:if>
										<button type="button" class="btn btn-danger btn-sm"
											onClick="javascript:closeBtn();">
											<i class="fa fa-dot-circle-o"></i> Close
										</button>
									</div>
									<div id="hiddenStock"></div>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- 		Bottom Page End -->
			<!-- .content -->
		</div>
	</div>
	<!-- 	MAIN CODE END HERE -->
	<jsp:include page="../footer.jsp"></jsp:include>
	<script>
		var totalStock = 0;
		var dtTable;
		dtTable = $('#dataTable').DataTable(
				{
					"columnDefs" : [ {
						"targets" : [ 5, 6, 7, 8, 9, 10, 11, 12, 13, 15, 18,
								19, 20, 22, 23 ],
						"visible" : false,
						"searchable" : false
					} ]
				});

		$(document)
				.ready(
						function() {

							$("#addStockBtn").click(function() {
								clearModelFields();
							});

							$("#netPriceBuyCurrency").val("0.0");
							$("#netPriceMyCurrency").val("0.0");

							$("#billAmountBuyCurrency").val("0.0");
							$("#billAmountMyCurrency").val("0.0");

							$('form')
									.submit(
											function(e) {
												e.preventDefault();
												var validLotNo = false;
												var validInvoiceNo = false;

												$("input[name$='.lotNo']")
														.each(
																function(index,
																		value) {

																	var lotNoCheck = $(
																			this)
																			.val();
																	$
																			.ajax({
																				type : 'GET',
																				async : false,
																				url : '/purchase/checkLotNoExists?lotNo='
																						+ lotNoCheck,
																				success : function(
																						data,
																						status) {
																					//alert(data);
																					if (data == "ERROR") {
																						alert("ERROR!!! Lot No : "
																								+ lotNoCheck
																								+ " Already Exists.");
																						//return false;
																					} else {
																						validLotNo = true;
																					}
																				}
																			});
																});

												$
														.ajax({
															type : 'GET',
															async : false,
															url : '/purchase/checkInvoiceNoExists?invoiceNo='
																	+ $(
																			"#invoiceNo")
																			.val(),
															success : function(
																	data,
																	status) {
																//alert(data);
																if (data == "ERROR") {
																	alert("ERROR!!! Invoice No : "
																			+ $(
																					"#invoiceNo")
																					.val()
																			+ " Already Exists.");
																	//return false;
																} else {
																	validInvoiceNo = true;
																}
															}
														});

												if (validInvoiceNo
														&& validLotNo) {
													this.submit();
												} else {
													return false;
												}

											});
						});

		function calBrokerage() {
			if ($("#brokerage").val() == "") {
				return false;
			}

			var brokerage = parseFloat($("#brokerage").val());

			var netPriceMyCurrency = parseFloat($("#netPriceMyCurrency").val());

			$("#brokerageAmount").val(
					parseFloat(netPriceMyCurrency * (brokerage / 100)).toFixed(
							2));

		}

		function modelSubmit() {
			var lotNo = document.getElementById('lotNo').value;
			if (lotNo == "") {
				alert("Please Enter LotNo");
				return false;
			}

			$("input[name$='.lotNo']").each(function(index, value) {
				if ($(this).val() == lotNo) {
					alert("Lot No : " + lotNo + " Already Exists");
					return false;
				}
			});

			var flag = false;
			$
					.ajax({
						type : 'GET',
						async : false,
						url : '/purchase/checkLotNoExists?lotNo=' + lotNo,
						success : function(data, status) {
							//alert(data);
							if (data == "ERROR") {
								alert("ERROR!!! Lot No : " + lotNo
										+ " Already Exists.");
								flag = true;
								return false;
							}
						}
					});

			if (flag) {
				return false;
			}
			var shape = document.getElementById('shape').value;
			if (shape == "-1") {
				alert("Please Enter shape");
				return false;
			}
			var carret = document.getElementById('carret').value;
			if (carret == "") {
				alert("Please Enter carret");
				return false;
			}
			var color = document.getElementById('color').value;
			if (color == "-1") {
				alert("Please Enter color");
				return false;
			}
			var clarity = document.getElementById('clarity').value;
			if (clarity == "-1") {
				alert("Please Enter clarity");
				return false;
			}
			var rapPrice = document.getElementById('rapPrice').value;
			if (rapPrice == "") {
				alert("Please Enter rapPrice");
				return false;
			}
			var totalPrice = document.getElementById('totalPrice').value;
			if (totalPrice == "") {
				alert("Please Enter totalPrice");
				return false;
			}

			var cut = document.getElementById('cut').value;
			var polish = document.getElementById('polish').value;
			var symmetry = document.getElementById('symmetry').value;
			var flColor = document.getElementById('flColor').value;
			var fl = document.getElementById('fl').value;
			var diameter = document.getElementById('diameter').value;
			var certificateType = document.getElementById('certificateType').value;
			var certificateNo = document.getElementById('certificateNo').value;
			var withRap = document.getElementById('withRap').value;
			var withPremium = 0;
			if (!document.getElementById("discountRadio").checked) {
				withPremium = 1;
			}
			var backPrice = document.getElementById('discount').value;
			var addDiscount = document.getElementById('addDiscount').value;
			var volDiscount = document.getElementById('volDiscount').value;
			var cashDiscount = document.getElementById('cashDiscount').value;

			var noBrokerDiscount = document.getElementById('noBrokerDiscount').value;
			var blindDiscount = document.getElementById('blindDiscount').value;

			var finalPricePerCrt = document.getElementById('finalPricePerCrt').value;

			//alert("ALL FINE");
			var addRow = "<tr data-my_id='" + totalStock +"'>";

			addRow = addRow + "<td>" + lotNo + "</td>";

			$("#hiddenStock")
					.html(
							$("#hiddenStock").html()
									+ "<input type='hidden' name='stockLst[" + totalStock + "].lotNo' value='" + lotNo + "'/>");

			addRow = addRow + "<td>" + shape + "</td>";

			$("#hiddenStock")
					.html(
							$("#hiddenStock").html()
									+ "<input type='hidden' name='stockLst[" + totalStock + "].shape' value='" + shape + "'/>");

			addRow = addRow + "<td>" + carret + "</td>";

			$("#hiddenStock")
					.html(
							$("#hiddenStock").html()
									+ "<input type='hidden' name='stockLst[" + totalStock + "].carret' value='" + carret + "'/>");

			addRow = addRow + "<td>" + color + "</td>";

			$("#hiddenStock")
					.html(
							$("#hiddenStock").html()
									+ "<input type='hidden' name='stockLst[" + totalStock + "].color' value='" + color + "'/>");

			addRow = addRow + "<td>" + clarity + "</td>";

			$("#hiddenStock")
					.html(
							$("#hiddenStock").html()
									+ "<input type='hidden' name='stockLst[" + totalStock + "].clarity' value='" + clarity + "'/>");

			addRow = addRow + "<td>" + cut + "</td>";

			$("#hiddenStock")
					.html(
							$("#hiddenStock").html()
									+ "<input type='hidden' name='stockLst[" + totalStock + "].cut' value='" + cut + "'/>");

			addRow = addRow + "<td>" + polish + "</td>";

			$("#hiddenStock")
					.html(
							$("#hiddenStock").html()
									+ "<input type='hidden' name='stockLst[" + totalStock + "].polish' value='" + polish + "'/>");

			addRow = addRow + "<td>" + symmetry + "</td>";

			$("#hiddenStock")
					.html(
							$("#hiddenStock").html()
									+ "<input type='hidden' name='stockLst[" + totalStock + "].symmetry' value='" + symmetry + "'/>");

			addRow = addRow + "<td>" + flColor + "</td>";

			$("#hiddenStock")
					.html(
							$("#hiddenStock").html()
									+ "<input type='hidden' name='stockLst[" + totalStock + "].flColor' value='" + flColor + "'/>");

			addRow = addRow + "<td>" + fl + "</td>";

			$("#hiddenStock")
					.html(
							$("#hiddenStock").html()
									+ "<input type='hidden' name='stockLst[" + totalStock + "].fl' value='" + fl + "'/>");

			addRow = addRow + "<td>" + diameter + "</td>";

			$("#hiddenStock")
					.html(
							$("#hiddenStock").html()
									+ "<input type='hidden' name='stockLst[" + totalStock + "].diameter' value='" + diameter + "'/>");

			addRow = addRow + "<td>" + certificateType + "</td>";

			$("#hiddenStock")
					.html(
							$("#hiddenStock").html()
									+ "<input type='hidden' name='stockLst[" + totalStock + "].certificateType' value='" + certificateType + "'/>");

			addRow = addRow + "<td>" + certificateNo + "</td>";

			$("#hiddenStock")
					.html(
							$("#hiddenStock").html()
									+ "<input type='hidden' name='stockLst[" + totalStock + "].certificateNo' value='" + certificateNo + "'/>");

			addRow = addRow + "<td>" + withRap + "</td>";

			$("#hiddenStock")
					.html(
							$("#hiddenStock").html()
									+ "<input type='hidden' name='stockLst[" + totalStock + "].withRap' value='" + withRap + "'/>");

			addRow = addRow + "<td>" + rapPrice + "</td>";

			$("#hiddenStock")
					.html(
							$("#hiddenStock").html()
									+ "<input type='hidden' name='stockLst[" + totalStock + "].rapPrice' value='" + rapPrice + "'/>");

			addRow = addRow + "<td>" + withPremium + "</td>";

			$("#hiddenStock")
					.html(
							$("#hiddenStock").html()
									+ "<input type='hidden' name='stockLst[" + totalStock + "].withPremium' value='" + withPremium + "'/>");

			addRow = addRow + "<td>" + backPrice + "</td>";

			$("#hiddenStock")
					.html(
							$("#hiddenStock").html()
									+ "<input type='hidden' name='stockLst[" + totalStock + "].backPrice' value='" + backPrice + "'/>");

			addRow = addRow + "<td>" + addDiscount + "</td>";

			$("#hiddenStock")
					.html(
							$("#hiddenStock").html()
									+ "<input type='hidden' name='stockLst[" + totalStock + "].addDiscount' value='" + addDiscount + "'/>");

			addRow = addRow + "<td>" + volDiscount + "</td>";

			$("#hiddenStock")
					.html(
							$("#hiddenStock").html()
									+ "<input type='hidden' name='stockLst[" + totalStock + "].volDiscount' value='" + volDiscount + "'/>");

			addRow = addRow + "<td>" + cashDiscount + "</td>";

			$("#hiddenStock")
					.html(
							$("#hiddenStock").html()
									+ "<input type='hidden' name='stockLst[" + totalStock + "].cashDiscount' value='" + cashDiscount + "'/>");

			addRow = addRow + "<td>" + finalPricePerCrt + "</td>";

			$("#hiddenStock")
					.html(
							$("#hiddenStock").html()
									+ "<input type='hidden' name='stockLst[" + totalStock + "].finalPricePerCrt' value='" + finalPricePerCrt + "'/>");

			addRow = addRow + "<td>" + totalPrice + "</td>";

			$("#hiddenStock")
					.html(
							$("#hiddenStock").html()
									+ "<input type='hidden' name='stockLst[" + totalStock + "].totalPrice' value='" + totalPrice + "'/>");

			addRow = addRow + "<td>" + noBrokerDiscount + "</td>";

			$("#hiddenStock")
					.html(
							$("#hiddenStock").html()
									+ "<input type='hidden' name='stockLst[" + totalStock + "].noBrokerDiscount' value='" + noBrokerDiscount + "'/>");

			addRow = addRow + "<td>" + blindDiscount + "</td>";

			$("#hiddenStock")
					.html(
							$("#hiddenStock").html()
									+ "<input type='hidden' name='stockLst[" + totalStock + "].blindDiscount' value='" + blindDiscount + "'/>");

			addRow = addRow
					+ "<td>"
					//+ "<button type='button' onClick='editStock("
					//+ totalStock
					//+ ")' class='btn btn-outline-primary btn-sm'>Update</button>"
					+ "<button type='button' onClick='deleteStock("
					+ totalStock
					+ ")' class='btn btn-outline-danger btn-sm'>Delete</button></td>";

			addRow = addRow + "</tr>";

			// 		<th>Add. Disc</th>
			// 		<th>Vol Disc</th>
			// 		<th>Cash Discount</th>
			// 		<th>Final Price(CRT)</th>
			// 		<th>Total Price</th>
			dtTable.row.add($(addRow)).draw();
			totalStock = totalStock + 1;
			//$("table tbody").append(addRow);
			clearModelFields();

			calNetPrice();
			modelClose();
		}

		function deleteStock(stockId) {
			if (confirm('Are you sure to delete this lot?')) {
				dtTable.row("tr[data-my_id='" + stockId + "']").remove().draw(
						false);

				$("input[name^='stockLst[" + stockId + "]']").remove();

				calNetPrice();
			}
		}

		function calNetPrice() {
			var netPriceBuyCurrency = 0;
			$("input[name$='.totalPrice']").each(
					function(index, value) {
						netPriceBuyCurrency = parseFloat(netPriceBuyCurrency
								+ parseFloat($(this).val()));
					});

			$("#netPriceBuyCurrency").val(
					parseFloat(netPriceBuyCurrency).toFixed(2));

			var conversionRate = parseFloat($("#conversionRate").val());

			$("#netPriceMyCurrency").val(
					parseFloat((netPriceBuyCurrency) * conversionRate).toFixed(
							2));

			$("#billAmountMyCurrency").val($("#netPriceMyCurrency").val());
			$("#billAmountBuyCurrency").val($("#netPriceBuyCurrency").val());

			taxChange();
			calBrokerage();
		}

		function closeBtn() {
			window.history.back();
		}

		function clearModelFields() {
			document.getElementById('lotNo').value = "";

			document.getElementById("shape").selectedIndex = 1;
			document.getElementById('carret').value = "";

			document.getElementById("color").selectedIndex = 0;
			document.getElementById("clarity").selectedIndex = 0;
			document.getElementById('rapPrice').value = "0";
			document.getElementById('totalPrice').value = "0";

			document.getElementById("cut").selectedIndex = 0;
			document.getElementById("polish").selectedIndex = 0;
			document.getElementById("symmetry").selectedIndex = 0;
			document.getElementById("flColor").selectedIndex = 0;
			document.getElementById("fl").selectedIndex = 0;

			document.getElementById('diameter').value = "";
			document.getElementById("certificateType").selectedIndex = 0;
			document.getElementById('certificateNo').value = "";

			document.getElementById("discountRadio").checked = true;

			document.getElementById('discount').value = "0";
			document.getElementById('addDiscount').value = "0";
			document.getElementById('volDiscount').value = "0";
			document.getElementById('cashDiscount').value = "0";
			document.getElementById('finalPricePerCrt').value = "0";
			document.getElementById('noBrokerDiscount').value = "0";
			document.getElementById('blindDiscount').value = "0";
		}

		function getRapPriceOnline() {
			var shape = document.getElementById('shape').value;
			var carret = document.getElementById('carret').value;
			var color = document.getElementById('color').value;
			var clarity = document.getElementById('clarity').value;

			if (shape == -1) {
				alert("Please Select Shape");
				return;
			}
			if (color == -1) {
				alert("Please Select Color");
				return;
			}
			if (clarity == -1) {
				alert("Please Select Purity");
				return;
			}
			if (carret == '') {
				alert("Please Select Carret");
				return;
			}

			document.getElementById('rapPrice').value = "";
			$.ajax({
				type : 'GET',
				url : '/rapnet/getRapPriceOnline?shape=' + shape + "&color="
						+ color + "&clarity=" + clarity + "&size=" + carret,

				success : function(data, status) {
					//alert(data);
					if (data == "ERROR") {
						alert("ERROR!!! Please Check RapNet Username/Password")
					} else {
						document.getElementById('rapPrice').value = data;
						calTotalPrice();
					}
				}
			});

			//$("#rapPrice").val()
		}

		function openModel() {
			//$("button[data-toggle=\"modal\" data-target=\"#largeModal\"]").click();
			//$('body').css('padding-right', '');
			$("#largeModal").model('show');
			//$(".modal").show();
			//$('#largeModal').appendTo("body").modal('show');
		}
		function modelClose() {
			//$("button[data-dismiss=\"modal\"]").click();
			$(".modal").removeClass("in");
			$(".modal-backdrop").remove();
			$('body').removeClass('modal-open');
			$('body').css('padding-right', '');
			$(".modal").hide();
			//	$(".alert-danger").click();
			// $('.modal').modal('hide');
		}

		function calTotalPrice() {
			var carret = document.getElementById('carret').value;
			if (carret == "") {
				carret = 0;
			}
			var rapPrice = document.getElementById('rapPrice').value;
			if (rapPrice == "") {
				rapPrice = 0;
			}
			var isDiscount = 1;
			if (!document.getElementById("discountRadio").checked) {
				isDiscount = 0;
			}

			var addDiscount = document.getElementById('addDiscount').value;
			if (addDiscount == "") {
				document.getElementById('addDiscount').value = 0;
				addDiscount = 0;
			}

			var volumnDiscount = document.getElementById('volDiscount').value;
			if (volumnDiscount == "") {
				document.getElementById('volDiscount').value = 0;
				volumnDiscount = 0;
			}

			var discount = document.getElementById('discount').value;
			if (discount == "") {
				document.getElementById('discount').value = 0;
				discount = 0;
			}

			var cashDiscount = document.getElementById('cashDiscount').value;
			if (cashDiscount == "") {
				document.getElementById('cashDiscount').value = 0;
				cashDiscount = 0;
			}

			var noBrokerDiscount = document.getElementById('noBrokerDiscount').value;
			if (noBrokerDiscount == "") {
				document.getElementById('noBrokerDiscount').value = 0;
				noBrokerDiscount = 0;
			}

			var blindDiscount = document.getElementById('blindDiscount').value;
			if (blindDiscount == "") {
				document.getElementById('blindDiscount').value = 0;
				blindDiscount = 0;
			}

			var finalPricePerCrt = 0;
			if (isDiscount == 1) {
				finalPricePerCrt = parseFloat(rapPrice)
						- parseFloat(rapPrice * discount / 100);
			} else {
				finalPricePerCrt = parseFloat(rapPrice)
						+ parseFloat((rapPrice * discount) / 100);
			}

			finalPricePerCrt = parseFloat(finalPricePerCrt)
					- parseFloat(finalPricePerCrt * addDiscount / 100);

			finalPricePerCrt = parseFloat(finalPricePerCrt)
					- parseFloat(finalPricePerCrt * blindDiscount / 100);

			finalPricePerCrt = parseFloat(finalPricePerCrt)
					- parseFloat(finalPricePerCrt * noBrokerDiscount / 100);

			finalPricePerCrt = parseFloat(finalPricePerCrt)
					- parseFloat(finalPricePerCrt * volumnDiscount / 100);

			finalPricePerCrt = parseFloat(finalPricePerCrt)
					- parseFloat(finalPricePerCrt * cashDiscount / 100);

			document.getElementById('finalPricePerCrt').value = parseFloat(
					finalPricePerCrt).toFixed(2);
			document.getElementById('totalPrice').value = parseFloat(
					finalPricePerCrt * carret).toFixed(2);
		}

		function formatDate(date) {
			var d = new Date(date), month = '' + (d.getMonth() + 1), day = ''
					+ d.getDate(), year = d.getFullYear();

			if (month.length < 2)
				month = '0' + month;
			if (day.length < 2)
				day = '0' + day;

			return [ year, month, day ].join('-');
		}

		function setDueDate() {
			var date = new Date($("#purchaseDate").val());
			var days = parseInt($("#terms").val(), 10);

			if (!isNaN(date.getTime())) {
				date.setDate(date.getDate() + days);

				$("#dueDate").val(formatDate(date));
			} else {
				alert("Invalid Date");
			}
		}

		function checkDiscPrem() {
			if (document.getElementById("discountRadio").checked) {
				document.getElementById("discPremLabel").innerHTML = "Discount";
			} else {
				document.getElementById("discPremLabel").innerHTML = "Premium";
			}
		}
		function buyCurrencyChange() {
			var buyCurrencyUnitVal = $("#buyCurrencyUnit").val();
			document.getElementById("finalPriceUnit").innerHTML = buyCurrencyUnitVal;
			document.getElementById("totalPriceUnit").innerHTML = buyCurrencyUnitVal;
			document.getElementById("rapPriceUnit").innerHTML = buyCurrencyUnitVal;
			document.getElementById("netPriceBuyCurrentUnit").innerHTML = buyCurrencyUnitVal;
			document.getElementById("billAmountBuyCurrencyUnit").innerHTML = buyCurrencyUnitVal;
		}

		function myCurrencyChange() {
			var myCurrencyUnitVal = $("#myCurrencyUnit").val();
			document.getElementById("netPriceMyCurrencyUnit").innerHTML = myCurrencyUnitVal;
			document.getElementById("billAmountMyCurrencyUnit").innerHTML = myCurrencyUnitVal;
			document.getElementById("brokerageAmountUnit").innerHTML = myCurrencyUnitVal;
		}

		function withRapClicked() {
			if (document.getElementById("withRap").checked) {
				document.getElementById('rapPriceBtn').style.display = '';
			} else {
				document.getElementById('rapPriceBtn').style.display = 'none';
			}
		}
		function taxChange() {
			var valueSelected = $('#taxDetails').val();
			//var myPurchasePrice = $('#myPurchasePrice').val();
			//var IGSTPer = parseFloat($('#IGSTPer').val());
			//var CGSTPer = parseFloat($('#CGSTPer').val());
			//var SGSTPer = parseFloat($('#SGSTPer').val());
			var netPriceMyCurrency = parseFloat($("#netPriceMyCurrency").val());
			var billAmountMyCurrency = parseFloat($("#billAmountMyCurrency")
					.val());

			if (valueSelected == 0) {
				$('#IGSTDiv').hide();
				$('#SGSTDiv').hide();
				$('#CGSTDiv').hide();

				$("#billAmountMyCurrency").val(
						parseFloat((netPriceMyCurrency)).toFixed(2));

				$('#SGST').val("0.0");
				$('#CGST').val("0.0");
				$('#IGST').val("0.0");
				//$("#netPriceAfterTax").val(
				//		parseFloat(myPurchasePrice).toFixed(2));
			} else if (valueSelected == 1) {
				$('#IGSTDiv').show();
				$('#SGSTDiv').hide();
				$('#CGSTDiv').hide();

				$("#igst").val(
						parseFloat(netPriceMyCurrency * 0.25 / 100).toFixed(2));
				$("#billAmountMyCurrency").val(
						parseFloat(
								netPriceMyCurrency
										+ (netPriceMyCurrency * 0.25 / 100))
								.toFixed(2));

				$('#SGST').val("0.0");
				$('#CGST').val("0.0");

				//$('#IGST').val(
				//		parseFloat(myPurchasePrice * IGSTPer / 100).toFixed(2));
				//$("#netPriceAfterTax").val(
				//		parseFloat(
				//				parseFloat($('#IGST').val())
				//						+ parseFloat(myPurchasePrice)).toFixed(
				//				2));
			} else if (valueSelected == 2) {
				$('#IGSTDiv').hide();
				$('#SGSTDiv').show();
				$('#CGSTDiv').show();

				$("#sgst")
						.val(
								parseFloat(netPriceMyCurrency * 0.125 / 100)
										.toFixed(2));
				$("#cgst")
						.val(
								parseFloat(netPriceMyCurrency * 0.125 / 100)
										.toFixed(2));

				$("#billAmountMyCurrency").val(
						parseFloat(
								netPriceMyCurrency
										+ (netPriceMyCurrency * 0.25 / 100))
								.toFixed(2));

				$('#IGST').val("0.0");
				//$('#SGST').val("0.0");
				//$('#CGST').val("0.0");

				// 				$('#CGST').val(
				// 						parseFloat(myPurchasePrice * CGSTPer / 100).toFixed(2));
				// 				$('#SGST').val(
				// 						parseFloat(myPurchasePrice * SGSTPer / 100).toFixed(2));
				// 				$("#netPriceAfterTax").val(
				// 						parseFloat(
				// 								parseFloat($('#SGST').val())
				// 										+ parseFloat($('#CGST').val())
				// 										+ parseFloat(myPurchasePrice)).toFixed(
				// 								2));
			}
		}
	</script>
</body>

</html>