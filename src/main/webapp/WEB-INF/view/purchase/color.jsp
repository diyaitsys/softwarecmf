<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page isELIgnored="false" %>
<select name="color" id="color" class="form-control" required
	<c:if test="${actionMode == 'view' || param.actionMode == 'view'}">disabled</c:if>>
	<option value="-1" selected="selected">Select Color</option>
	<option value="D"
		<c:if test="${stock.color == 'D'}"> selected="selected" </c:if>>D</option>
	<option value="E"
		<c:if test="${stock.color == 'E'}"> selected="selected" </c:if>>E</option>
	<option value="F"
		<c:if test="${stock.color == 'F'}"> selected="selected" </c:if>>F</option>
	<option value="G"
		<c:if test="${stock.color == 'G'}"> selected="selected" </c:if>>G</option>
	<option value="H"
		<c:if test="${stock.color == 'H'}"> selected="selected" </c:if>>H</option>
	<option value="I"
		<c:if test="${stock.color == 'I'}"> selected="selected" </c:if>>I</option>
	<option value="J"
		<c:if test="${stock.color == 'J'}"> selected="selected" </c:if>>J</option>
	<option value="K"
		<c:if test="${stock.color == 'K'}"> selected="selected" </c:if>>K</option>
	<option value="L"
		<c:if test="${stock.color == 'L'}"> selected="selected" </c:if>>L</option>
	<option value="M"
		<c:if test="${stock.color == 'M'}"> selected="selected" </c:if>>M</option>
</select>