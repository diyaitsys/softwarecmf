<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page isELIgnored="false" %>
<select name="flColor" id="flColor" class="form-control"
	<c:if test="${actionMode == 'view' || param.actionMode == 'view'}">disabled</c:if>>
	<option value="None" selected="selected">None</option>
	<option value="Blue"
		<c:if test="${stock.flColor == 'Blue'}"> selected="selected" </c:if>>Blue</option>
	<option value="Yellow"
		<c:if test="${stock.flColor == 'Yellow'}"> selected="selected" </c:if>>Yellow</option>
	<option value="Green"
		<c:if test="${stock.flColor == 'Green'}"> selected="selected" </c:if>>Green</option>
	<option value="Red"
		<c:if test="${stock.flColor == 'Red'}"> selected="selected" </c:if>>Red</option>
	<option value="Orange"
		<c:if test="${stock.flColor == 'Orange'}"> selected="selected" </c:if>>Orange</option>
	<option value="White"
		<c:if test="${stock.flColor == 'White'}"> selected="selected" </c:if>>White</option>
</select>