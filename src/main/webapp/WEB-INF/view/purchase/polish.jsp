<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page isELIgnored="false" %>
<select name="polish" id="polish" class="form-control"
	<c:if test="${actionMode == 'view' || param.actionMode == 'view'}">disabled</c:if>>
	<option value="-1" selected="selected">Select Polish</option>
	<option value="Excellent"
		<c:if test="${stock.polish == 'Excellent'}"> selected="selected" </c:if>>Excellent</option>
	<option value="VeryGood"
		<c:if test="${stock.polish == 'VeryGood'}"> selected="selected" </c:if>>VeryGood</option>
	<option value="Good"
		<c:if test="${stock.polish == 'Good'}"> selected="selected" </c:if>>Good</option>
	<option value="Fair"
		<c:if test="${stock.polish == 'Fair'}"> selected="selected" </c:if>>Fair</option>
	<option value="Poor"
		<c:if test="${stock.polish == 'Poor'}"> selected="selected" </c:if>>Poor</option>
</select>
