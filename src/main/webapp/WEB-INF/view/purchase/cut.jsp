<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page isELIgnored="false" %>
<select name="cut" id="cut" class="form-control"
					<c:if test="${actionMode == 'view' || param.actionMode == 'view'}">disabled</c:if>>
			>
				<option value="-1" selected="selected">Select Cut</option>
				<option value="Excellent" <c:if test="${stock.cut == 'Excellent'}"> selected="selected" </c:if>>Excellent</option>
				<option value="VeryGood" <c:if test="${stock.cut == 'VeryGood'}"> selected="selected" </c:if>>VeryGood</option>
				<option value="Good" <c:if test="${stock.cut == 'Good'}"> selected="selected" </c:if>>Good</option>
				<option value="Fair" <c:if test="${stock.cut == 'Fair'}"> selected="selected" </c:if>>Fair</option>
				<option value="Poor"
		<c:if test="${stock.cut == 'Poor'}"> selected="selected" </c:if>>Poor</option>
</select>