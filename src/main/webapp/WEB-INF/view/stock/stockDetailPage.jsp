<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" lang="">
<!--<![endif]-->
<head>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page isELIgnored="false" %>
<jsp:include page="../header.jsp"></jsp:include>
</head>
<body>
	<%-- 	<jsp:include page="../menu.jsp"></jsp:include> --%>
	<!-- 	MAIN CODE START HERE -->
	<div id="right-panel" class="right-panel">

		<!-- Header-->
		<%-- 		<jsp:include page="../topHeader.jsp"></jsp:include> --%>
		<!-- Header-->

		<div class="content mt-3">
			<div class="animated fadeIn">
				<div class="row">
					<div class="col-lg-12">
						<div class="card">
							<div class="card-header">
								<strong>Stock Details</strong>
							</div>
							<div class="card-body card-block">
								<div class="row">
									<div class="col-lg-12">
										<div class="row form-group">
											<div class="col col-lg-2">
												<label for="text-input" class=" form-control-label">Lot
													No <span class="required">*</span>
												</label> <input type="text" readonly id="lotNo" value="${stock.lotNo}"
													placeholder="Lot No" class="form-control">
													
													<button type="button" class="btn btn-secondary"
														onClick="check360Vdo();">See 360 Video</button>
											</div>

											<div class="col col-md-2">
												<label for="select" class=" form-control-label">Shape
													<span class="required">*</span>
												</label>
												<jsp:include page="../purchase/shape.jsp"></jsp:include>
											</div>

											<div class="col col-md-2">
												<label for="text-input" class=" form-control-label">Carat
													<span class="required">*</span>
												</label> <input type="text" id="carret" onChange="calTotalPrice()"
													value="${stock.carret}" placeholder="Carat" readonly class="form-control">
											</div>

											<div class="col col-md-2">
												<label for="select" class=" form-control-label">Color
													<span class="required">*</span>
												</label>
												<jsp:include page="../purchase/color.jsp"></jsp:include>
											</div>

											<div class="col col-md-2">
												<label for="select" class=" form-control-label">Clarity
													<span class="required">*</span>
												</label>
												<jsp:include page="../purchase/clarity.jsp"></jsp:include>
											</div>
										</div>

										<div class="row form-group">
											<div class="col col-md-2">
												<label for="select" class=" form-control-label">Cut
												</label>
												<jsp:include page="../purchase/cut.jsp"></jsp:include>
											</div>

											<div class="col col-md-2">
												<label for="select" class=" form-control-label">Polish</label>
												<jsp:include page="../purchase/polish.jsp"></jsp:include>
											</div>

											<div class="col col-md-2">
												<label for="select" class=" form-control-label">Symmetry</label>
												<jsp:include page="../purchase/symmetry.jsp"></jsp:include>
											</div>

											<div class="col col-md-2">
												<label for="select" class=" form-control-label">FL</label>
												<jsp:include page="../purchase/fl.jsp"></jsp:include>
											</div>

											<div class="col col-md-2">
												<label for="select" class=" form-control-label">FL
													Color </label>
												<jsp:include page="../purchase/fluorescenceColor.jsp"></jsp:include>
											</div>

											<div class="col col-lg-2">
												<label for="text-input" class=" form-control-label">Diameter
												</label>
												<div class="input-group">
													<input type="text" id="diameter" name="diameter" readonly value="${stock.diameter}"
														placeholder="Diameter" class="form-control">
													<div class="input-group-addon">MM</div>
												</div>
											</div>
										</div>

										<div class="row form-group">

											<div class="col col-md-3">
												<label for="select" class=" form-control-label">Certificate
													Type </label>
												<jsp:include page="../purchase/certificateType.jsp"></jsp:include>
											</div>

											<div class="col col-lg-3">
												<label for="text-input" class=" form-control-label">Certificate
													No </label> <input type="text" id="certificateNo"
													name="certificateNo" value="${stock.certificateNo}" readonly
													placeholder="Enter Certificate No" class="form-control">
													
													<button type="button" class="btn btn-secondary"
														onClick="checkGiaReport();">Check GIA Report</button>
											</div>
											
											<div class="col col-md-4">
												<label class=" form-control-label">WithRap</label>
												<div class="form-check-inline form-check">
													<input id="withRap" name="withRap" disabled="disabled"
														class="form-check-input" 
														<c:if test="${stock.withRap ==1}"> checked </c:if>
														type="checkbox">
												</div>
											</div>
										</div>

										<div class="row form-group">
											<div class="col col-md-2">
												<div class="input-group">
													<input type="text" id="rapPrice" name="rapPrice"
														value="${stock.rapPrice}" value="" readonly
														placeholder="Rap Price" class="form-control">
													<div id="rapPriceUnit" class="input-group-addon">${purchaseMaster.buyCurrencyUnit}</div>
												</div>
											</div>

											<div class="col col-md-2">
												<label class=" form-control-label">Disc/Prem</label>
												<div class="form-check">
													<div class="radio">
														<label for="radio1" class="form-check-label"> <input
															id="discountRadio" name="withPremium"
															disabled="disabled" value="discount" readonly
															class="form-check-input" type="radio" checked="checked"
															onClick="checkDiscPrem()">Discount
														</label>
													</div>
													<div class="radio">
														<label for="radio2" class="form-check-label "> <input
															id="premiumRadio" name="withPremium"
															disabled="disabled"
															<c:if test="${stock.withPremium ==1}"> checked </c:if>
															value="premium" class="form-check-input" type="radio">Premium
														</label>
													</div>
												</div>
											</div>
										</div>

										<div class="row form-group">
											<div class="col col-lg-2">
												<label id="discPremLabel" for="text-input"
													class=" form-control-label">Discount </label>
												<div class="input-group">
													<input id="discount" name="discount"
														value="${stock.backPrice}" placeholder="Discount" readonly
														class="form-control" type="text">
													<div class="input-group-addon">%</div>
												</div>
											</div>

											<div class="col col-lg-2">
												<label for="text-input" class=" form-control-label">Delivery
													Disc.</label>
												<div class="input-group">
													<input id="addDiscount" name="addDiscount"
														value="${stock.addDiscount}" placeholder="Delivery" readonly
														class="form-control" type="text">
													<div class="input-group-addon">%</div>
												</div>
											</div>

											<div class="col col-lg-2">
												<label for="text-input" class=" form-control-label">Blind
													Disc.</label>
												<div class="input-group">
													<input id="blindDiscount" name="blindDiscount"
														value="${stock.blindDiscount}" placeholder="Blind" readonly
														class="form-control" type="text">
													<div class="input-group-addon">%</div>
												</div>
											</div>

											<div class="col col-lg-2">
												<label for="text-input" class=" form-control-label">No
													Broker Disc.</label>
												<div class="input-group">
													<input id="noBrokerDiscount" name="noBrokerDiscount"
														value="${stock.noBrokerDiscount}" placeholder="No Broker" readonly
														class="form-control" type="text">
													<div class="input-group-addon">%</div>
												</div>
											</div>

											<div class="col col-lg-2">
												<label for="text-input" class=" form-control-label">Vol
													Disc.</label>
												<div class="input-group">
													<input id="volDiscount" name="volDiscount"
														value="${stock.volDiscount}" placeholder="Volume" readonly
														class="form-control" type="text">
													<div class="input-group-addon">%</div>
												</div>
											</div>

											<div class="col col-lg-2">
												<label for="text-input" class=" form-control-label">Adv.
													Pay Disc. </label>
												<div class="input-group">
													<input id="cashDiscount" name="cashDiscount"
														value="${stock.cashDiscount}" placeholder="Cash" readonly
														class="form-control" type="text">
													<div class="input-group-addon">%</div>
												</div>
											</div>
										</div>

										<div class="row form-group">
											<div class="col col-lg-3">
												<label for="text-input" class=" form-control-label">Final
													Price(CRT) </label>
												<div class="input-group">
													<input type="text" id="finalPricePerCrt"
														value="${stock.finalPricePerCrt}" readonly value="" readonly
														placeholder="Final Price" class="form-control">
													<div id="finalPriceUnit" class="input-group-addon">${purchaseMaster.buyCurrencyUnit}</div>
												</div>
											</div>

											<div class="col col-lg-3">
												<label for="text-input" class=" form-control-label">Total
													Price </label>
												<div class="input-group">
													<input type="text" id="totalPrice" value="${stock.totalPrice}" readonly
														readonly placeholder="Total Price"
														class="form-control">
													<div id="totalPriceUnit" class="input-group-addon">${purchaseMaster.buyCurrencyUnit}</div>
												</div>
											</div>
											
											<div class="col col-md-3">
												<label for="text-input" style="color:red" class=" form-control-label">Status : ${stock.stockStatus}
												</label>
											</div>
										</div>
										<div class="row form-group">
											<div class="col col-lg-3">
												<button type="button" class="btn btn-secondary"
														onClick="javascript:window.close();">Close</button>
											</div>
										</div>
									</div>
								</div>
							</div>

						</div>
					</div>
				</div>
			</div>
			<!-- .animated -->
		</div>
		<!-- .content -->
	</div>
	<!-- 	MAIN CODE END HERE -->
	<jsp:include page="../footer.jsp"></jsp:include>
	<script>
	
	function checkGiaReport()
	{
		var certificateNo = document.getElementById('certificateNo').value;
		if(certificateNo != '')
		{
			window.open("https://www.gia.edu/report-check?reportno=" + certificateNo, "_blank");
		}
		else
		{
			alert("Please write certificate no.")
		}
	}
	
	function check360Vdo()
	{
		var lotNo = document.getElementById('lotNo').value;
		window.open("https://v360.in/DiamondView.aspx?d=" + lotNo + "&cid=Diamond360", "_blank");
	}
	$(document).ready(function() {
		$('select[id="shape"]').find("option[value='" + <c:out value="${stock.shape}"/> + "']").attr("selected",true);
// 		$('select[id="color"]').find("option[value='" + ${stock.color} + "']").attr("selected",true);
// 		$('select[id="clarity"]').find("option[value='" + ${stock.clarity} + "']").attr("selected",true);
// 		$('select[id="cut"]').find("option[value='" + ${stock.cut} + "']").attr("selected",true);
// 		$('select[id="polish"]').find("option[value='" + ${stock.polish} + "']").attr("selected",true);
// 		$('select[id="fl"]').find("option[value='" + ${stock.fl} + "']").attr("selected",true);
// 		$('select[id="flColor"]').find("option[value='" + ${stock.flColor} + "']").attr("selected",true);
// 		$('select[id="symmetry"]').find("option[value='" + ${stock.symmetry} + "']").attr("selected",true);
// 		$('select[id="color"]').find("option[value='" + ${stock.color} + "']").attr("selected",true);
// 		$('select[id="color"]').find("option[value='" + ${stock.color} + "']").attr("selected",true);
// 		$('select[id="color"]').find("option[value='" + ${stock.color} + "']").attr("selected",true);
// 		$('select[id="color"]').find("option[value='" + ${stock.color} + "']").attr("selected",true);
// 		$('select[id="color"]').find("option[value='" + ${stock.color} + "']").attr("selected",true);
// 		$('select[id="color"]').find("option[value='" + ${stock.color} + "']").attr("selected",true);
// 		$('select[id="color"]').find("option[value='" + ${stock.color} + "']").attr("selected",true);
// 		$('select[id="color"]').find("option[value='" + ${stock.color} + "']").attr("selected",true);
// 		$('select[id="color"]').find("option[value='" + ${stock.color} + "']").attr("selected",true);
// 		$('select[id="color"]').find("option[value='" + ${stock.color} + "']").attr("selected",true);
	});
	</script>
</body>
</html>
