<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" lang="">
<!--<![endif]-->
<head>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page isELIgnored="false"%>
<jsp:include page="../header.jsp"></jsp:include>
</head>
<body>
	<jsp:include page="../menu.jsp"></jsp:include>
	<!-- 	MAIN CODE START HERE -->
	<div id="right-panel" class="right-panel">
		<!-- Header-->
		<jsp:include page="../topHeader.jsp"></jsp:include>
		<!-- Header-->
		<div class="breadcrumbs">
			<div class="col-sm-4">
				<div class="page-header float-left">
					<div class="page-title">
						<h1>Return From Lab (Stock Update)</h1>
					</div>
				</div>
			</div>
		</div>
		<div class="content mt-3">
			<div class="animated fadeIn">
				<div class="row">
					<div class="col-md-12">
						<div class="card">
							<div class="card-body">
								<form action="/stock/returnLabById" method="post"
									id="frm" class="form-horizontal">
									<input type="hidden" name="actionMode" value="${actionMode}" />
									<input type="hidden" name="id" value="${stock.id}" />
									<c:if test="${message != null && message != ''}">
										<div
											class="alert with-close alert-success alert-dismissible fade show">
											<span class="badge badge-pill badge-success">Success</span>${message}
											<button type="button" class="close" data-dismiss="alert"
												aria-label="Close">
												<span aria-hidden="true">&times;</span>
											</button>
										</div>
									</c:if>
									<div class="row form-group">
										<div class="col col-lg-2">
											<label for="text-input" class=" form-control-label">Lot
												No <span class="required">*</span>
											</label> <input type="text" id="lotNo" name="lotNo" value="${stock.lotNo}"
												placeholder="Lot No" class="form-control" readonly>
										</div>

										<div class="col col-md-2">
											<label for="select" class=" form-control-label">Shape
												<span class="required">*</span>
											</label>
											<jsp:include page="../purchase/shape.jsp" />
										</div>

										<div class="col col-md-2">
											<label for="text-input" class=" form-control-label">Carat
												<span class="required">*</span>
											</label> <input type="text" name="carret" id="carat"
												value="${stock.carret}"
												placeholder="Carat" class="form-control">
										</div>

										<div class="col col-md-2">
											<label for="select" class=" form-control-label">Color
												<span class="required">*</span>
											</label>
											<jsp:include page="../purchase/color.jsp" />
										</div>

										<div class="col col-md-2">
											<label for="select" class=" form-control-label">Clarity
												<span class="required">*</span>
											</label>
											<jsp:include page="../purchase/clarity.jsp" />
										</div>
									</div>

									<div class="row form-group">
										<div class="col col-md-2">
											<label for="select" class=" form-control-label">Cut </label>
											<jsp:include page="../purchase/cut.jsp" />
										</div>

										<div class="col col-md-2">
											<label for="select" class=" form-control-label">Polish</label>
											<jsp:include page="../purchase/polish.jsp"/>
										</div>

										<div class="col col-md-2">
											<label for="select" class=" form-control-label">Symmetry</label>
											<jsp:include page="../purchase/symmetry.jsp" />
										</div>

										<div class="col col-md-2">
											<label for="select" class=" form-control-label">FL</label>
											<jsp:include page="../purchase/fl.jsp"/></div>

										<div class="col col-md-2">
											<label for="select" class=" form-control-label">FL
												Color </label>
											<jsp:include page="../purchase/fluorescenceColor.jsp" />
										</div>

										<div class="col col-lg-2">
											<label for="text-input" class=" form-control-label">Diameter
											</label>
											<div class="input-group">
												<input type="text" id="diameter" name="diameter"
													value="${stock.diameter}" placeholder="Diameter" class="form-control">
												<div class="input-group-addon">MM</div>
											</div>
										</div>
									</div>

									<div class="row form-group">

										<div class="col col-md-3">
											<label for="select" class=" form-control-label">Certificate
												Type </label>
											<jsp:include page="../purchase/certificateType.jsp" />
										</div>

										<div class="col col-lg-3">
											<label for="text-input" class=" form-control-label">Certificate
												No </label> <input type="text" id="certificateNo"
												name="certificateNo" value="${stock.certificateNo}"
												placeholder="Enter Certificate No" class="form-control">
												
												<button type="button" class="btn btn-secondary"
														onClick="checkGiaReport();">Check GIA Report</button>
										</div>
									</div>
									<div class="card-footer">
										<c:if test="${actionMode != 'view'}">
											<button type="submit" class="btn btn-primary btn-sm">
												<i class="fa fa-dot-circle-o"></i> Submit
											</button>
										</c:if>
										<button type="button" class="btn btn-danger btn-sm"
											onClick="javascript:closeBtn();">
											<i class="fa fa-dot-circle-o"></i> Close
										</button>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- .animated -->
		</div>
		<!-- .content -->
	</div>
	<!-- 	MAIN CODE END HERE -->
	<jsp:include page="../footer.jsp"></jsp:include>
	<script>
	function checkGiaReport()
	{
		var certificateNo = document.getElementById('certificateNo').value;
		if(certificateNo != '')
		{
			window.open("https://www.gia.edu/report-check?reportno=" + certificateNo, "_blank");
		}
		else
		{
			alert("Please write certificate no.")
		}
	}

	</script>
</body>
</html>
