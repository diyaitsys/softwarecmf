<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" lang="">
<!--<![endif]-->
<head>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page isELIgnored="false"%>
<jsp:include page="../header.jsp"></jsp:include>
</head>
<body>
	<jsp:include page="../menu.jsp"></jsp:include>
	<!-- 	MAIN CODE START HERE -->
	<div id="right-panel" class="right-panel">
		<!-- Header-->
		<jsp:include page="../topHeader.jsp"></jsp:include>
		<!-- Header-->
		<div class="breadcrumbs">
			<div class="col-sm-4">
				<div class="page-header float-left">
					<div class="page-title">
						<c:choose>
							<c:when test="${status=='OnMemo'}">
								<h1>Memo Stock Details</h1>
							</c:when>
							<c:when test="${status=='OnLab'}">
								<h1>Lab Stock Details</h1>
							</c:when>
							<c:when test="${status=='OnManufacture'}">
								<h1>Manufacture Stock Details</h1>
							</c:when>
							<c:otherwise>
								<h1>Stock Details</h1>
							</c:otherwise>
						</c:choose>
					</div>
				</div>
			</div>
		</div>
		<div class="content mt-3">
			<div class="animated fadeIn">
				<div class="row">
					<div class="col-md-12">
						<div class="card">
							<div class="card-body card-block">
								<c:if test="${message != null && message != ''}">
									<div
										class="alert with-close alert-success alert-dismissible fade show">
										<span class="badge badge-pill badge-success">Success</span>${message}
										<button type="button" class="close" data-dismiss="alert"
											aria-label="Close">
											<span aria-hidden="true">&times;</span>
										</button>
									</div>
								</c:if>
								<c:if test="${status=='OnMemo'}">
								<form action="/stock/returnMemoByMultipleId" method="post" id="frm"
									class="form-horizontal">
									<div class="row form-group">
										<div class="col col-lg-3">
											<label for="text-input" class=" form-control-label">Memo Return
												Date <span class="required">*</span>
											</label> <input type="text" id="memoRtnDate" name="memoRtnDate" required
												placeholder="Memo Return Date" class="form-control dtPicker"
												<c:if test="${actionMode == 'view'}"> readonly
											</c:if>>
										</div>
										<div class="col col-lg-6" style="padding-top: 30px">
											<button type="submit" class="btn btn-primary btn-sm">
												<i class="fa fa-dot-circle-o"></i> Return From Memo
											</button>
										</div>
									</div>
								</c:if>
								<table id="datatable" class="table table-striped table-bordered">
									<thead>
										<tr>
											<th></th>
											<th>Lot No</th>
											<th>Carat</th>
											<th>Shape</th>
											<th>Color</th>
											<th>Clarity</th>
											<c:if test="${status=='OnLab'}">
												<th>Lab Name</th>
											</c:if>
											<c:if test="${status=='OnManufacture'}">
												<th>Manufacture Name</th>
											</c:if>
											<th></th>
										</tr>
									</thead>
									<tbody>
										<c:forEach items="${stockList}" var="stock">
											<tr>
												<td><input type="checkbox" name="selectedIds[]"
														value="${stock.id}"></td>
												<td>${stock.lotNo}</td>
												<td>${stock.carret}</td>
												<td>${stock.shape}</td>
												<td>${stock.color}</td>
												<td>${stock.clarity}</td>
												<c:if test="${status=='OnLab'}">
													<td>${labMap[stock.id]}</td>
												</c:if>
												<c:if test="${status=='OnManufacture'}">
													<td>${manufactureMap[stock.id]}</td>
												</c:if>
												<td>
													<button type="button" onClick="viewStock(${stock.id})"
														class="btn btn-outline-success btn-sm">Details</button> <c:if
														test="${status=='OnMemo'}">
														<button type="button" onClick="memoReturn(${stock.id})"
															class="btn btn-outline-danger btn-sm">Return</button>
													</c:if>
													<c:if test="${status=='OnLab'}">
														<button type="button" id="addStockBtn"
															class="btn btn-outline-danger btn-sm" data-toggle="modal"
															onClick="labReturn(${stock.id})">Return</button>
													</c:if>
													<c:if test="${status=='OnManufacture'}">
														<button type="button" id="addStockBtn"
															class="btn btn-outline-danger btn-sm" data-toggle="modal"
															onClick="manufactureReturn(${stock.id})">Return</button>
													</c:if>
												</td>
											</tr>
										</c:forEach>
									</tbody>
									<tfoot>
										<tr>
											<th></th>
											<th>Lot No</th>
											<th>Carat</th>
											<th>Shape</th>
											<th>Color</th>
											<th>Clarity</th>
											<th></th>
										</tr>
									</tfoot>
								</table>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- .animated -->
		</div>
	</div>
	<!-- 	MAIN CODE END HERE -->
	<jsp:include page="../footer.jsp"></jsp:include>
	<link type="text/css"
		href="//gyrocode.github.io/jquery-datatables-checkboxes/1.2.10/css/dataTables.checkboxes.css"
		rel="stylesheet" />
	<script type="text/javascript"
		src="//gyrocode.github.io/jquery-datatables-checkboxes/1.2.10/js/dataTables.checkboxes.min.js"></script>
	<script type="text/javascript">
		//var $j = jQuery.noConflict();
		$(document).ready(function() {
			var table = $('.table').DataTable();
			
			 $('.table tfoot th:nth-last-child(n+1)').each( function () {
			     var title = $(this).text();
			     $(this).html( '<input type="text" size="5" placeholder="Search" />' );
			     //$(this).html( '<input type="text" placeholder="Search '+title+'" />' );
			 } );
		   
		   table.columns().every( function () {
			     var that = this;
			     $( 'input', this.footer() ).on( 'keyup change', function () {
			         if ( that.search() !== this.value ) {
			             that
			                 .search( this.value )
			                 .draw();
			         }
			     } );
			 } );
		});

		function viewStock(id)
		{
			window.open("/stock/getStockDetailsById?id=" + id, "_blank");
		}
		
		function memoReturn(id)
		{
			if(confirm("Sure to return stock from Memo to InHouse?"))
			{
				window.location.href = "/stock/returnMemoById?id=" + id;
			}
		}
		
		function labReturn(id)
		{
			if(confirm("Sure to return stock from Lab to InHouse?"))
			{
				window.location.href = "/stock/returnLabByIdStep1?id=" + id;
			}
		}
		
		
		function manufactureReturn(id)
		{
			if(confirm("Sure to return stock from Manufacture to InHouse?"))
			{
				window.location.href = "/stock/returnManufactureByIdStep1?id=" + id;
			}
		}

		function openModel() {
			//$("button[data-toggle=\"modal\" data-target=\"#largeModal\"]").click();
			//$('body').css('padding-right', '');
			$("#largeModal").model('show');
			//$(".modal").show();
			//$('#largeModal').appendTo("body").modal('show');
		}
	</script>
</body>
</html>
