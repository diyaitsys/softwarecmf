<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" lang="">
<!--<![endif]-->
<head>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page isELIgnored="false" %>
<jsp:include page="../header.jsp"></jsp:include>
</head>
<body>
	<jsp:include page="../menu.jsp"></jsp:include>
	<!-- 	MAIN CODE START HERE -->
	<div id="right-panel" class="right-panel">

		<!-- Header-->
		<jsp:include page="../topHeader.jsp"></jsp:include>
		<!-- Header-->

		<div class="content mt-3">
			<div class="animated fadeIn">
				<div class="row">
					<div class="col-lg-12">
						<div class="card">
							<div class="card-header">
								<strong>Manufacture Details</strong>
							</div>
							<c:if test="${error != null && error != ''}">
								<div
									class="alert with-close alert-danger alert-dismissible fade show">
									<span class="badge badge-pill badge-danger">Error</span>${error}
									<button type="button" class="close" data-dismiss="alert"
										aria-label="Close">
										<span aria-hidden="true">&times;</span>
									</button>
								</div>
							</c:if>
							<div class="card-body card-block">
								<form action="/manufacture/addUpdateManufacture" method="post"
									class="form-horizontal">
									<input type="hidden" name="actionMode" value="${actionMode}" />
									<c:if test="${actionMode != 'add'}">
										<input type="hidden" name="id" value="${manufacture.id}" />
									</c:if>
									<div class="row form-group">
										<div class="col col-lg-6">
											<label for="text-input" class=" form-control-label">Name
												<span class="required">*</span>
											</label> <input type="text" id="text-input" name="name" required
												value="${manufacture.name}" placeholder="Enter Name"
												class="form-control"
												<c:if test="${actionMode == 'view'}">
										readonly
									</c:if>>
										</div>
										<div class="col col-lg-6">
											<label for="text-input" class=" form-control-label">Address</label>
											<input type="text" id="text-input" name="address"
												value="${manufacture.address}" placeholder="Enter Address"
												class="form-control"
												<c:if test="${actionMode == 'view'}">
										readonly
									</c:if>>
										</div>
									</div>
									<div class="row form-group">
										<div class="col col-lg-6">
											<label for="text-input" class=" form-control-label">Country</label>
											<input type="text" id="text-input" name="country"
												value="${manufacture.country}" placeholder="Enter Country"
												class="form-control"
												<c:if test="${actionMode == 'view'}">
										readonly
									</c:if>>
										</div>
										<div class="col col-lg-6">
											<label for="text-input" class=" form-control-label">Email</label>
											<input type="text" id="text-input" name="emailAddress"
												value="${manufacture.emailAddress}" placeholder="Enter Email"
												class="form-control"
												<c:if test="${actionMode == 'view'}">
										readonly
									</c:if>>
										</div>
									</div>
									<div class="row form-group">
										<div class="col col-lg-6">
											<label for="text-input" class=" form-control-label">GST
												No</label> <input type="text" id="text-input" name="gstNumber"
												value="${manufacture.gstNumber}" placeholder="Enter GST No"
												class="form-control"
												<c:if test="${actionMode == 'view'}">
										readonly
									</c:if>>
										</div>
										<div class="col col-lg-6">
											<label for="text-input" class=" form-control-label">Contact
												Person</label> <input type="text" id="text-input"
												name="contantPersonName" value="${manufacture.contantPersonName}"
												placeholder="Enter Contact Person Name" class="form-control"
												<c:if test="${actionMode == 'view'}">
										readonly
									</c:if>>
										</div>
									</div>
									<div class="row form-group">
										<div class="col col-lg-6">
											<label for="text-input" class=" form-control-label">Bank
												Name</label> <input type="text" id="text-input" name="bankName"
												value="${manufacture.bankName}" placeholder="Enter Bank Name"
												class="form-control"
												<c:if test="${actionMode == 'view'}">
										readonly
									</c:if>>
										</div>
										<div class="col col-lg-6">
											<label for="text-input" class=" form-control-label">IFSC
												Code</label> <input type="text" id="text-input" name="ifcsCode"
												value="${manufacture.ifcsCode}" placeholder="Enter IFSC Code"
												class="form-control"
												<c:if test="${actionMode == 'view'}">
										readonly
									</c:if>>
										</div>
									</div>
									<div class="row form-group">
										<div class="col col-lg-6">
											<label for="text-input" class=" form-control-label">Bank
												Account No</label> <input type="text" id="bankAccount"
												value="${manufacture.bankAccount}" name="bankAccount"
												placeholder="Enter Bank Account No" class="form-control"
												<c:if test="${actionMode == 'view'}">
										readonly
									</c:if>>
										</div>
										<div class="col col-lg-6">
											<label for="text-input" class=" form-control-label">Swift
												Code</label> <input type="text" id="text-input" name="swiftCode"
												value="${manufacture.swiftCode}" placeholder="Enter Swift Code"
												class="form-control"
												<c:if test="${actionMode == 'view'}">
										readonly
									</c:if>>
										</div>
									</div>
									<div class="row form-group">
										<div class="col col-lg-6">
											<label for="text-input" class=" form-control-label">Remarks</label>
											<input type="text" id="text-input" name="remarks"
												value="${manufacture.remarks}" placeholder="Enter Remarks"
												class="form-control"
												<c:if test="${actionMode == 'view'}">
										readonly
									</c:if>>
										</div>
										<div class="col col-lg-6">
											<label for="text-input" class=" form-control-label">Phone
												No</label> <input type="text" id="text-input" name="mobileNumber"
												value="${manufacture.mobileNumber}" placeholder="Enter Phone No"
												class="form-control"
												<c:if test="${actionMode == 'view'}">
										readonly
									</c:if>>
										</div>
									</div>
									<div class="row form-group">
										<div class="col col-lg-6">
											<label for="text-input" class=" form-control-label">Website</label>
											<input type="text" id="text-input" name="website"
												value="${manufacture.website}" placeholder="Enter Website"
												class="form-control"
												<c:if test="${actionMode == 'view'}">
										readonly
									</c:if>>
										</div>
										<div class="col col-lg-6">
											<label for="text-input" class=" form-control-label">PAN
												No</label> <input type="text" id="text-input" name="panNumber"
												value="${manufacture.panNumber}" placeholder="Enter PAN No"
												class="form-control"
												<c:if test="${actionMode == 'view'}">
										readonly
									</c:if>>
										</div>
									</div>
									<div class="card-footer">
										<c:if test="${actionMode != 'view'}">
											<button type="submit" class="btn btn-primary btn-sm">
												<i class="fa fa-dot-circle-o"></i> Submit
											</button>
										</c:if>
										<button type="button" class="btn btn-danger btn-sm"
											onClick="javascript:closeBtn();">
											<i class="fa fa-dot-circle-o"></i> Close
										</button>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- .animated -->
		</div>
		<!-- .content -->
	</div>
	<!-- 	MAIN CODE END HERE -->
	<jsp:include page="../footer.jsp"></jsp:include>
	<script>
		function closeBtn() {
			window.history.back();
		}
	</script>
</body>
</html>
