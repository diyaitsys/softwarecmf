
<!-- Left Panel -->
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page isELIgnored="false" %>
<aside id="left-panel" class="left-panel">
	<nav class="navbar navbar-expand-sm navbar-default">

		<div class="navbar-header">
			<button class="navbar-toggler" type="button" data-toggle="collapse"
				data-target="#main-menu" aria-controls="main-menu"
				aria-expanded="false" aria-label="Toggle navigation">
				<i class="fa fa-bars"></i>
			</button>
			<!--                 <a class="navbar-brand" href="./"><img src="images/logo.png" alt="Logo"></a> -->
			<!-- 			<a class="navbar-brand hidden" href="./"><img src="images/logo2.png" alt="Logo"></a> -->
		</div>

		<div id="main-menu" class="main-menu collapse navbar-collapse">
			<ul class="nav navbar-nav">
				<c:if test="${sessionScope['USER_SESSION'] !=  null}">
					<li><a href="#">Welcome,
							${sessionScope['USER_SESSION'].firstName}
							${sessionScope['USER_SESSION'].lastName}</a></li>
				</c:if>
				<c:if test="${sessionScope['BRANCH_SESSION'] !=  null}">
					<li><a href="/dashboard/dashboardHomePage"> <i
							class="menu-icon fa fa-dashboard"></i>Dashboard
					</a></li>
				</c:if>
				<li><a href="/branch/selectBranch"> <i
						class="menu-icon fa fa-dashboard"></i>Select Branch
				</a></li>
				<c:if test="${sessionScope['BRANCH_SESSION'] !=  null}">
					<h3 class="menu-title">Branch Admin</h3>
					<!-- /.menu-title -->
					<li class="menu-item-has-children dropdown"><a href="#"
						class="dropdown-toggle" data-toggle="dropdown"
						aria-haspopup="true" aria-expanded="false"> <i
							class="menu-icon fa fa-laptop"></i>KYC
					</a>
						<ul class="sub-menu children dropdown-menu">
							<li><i class="fa fa-puzzle-piece"></i><a
								href="/party/partyListPage">Party</a></li>
							<li><i class="fa fa-id-badge"></i><a
								href="/broker/brokerListPage">Broker</a></li>
							<!--                             <li><i class="fa fa-bars"></i><a href="ui-tabs.html">Tabs</a></li> -->
							<!--                             <li><i class="fa fa-share-square-o"></i><a href="ui-social-buttons.html">Social Buttons</a></li> -->
							<!--                             <li><i class="fa fa-id-card-o"></i><a href="ui-cards.html">Cards</a></li> -->
							<!--                             <li><i class="fa fa-exclamation-triangle"></i><a href="ui-alerts.html">Alerts</a></li> -->
							<!--                             <li><i class="fa fa-spinner"></i><a href="ui-progressbar.html">Progress Bars</a></li> -->
							<!--                             <li><i class="fa fa-fire"></i><a href="ui-modals.html">Modals</a></li> -->
							<!--                             <li><i class="fa fa-book"></i><a href="ui-switches.html">Switches</a></li> -->
							<!--                             <li><i class="fa fa-th"></i><a href="ui-grids.html">Grids</a></li> -->
							<!--                             <li><i class="fa fa-file-word-o"></i><a href="ui-typgraphy.html">Typography</a></li> -->
						</ul></li>
					<li class="menu-item-has-children dropdown"><a href="#"
						class="dropdown-toggle" data-toggle="dropdown"
						aria-haspopup="true" aria-expanded="false"> <i
							class="menu-icon fa fa-table"></i>Master
					</a>
						<ul class="sub-menu children dropdown-menu">
							<li><i class="fa fa-table"></i><a
								href="/branch/branchListPage">Branch</a></li>
							<li><i class="fa fa-table"></i><a
								href="/lab/labListPage">Lab</a></li>
							<li><i class="fa fa-table"></i><a
								href="/manufacture/manufactureListPage">Manufacture</a></li>
							<li><i class="fa fa-table"></i><a href="/rapnet/rapNetPage">RapNet</a></li>
						</ul></li>
					<li class="menu-item-has-children active dropdown"><a href="#"
						class="dropdown-toggle" data-toggle="dropdown"
						aria-haspopup="true" aria-expanded="false"> <i
							class="menu-icon fa fa-th"></i>Transactions
					</a>
						<ul class="sub-menu children dropdown-menu">
							<li><i class="menu-icon fa fa-th"></i><a
								href="/purchase/purchasePage">Purchase</a></li>
							<li><i class="menu-icon fa fa-th"></i><a
								href="/sales/salesPage">Sell</a></li>
							<li><i class="menu-icon fa fa-th"></i><a
								href="/payment/paymentPage?actionMode=purchase">Payment
									(Invoice)</a></li>
							<li><i class="menu-icon fa fa-th"></i><a
								href="/payment/paymentPage?actionMode=broker">Payment
									(Broker)</a></li>
							<li><i class="menu-icon fa fa-th"></i><a
								href="/receipt/receiptPage">Receipt</a></li>
							<li><i class="menu-icon fa fa-th"></i><a
								href="/stock/getStockListForTranfer">Branch Transfer</a></li>
							<li><i class="menu-icon fa fa-th"></i><a
								href="/stock/getStockListForMemo">Give To Memo</a></li>
							<li><i class="menu-icon fa fa-th"></i><a
								href="/stock/getStockList?status=OnMemo">Return From Memo</a></li>
							<li><i class="menu-icon fa fa-th"></i><a
								href="/stock/getStockListForLab">Give To Lab</a></li>
							<li><i class="menu-icon fa fa-th"></i><a
								href="/stock/getStockList?status=OnLab">Return From Lab</a></li>
							<li><i class="menu-icon fa fa-th"></i><a
								href="/stock/getStockListForManufacture">Give To Manufacture</a></li>
							<li><i class="menu-icon fa fa-th"></i><a
								href="/stock/getStockList?status=OnManufacture">Return From Manufacture</a></li>

						</ul></li>

					<li class="menu-item-has-children active dropdown"><a href="#"
						class="dropdown-toggle" data-toggle="dropdown"
						aria-haspopup="true" aria-expanded="false"> <i
							class="menu-icon fa fa-th"></i>Reports
					</a>
						<ul class="sub-menu children dropdown-menu">
							<li><i class="menu-icon fa fa-th"></i><a
								href="/purchase/getInvoiceList">Purchase</a></li>
							<li><i class="menu-icon fa fa-th"></i><a
								href="/sales/getInvoiceList">Sell</a></li>
							<li><i class="menu-icon fa fa-th"></i><a
								href="/stock/stoneHistroyPage">Stone History</a></li>
							<li><i class="menu-icon fa fa-th"></i><a
								href="/stock/getStockList?status=OnManufacture">Stock In Manufacture</a></li>
							<li><i class="menu-icon fa fa-th"></i><a
								href="/stock/getStockList?status=OnLab">Stock InLab</a></li>
							<li><i class="menu-icon fa fa-th"></i><a
								href="/stock/getStockList?status=InHouse">Stock InHouse</a></li>
							<li><i class="menu-icon fa fa-th"></i><a
								href="/stock/getStockList?status=OnMemo">Stock OnMemo</a></li>
							<li><i class="menu-icon fa fa-th"></i><a
								href="/report/pendingPurchasePayment?actionMode=pending">Pending
									Purchase Payment</a></li>
							<li><i class="menu-icon fa fa-th"></i><a
								href="/report/pendingPurchasePayment?actionMode=done">Purchase
									Payment Done</a></li>
							<li><i class="menu-icon fa fa-th"></i><a
								href="/report/pendingBrokerPayment?actionMode=pending">Pending
									Broker Payment</a></li>
							<li><i class="menu-icon fa fa-th"></i><a
								href="/report/pendingBrokerPayment?actionMode=done">Broker
									Payment Done</a></li>
							<li><i class="menu-icon fa fa-th"></i><a
								href="/report/pendingSaleReceipt?actionMode=pending">Pending
									Sell Receipt</a></li>
							<li><i class="menu-icon fa fa-th"></i><a
								href="/report/pendingSaleReceipt?actionMode=done">Sell
									Receipt Done</a></li>
							<li><i class="menu-icon fa fa-th"></i><a
								href="/report/memoPrintTable">
									Memo Print</a></li>
						</ul></li>
					<li class="menu-item-has-children active dropdown"><a href="#"
						class="dropdown-toggle" data-toggle="dropdown"
						aria-haspopup="true" aria-expanded="false"> <i
							class="menu-icon fa fa-th"></i>User Details
					</a>
						<ul class="sub-menu children dropdown-menu">
							<li><i class="menu-icon fa fa-th"></i><a
								href="/user/userListPage">User</a></li>
						</ul></li>
					<!-- 						<h3 class="menu-title">Reports</h3>

					<li class="menu-item-has-children dropdown"><a href="#"
						class="dropdown-toggle" data-toggle="dropdown"
						aria-haspopup="true" aria-expanded="false"> <i
							class="menu-icon fa fa-tasks"></i>Icons
					</a>
						<ul class="sub-menu children dropdown-menu">
							<li><i class="menu-icon fa fa-fort-awesome"></i><a
								href="font-fontawesome.html">Font Awesome</a></li>
							<li><i class="menu-icon ti-themify-logo"></i><a
								href="font-themify.html">Themefy Icons</a></li>
						</ul></li>
					<li><a href="widgets.html"> <i class="menu-icon ti-email"></i>Widgets
					</a></li>
					<li class="menu-item-has-children dropdown"><a href="#"
						class="dropdown-toggle" data-toggle="dropdown"
						aria-haspopup="true" aria-expanded="false"> <i
							class="menu-icon fa fa-bar-chart"></i>Charts
					</a>
						<ul class="sub-menu children dropdown-menu">
							<li><i class="menu-icon fa fa-line-chart"></i><a
								href="charts-chartjs.html">Chart JS</a></li>
							<li><i class="menu-icon fa fa-area-chart"></i><a
								href="charts-flot.html">Flot Chart</a></li>
							<li><i class="menu-icon fa fa-pie-chart"></i><a
								href="charts-peity.html">Peity Chart</a></li>
						</ul></li>

					<li class="menu-item-has-children dropdown"><a href="#"
						class="dropdown-toggle" data-toggle="dropdown"
						aria-haspopup="true" aria-expanded="false"> <i
							class="menu-icon fa fa-area-chart"></i>Maps
					</a>
						<ul class="sub-menu children dropdown-menu">
							<li><i class="menu-icon fa fa-map-o"></i><a
								href="maps-gmap.html">Google Maps</a></li>
							<li><i class="menu-icon fa fa-street-view"></i><a
								href="maps-vector.html">Vector Maps</a></li>
						</ul></li>
					<h3 class="menu-title">Extras</h3>
					<li class="menu-item-has-children dropdown"><a href="#"
						class="dropdown-toggle" data-toggle="dropdown"
						aria-haspopup="true" aria-expanded="false"> <i
							class="menu-icon fa fa-glass"></i>Pages
					</a>
						<ul class="sub-menu children dropdown-menu">
							<li><i class="menu-icon fa fa-sign-in"></i><a
								href="page-login.html">Login</a></li>
							<li><i class="menu-icon fa fa-sign-in"></i><a
								href="page-register.html">Register</a></li>
							<li><i class="menu-icon fa fa-paper-plane"></i><a
								href="pages-forget.html">Forget Pass</a></li>
						</ul></li>
						-->
			</ul>
			</c:if>
		</div>
		<!-- /.navbar-collapse -->
	</nav>
</aside>
<!-- /#left-panel -->