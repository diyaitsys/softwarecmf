<script src="/assets/js/vendor/jquery-2.1.4.min.js"></script>

<script src="/assets/js/popper.min.js"></script>
<script src="/assets/js/plugins.js"></script>
<script src="/assets/js/main.js"></script>
<!-- <script src="/assets/js/lib/data-table/datatables.min.js"></script> -->
<!-- <script src="/assets/js/lib/data-table/dataTables.bootstrap.min.js"></script> -->
<!-- <script src="/assets/js/lib/data-table/dataTables.buttons.min.js"></script> -->
<!-- <script src="/assets/js/lib/data-table/buttons.bootstrap.min.js"></script> -->
<!-- <script src="/assets/js/lib/data-table/jszip.min.js"></script> -->
<!-- <script src="/assets/js/lib/data-table/pdfmake.min.js"></script> -->
<!-- <script src="/assets/js/lib/data-table/vfs_fonts.js"></script> -->
<!-- <script src="/assets/js/lib/data-table/buttons.html5.min.js"></script> -->
<!-- <script src="/assets/js/lib/data-table/buttons.print.min.js"></script> -->
<!-- <script src="/assets/js/lib/data-table/buttons.colVis.min.js"></script> -->
<!-- <script src="/assets/js/lib/data-table/datatables-init.js"></script> -->
<script src="/assets/js/jquery.js"></script>
<script src="/assets/js/lib/chosen/chosen.jquery.min.js"></script>
<script src="/assets/js/bootstrap-datepicker.min.js"></script>
<script src="/assets/js/bootstrap.min.js"></script>
<!-- <script src="/assets/js/jquery.dataTables.js"></script> -->
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css"/>
 
<script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>



<script>
	$(document).ready(function() {
		$(".standardSelect").chosen({
			disable_search_threshold : 10,
			no_results_text : "Oops, nothing found!",
			width : "100%"
		});

		$('.dtPicker').datepicker({
			format : "yyyy-mm-dd",
			todayHighlight : true
		});
	});
</script>
