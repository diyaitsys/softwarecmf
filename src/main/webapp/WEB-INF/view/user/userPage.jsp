<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" lang="">
<!--<![endif]-->
<head>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page isELIgnored="false" %>
<jsp:include page="../header.jsp"></jsp:include>
</head>
<body>
	<jsp:include page="../menu.jsp"></jsp:include>
	<!-- 	MAIN CODE START HERE -->
	<div id="right-panel" class="right-panel">

		<!-- Header-->
		<jsp:include page="../topHeader.jsp"></jsp:include>
		<!-- Header-->

		<div class="content mt-3">
			<div class="animated fadeIn">
				<div class="row">
					<div class="col-lg-12">
						<div class="card">
							<div class="card-header">
								<strong>User Details</strong>
							</div>
							<c:if test="${error != null && error != ''}">
								<div
									class="alert with-close alert-danger alert-dismissible fade show">
									<span class="badge badge-pill badge-danger">Error</span>${error}
									<button type="button" class="close" data-dismiss="alert"
										aria-label="Close">
										<span aria-hidden="true">&times;</span>
									</button>
								</div>
							</c:if>
							<div class="card-body card-block">
								<form action="/user/addUpdateUser" method="post"
									class="form-horizontal">
									<input type="hidden" name="actionMode" value="${actionMode}" />
									<c:if test="${actionMode != 'add'}">
										<input type="hidden" name="id" value="${user.id}" />
									</c:if>
									<div class="row form-group">
										<div class="col col-lg-4">
											<label for="text-input" class=" form-control-label">First
												Name <span class="required">*</span>
											</label> <input type="text" id="text-input" name="firstName" required
												value="${user.firstName}" placeholder="Enter First Name"
												class="form-control"
												<c:if test="${actionMode == 'view'}">
										readonly
									</c:if>>
										</div>
										<div class="col col-lg-4">
											<label for="text-input" class=" form-control-label">Father
												Name </label> <input type="text" id="middleName" name="middleName"
												value="${user.middleName}" placeholder="Enter Father Name"
												class="form-control"
												<c:if test="${actionMode == 'view'}">
										readonly
									</c:if>>
										</div>
										<div class="col col-lg-4">
											<label for="text-input" class=" form-control-label">Surname
												<span class="required">*</span>
											</label> <input type="text" id="lastName" name="lastName" required
												value="${user.lastName}" placeholder="Enter Surname"
												class="form-control"
												<c:if test="${actionMode == 'view'}">
										readonly
									</c:if>>
										</div>
									</div>
									<div class="row form-group">
										<div class="col col-lg-4">
											<label for="text-input" class=" form-control-label">Email
												<span class="required">*</span>
											</label> <input type="text" id="text-input" name="emailAddress"
												required value="${user.emailAddress}"
												placeholder="Enter Email" class="form-control"
												<c:if test="${actionMode == 'view'}">
										readonly
									</c:if>>
										</div>
										<div class="col col-md-3">
											<label for="select" class=" form-control-label">Employee
												Type </label> <select name="employeeRole"
												id="employeeRole" class="form-control"
												<c:if test="${actionMode == 'view'}">disabled</c:if>>
												<option value="-1" selected="selected">Select
													Employee Type</option>
												<option value="Normal User"
													<c:if test="${user.employeeRole == 'Normal User'}"> selected="selected" </c:if>>Normal
													User</option>
												<option value="Company Admin"
													<c:if test="${user.employeeRole == 'Company Admin'}"> selected="selected" </c:if>>Company
													Admin</option>
												<option value="Branch Admin"
													<c:if test="${user.employeeRole == 'Branch Admin'}"> selected="selected" </c:if>>Branch
													Admin</option>
											</select>
										</div>
									</div>
									<div class="row form-group">
										<div class="col col-lg-3">
											<label for="text-input" class=" form-control-label">Date
												of Birth </label> <input type="text" id="dateOfBirth"
												name="dateOfBirth" placeholder="Date Of Birth"
												value="${user.dateOfBirth}" class="form-control dtPicker"
												<c:if test="${actionMode == 'view'}"> readonly
											</c:if>>
										</div>
										<div class="col col-lg-3">
											<label for="text-input" class=" form-control-label">Date
												of Joining </label> <input type="text" id="dateOfJoining"
												name="dateOfJoining" placeholder="Date Of Joining"
												value="${user.dateOfJoining}" class="form-control dtPicker"
												<c:if test="${actionMode == 'view'}"> readonly
											</c:if>>
										</div>
									</div>
									<div class="row form-group">
										<div class="col col-lg-6">
											<label for="text-input" class=" form-control-label">Address</label>
											<input type="text" id="address" name="address"
												value="${user.address}" placeholder="Enter Address"
												class="form-control"
												<c:if test="${actionMode == 'view'}">
										readonly
									</c:if>>
										</div>
									</div>
									<div class="row form-group">
										<div class="col col-lg-3">
											<label for="text-input" class=" form-control-label">Mobile
												No. <span class="required">*</span>
											</label> <input type="text" id="mobileNo" name="mobileNo" required
												value="${user.mobileNo}" placeholder="Enter Mobile No"
												class="form-control"
												<c:if test="${actionMode == 'view'}">
										readonly
									</c:if>>
										</div>

										<div class="col col-lg-3">
											<label for="text-input" class=" form-control-label">PAN
												No</label> <input type="text" id="panNo" name="panNo"
												value="${user.panNo}" placeholder="Enter PAN No"
												class="form-control"
												<c:if test="${actionMode == 'view'}">
										readonly
									</c:if>>
										</div>
									</div>
									<div class="row form-group"></div>
									<div class="card-footer">
										<c:if test="${actionMode != 'view'}">
											<button type="submit" class="btn btn-primary btn-sm">
												<i class="fa fa-dot-circle-o"></i> Submit
											</button>
										</c:if>
										<button type="button" class="btn btn-danger btn-sm"
											onClick="javascript:closeBtn();">
											<i class="fa fa-dot-circle-o"></i> Close
										</button>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- .animated -->
		</div>
		<!-- .content -->
	</div>
	<!-- 	MAIN CODE END HERE -->
	<jsp:include page="../footer.jsp"></jsp:include>
	<script>
		function closeBtn() {
			window.history.back();
		}
	</script>
</body>
</html>
