<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" lang="">
<!--<![endif]-->
<head>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page isELIgnored="false" %>
<jsp:include page="../header.jsp"></jsp:include>
</head>
<body>
	<jsp:include page="../menu.jsp"></jsp:include>
	<!-- 	MAIN CODE START HERE -->
	<div id="right-panel" class="right-panel">
		<!-- Header-->
		<jsp:include page="../topHeader.jsp"></jsp:include>
		<!-- Header-->
		<div class="breadcrumbs">
			<div class="col-sm-4">
				<div class="page-header float-left">
					<div class="page-title">
						<h1>Sell Receipt Details</h1>
					</div>
				</div>
			</div>
		</div>
		<div class="content mt-3">
			<div class="animated fadeIn">
				<div class="row">
					<div class="col-md-12">
						<div class="card">
							<div class="card-body">
								<form action="/receipt/submitSaleReceipt" method="post"
									id="frm" class="form-horizontal">
									<input type="hidden" name="actionMode" id="actionMode" value="${actionMode}" />
<%-- 									<input type="hidden" name="saleId" id="saleId" value="${id}" /> --%>
									<c:if test="${message != null && message != ''}">
										<div
											class="alert with-close alert-success alert-dismissible fade show">
											<span class="badge badge-pill badge-success">Success</span>${message}
											<button type="button" class="close" data-dismiss="alert"
												aria-label="Close">
												<span aria-hidden="true">&times;</span>
											</button>
										</div>
									</c:if>
									<div class="row form-group">
										<div class="col col-lg-4">
											<label for="text-input" class=" form-control-label">Invoice
												No <span class="required">*</span>
											</label> <input type="text" name="invoiceNo" id="invoiceNo"
												value="${invoiceNo}" placeholder="Invoice No"
												class="form-control">
										</div>
										<div class="col col-lg-3" style="padding-top: 30px">
											<button type="button" onClick="getReceiptDetails()"
												class="btn btn-primary btn-sm">
												<i class="fa fa-dot-circle-o"></i> Check Receipt
											</button>
										</div>
									</div>
									<div id="paymentDetailsDiv" class="row form-group"></div>
									<div class="row form-group">
										<div class="col col-lg-4">
											<label for="text-input" class=" form-control-label">Amount
												Received <span class="required">*</span>
											</label> <input type="text" name="receiptAmount" id="receiptAmount"
												value="${receiptAmount}" placeholder="Amount"
												class="form-control" required>
										</div>
										<div class="col col-lg-3">
											<label for="text-input" class=" form-control-label">Receipt
												Date <span class="required">*</span>
											</label> <input type="text" id="receiptDate" name="receiptDate"
												required placeholder="Receipt Date"
												class="form-control dtPicker">
										</div>
										<div class="col col-lg-4">
											<label for="text-input" class=" form-control-label">Mode
												<span class="required">*</span>
											</label> <select name="paymentMode" id="paymentMode"
												class="form-control">
												<option value="CASH">CASH</option>
												<option value="BANK">BANK</option>
											</select>
										</div>
									</div>
									<div class="row form-group">
										<div class="col col-lg-6">
											<label for="text-input" class=" form-control-label">Remarks
											</label> <input type="text" name="remarks" id="remarks"
												value="${remarks}" placeholder="Remarks"
												class="form-control">
										</div>
									</div>
									<div class="card-footer">
										<button type="submit" class="btn btn-primary btn-sm">
											<i class="fa fa-dot-circle-o"></i> Submit
										</button>
										<button type="button" class="btn btn-danger btn-sm"
											onClick="javascript:closeBtn();">
											<i class="fa fa-dot-circle-o"></i> Close
										</button>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- .animated -->
		</div>
		<!-- .content -->
	</div>
	<!-- 	MAIN CODE END HERE -->
	<jsp:include page="../footer.jsp"></jsp:include>
	<link type="text/css"
		href="//gyrocode.github.io/jquery-datatables-checkboxes/1.2.10/css/dataTables.checkboxes.css"
		rel="stylesheet" />
	<script type="text/javascript"
		src="//gyrocode.github.io/jquery-datatables-checkboxes/1.2.10/js/dataTables.checkboxes.min.js"></script>
	<script type="text/javascript">
		//var $j = jQuery.noConflict();
		var table;
		$(document).ready(function() {
			table = $('#datatable').DataTable();

		});

		function getReceiptDetails() {
			if ($("#invoiceNo").val() == "") {
				alert("Please Mention InvoiceNo.")
			} else {
				$.ajax({
					type : 'GET',
					async : false,
					url : '/receipt/getReceiptDetailsAjax?actionMode=' + $("#actionMode").val() + '&invoiceNo='
							+ $("#invoiceNo").val(),
					success : function(data, status) {
						$("#paymentDetailsDiv").html(data);
					}
				});
			}
		}
	</script>
</body>
</html>
