<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" lang="">
<!--<![endif]-->
<head>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page isELIgnored="false" %>
</head>
<body>
	<!-- 	MAIN CODE START HERE -->
	<div id="right-panel" class="right-panel">
		<!-- Header-->
		<!-- Header-->
		<input type="hidden" name="saleId" value="${salesMaster.id}"/>
		<div class="content mt-3">
			<div class="animated fadeIn">
				<div class="row">
					<div class="col-md-12">
						<div class="card">
							<div class="card-body">
								<div class="row form-group">
									<div class="col col-lg-4">
										<label for="text-input" class=" form-control-label">
											Party Name : ${invoiceListDVO.partyName} </label>
									</div>
									<div class="col col-lg-4">
										<label for="text-input" class=" form-control-label">
											Sell Date : ${salesMaster.saleDate} </label>
									</div>
									<div class="col col-lg-4">
										<label for="text-input" class=" form-control-label">
											Due Date : ${salesMaster.dueDate} </label>
									</div>
								</div>
								<div class="row form-group">
									<div class="col col-lg-4">
										<label for="text-input" class=" form-control-label">
											Invoice No : ${salesMaster.invoiceNo} </label>
									</div>
									<div class="col col-lg-4">
										<label for="text-input" class=" form-control-label">
											Bill Amount : ${salesMaster.billAmountMyCurrency} (${salesMaster.myCurrencyUnit}) </label>
									</div>
									<div class="col col-lg-4">
										<label for="text-input" class=" form-control-label">
											Received Amount : ${invoiceListDVO.paidAmount} (${salesMaster.myCurrencyUnit}) </label>
									</div>
									<div class="col col-lg-4">
										<label for="text-input" class=" form-control-label">
											Remaining Amount : ${invoiceListDVO.remainingAmount} (${salesMaster.myCurrencyUnit}) </label>
									</div>
								</div>
								<div class="row form-group">
									<table id="datatable" class="table table-striped table-bordered">
									<thead>
										<tr>
											<th>Receipt Date</th>
											<th>Amount</th>
											<th>Remarks</th>
											<th>Delete</th>
										</tr>
									</thead>
									<tbody>
										<c:forEach items="${invoiceListDVO.receiptHistroyList}" var="receipt">
											<tr>
												<td>${receipt.receiptDate}</td>
												<td>${receipt.receiptAmount} (${salesMaster.myCurrencyUnit})</td>
												<td>${receipt.remarks}</td>
												<td><button type="button" onClick="deleteRecord(${receipt.id})"
												class="btn btn-danger btn-sm">
												<i class="fa fa-dot-circle-o"></i> Delete Receipt
											</button></td>
											</tr>
										</c:forEach>
									</tbody>
									<tfoot>
										<tr>
											<th>Receipt Date</th>
											<th>Amount</th>
											<th>Remarks</th>
											<th>Delete</th>
										</tr>
									</tfoot>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- .animated -->
		</div>
		<!-- .content -->
	</div>
	<!-- 	MAIN CODE END HERE -->
	<link type="text/css"
		href="//gyrocode.github.io/jquery-datatables-checkboxes/1.2.10/css/dataTables.checkboxes.css"
		rel="stylesheet" />
	<script type="text/javascript"
		src="//gyrocode.github.io/jquery-datatables-checkboxes/1.2.10/js/dataTables.checkboxes.min.js"></script>
	<script type="text/javascript">
		//var $j = jQuery.noConflict();
		var table;
		$(document).ready(function() {
			var table = $('.table').DataTable();
			
			 $('.table tfoot th:nth-last-child(n+2)').each( function () {
			     var title = $(this).text();
			     $(this).html( '<input type="text" size="5" placeholder="Search" />' );
			     //$(this).html( '<input type="text" placeholder="Search '+title+'" />' );
			 } );
		   
		   table.columns().every( function () {
			     var that = this;
			     $( 'input', this.footer() ).on( 'keyup change', function () {
			         if ( that.search() !== this.value ) {
			             that
			                 .search( this.value )
			                 .draw();
			         }
			     } );
			 } );
		});
		
		function deleteRecord(id)
		{
			if(confirm("Sure to delete this receipt?"))
			{
			window.location.href = "/receipt/deleteReceipt?id=" + id + "&invoiceNo=" + $("#invoiceNo").val();
			}
		}
	</script>
</body>
</html>
