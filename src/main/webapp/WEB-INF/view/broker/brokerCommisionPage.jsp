<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" lang="">
<!--<![endif]-->
<head>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page isELIgnored="false" %>
<jsp:include page="../header.jsp"></jsp:include>
</head>
<body>
	<jsp:include page="../menu.jsp"></jsp:include>
	<!-- 	MAIN CODE START HERE -->
	<div id="right-panel" class="right-panel">
		<!-- Header-->
		<jsp:include page="../topHeader.jsp"></jsp:include>
		<!-- Header-->
		<div class="breadcrumbs">
			<div class="col-sm-4">
				<div class="page-header float-left">
					<div class="page-title">
						<h1>Transfer Stock To Other Branch</h1>
					</div>
				</div>
			</div>
		</div>
		<div class="content mt-3">
			<div class="animated fadeIn">
				<div class="row">
					<div class="col-md-12">
						<div class="card">
							<div class="card-body">
								<form action="/stock/tranferToBranch" method="post" id="frm"
									class="form-horizontal">
									<input type="hidden" name="actionMode" value="${actionMode}" />
									<c:if test="${message != null && message != ''}">
										<div
											class="alert with-close alert-success alert-dismissible fade show">
											<span class="badge badge-pill badge-success">Success</span>${message}
											<button type="button" class="close" data-dismiss="alert"
												aria-label="Close">
												<span aria-hidden="true">&times;</span>
											</button>
										</div>
									</c:if>
									<div class="row form-group">
										<div class="col col-lg-4">
											<label for="text-input" class=" form-control-label">Lot
												No <span class="required">*</span>
											</label> <input type="text" name="lotNo" id="lotNo" value="${lotNo}"
												placeholder="Lot No" class="form-control">
										</div>
										<div class="col col-lg-3" style="padding-top: 30px">
											<button type="submit" class="btn btn-primary btn-sm">
												<i class="fa fa-dot-circle-o"></i> Check History
											</button>
										</div>
									</div>
									<table id="datatable"
										class="table table-striped table-bordered">
										<thead>
											<tr>
												<th>Date</th>
												<th>History</th>
											</tr>
										</thead>
										<tbody>
											<c:forEach items="${stoneHistroy}" var="stock">
												<tr>
													<td>${stock.histroyDate}</td>
													<td>${stock.remarks}</td>
												</tr>
											</c:forEach>
										</tbody>
									</table>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- .animated -->
		</div>
		<!-- .content -->
	</div>
	<!-- 	MAIN CODE END HERE -->
	<jsp:include page="../footer.jsp"></jsp:include>
	<link type="text/css"
		href="//gyrocode.github.io/jquery-datatables-checkboxes/1.2.10/css/dataTables.checkboxes.css"
		rel="stylesheet" />
	<script type="text/javascript"
		src="//gyrocode.github.io/jquery-datatables-checkboxes/1.2.10/js/dataTables.checkboxes.min.js"></script>
	<script type="text/javascript">
		//var $j = jQuery.noConflict();
		var table;
			$(document).ready(function() {
				table = $('#datatable').DataTable();
	});
	</script>
</body>
</html>
