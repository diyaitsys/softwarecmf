<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" lang="">
<!--<![endif]-->
<head>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page isELIgnored="false" %>
<jsp:include page="../header.jsp"></jsp:include>
</head>
<body>
	<jsp:include page="../menu.jsp"></jsp:include>
	<!-- 	MAIN CODE START HERE -->
	<div id="right-panel" class="right-panel">

		<!-- Header-->
		<jsp:include page="../topHeader.jsp"></jsp:include>
		<!-- Header-->

		<div class="content mt-3">
			<div class="animated fadeIn">
				<div class="row">
					<div class="col-lg-12">
						<div class="card">
							<div class="card-header">
								<strong>Dashboard</strong>
							</div>
							<c:if test="${error != null && error != ''}">
								<div
									class="alert with-close alert-danger alert-dismissible fade show">
									<span class="badge badge-pill badge-danger">Error</span>${error}
									<button type="button" class="close" data-dismiss="alert"
										aria-label="Close">
										<span aria-hidden="true">&times;</span>
									</button>
								</div>
							</c:if>
							<c:if test="${message != null && message != ''}">
								<div
									class="alert with-close alert-success alert-dismissible fade show">
									<span class="badge badge-pill badge-success">Success</span>${message}
									<button type="button" class="close" data-dismiss="alert"
										aria-label="Close">
										<span aria-hidden="true">&times;</span>
									</button>
								</div>
							</c:if>
							<div class="card-body card-block">
								<div class="col-sm-6 col-lg-3">
									<div class="card text-white bg-flat-color-1">
										<div class="card-body pb-0">
											<div class="dropdown float-right">
												<button
													class="btn bg-transparent dropdown-toggle theme-toggle text-light"
													type="button" id="dropdownMenuButton"
													data-toggle="dropdown">
													<i class="fa fa-cog"></i>
												</button>
												<div class="dropdown-menu"
													aria-labelledby="dropdownMenuButton">
													<div class="dropdown-menu-content">
														<a class="dropdown-item" href="/stock/getStockList?status=InHouse">Show
															Details</a>
													</div>
												</div>
											</div>
											<h4 class="mb-0">
												<span>${stockInHouse}</span>
											</h4>
											<p class="text-light">Stock In-House</p>
										</div>
									</div>
								</div>
								<div class="col-sm-6 col-lg-3">
									<div class="card text-white bg-flat-color-1">
										<div class="card-body pb-0">
											<div class="dropdown float-right">
												<button
													class="btn bg-transparent dropdown-toggle theme-toggle text-light"
													type="button" id="dropdownMenuButton"
													data-toggle="dropdown">
													<i class="fa fa-cog"></i>
												</button>
												<div class="dropdown-menu"
													aria-labelledby="dropdownMenuButton">
													<div class="dropdown-menu-content">
														<a class="dropdown-item" href="/stock/getStockList?status=OnMemo">Show
															Details</a>
													</div>
												</div>
											</div>
											<h4 class="mb-0">
												<span>${stockOnMemo}</span>
											</h4>
											<p class="text-light">Stock On Memo</p>
										</div>
									</div>
								</div>
								<div class="col-sm-6 col-lg-3">
									<div class="card text-white bg-flat-color-1">
										<div class="card-body pb-0">
											<div class="dropdown float-right">
												<button
													class="btn bg-transparent dropdown-toggle theme-toggle text-light"
													type="button" id="dropdownMenuButton"
													data-toggle="dropdown">
													<i class="fa fa-cog"></i>
												</button>
												<div class="dropdown-menu"
													aria-labelledby="dropdownMenuButton">
													<div class="dropdown-menu-content">
														<a class="dropdown-item" href="/stock/stockListForReceivedPage">Show
															Details</a>
													</div>
												</div>
											</div>
											<h4 class="mb-0">
												<span>${stockNotReceived}</span>
											</h4>
											<p class="text-light">Branch Transfer Received</p>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- .animated -->
		</div>
		<!-- .content -->
	</div>
	<!-- 	MAIN CODE END HERE -->
	<jsp:include page="../footer.jsp"></jsp:include>
	<script>
		function closeBtn() {
			window.history.back();
		}
	</script>
</body>
</html>
