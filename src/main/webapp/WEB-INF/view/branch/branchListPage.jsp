<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" lang="">
<!--<![endif]-->
<head>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page isELIgnored="false" %>
<jsp:include page="../header.jsp"></jsp:include>
</head>
<body>
	<jsp:include page="../menu.jsp"></jsp:include>
	<!-- 	MAIN CODE START HERE -->
	<div id="right-panel" class="right-panel">
		<!-- Header-->
		<jsp:include page="../topHeader.jsp"></jsp:include>
		<!-- Header-->
		<div class="breadcrumbs">
			<div class="col-sm-4">
				<div class="page-header float-left">
					<div class="page-title">
						<h1>Branch Module</h1>
					</div>
				</div>
			</div>
		</div>
		<div class="content mt-3">
			<div class="animated fadeIn">
				<div class="row">
					<div class="col-md-12">
						<div class="card">
							<div class="content mt-3">
								<div class="animated fadeIn">
									<div class="row">
										<div class="col-md-12">
											<a class="btn btn-primary" href="./branchPage" role="button">Add
												Branch</a>
										</div>
									</div>
								</div>
							</div>
							<div class="card-body">
								<table id="datatable" class="table table-striped table-bordered">
									<thead>
										<tr>
											<th>Branch Name</th>
											<th>Address</th>
											<th>Other Info.</th>
											<th></th>
										</tr>
									</thead>
									<tbody>
										<c:forEach items="${branchList}" var="branch">
											<tr>
												<td>${branch.name}</td>
												<td>${branch.address}</td>
												<td>${branch.otherDetails}</td>
												<td>
													<button type="button"
														onClick="viewEditBranch(${branch.id},'view')"
														class="btn btn-outline-success btn-sm">View</button>
													<button type="button"
														onClick="viewEditBranch(${branch.id},'edit')"
														class="btn btn-outline-primary btn-sm">Update</button>
													<button type="button"
														onClick="deleteBranch(${branch.id})"
														class="btn btn-outline-danger btn-sm">Delete</button>
												</td>
											</tr>
										</c:forEach>
									</tbody>
									<tfoot>
										<tr>
											<th>Branch Name</th>
											<th>Address</th>
											<th>Other Info.</th>
											<th></th>
										</tr>
									</tfoot>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- .animated -->
		</div>
		<!-- .content -->
	</div>
	<!-- 	MAIN CODE END HERE -->
	<jsp:include page="../footer.jsp"></jsp:include>
	<script type="text/javascript">
		//var $j = jQuery.noConflict();
		$(document).ready(function() {
			var table = $('.table').DataTable();
			
			 $('.table tfoot th:nth-last-child(n+2)').each( function () {
			     var title = $(this).text();
			     $(this).html( '<input type="text" size="5" placeholder="Search" />' );
			     //$(this).html( '<input type="text" placeholder="Search '+title+'" />' );
			 } );
		   
		   table.columns().every( function () {
			     var that = this;
			     $( 'input', this.footer() ).on( 'keyup change', function () {
			         if ( that.search() !== this.value ) {
			             that
			                 .search( this.value )
			                 .draw();
			         }
			     } );
			 } );
		});

		function deleteBranch(id)
		{
			if(confirm('You are sure to delete this record?'))
			{
				window.location.href="../branch/remove/" + id;
			}
		}
		
		function viewEditBranch(id,actionMode)
		{
			window.location.href="../branch/branchPage/" + actionMode + "/" + id;
		}
	</script>
</body>
</html>
