<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" lang="">
<!--<![endif]-->
<head>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page isELIgnored="false" %>
<jsp:include page="../header.jsp"></jsp:include>
</head>
<body>
	<jsp:include page="../menu.jsp"></jsp:include>
	<!-- 	MAIN CODE START HERE -->
	<div id="right-panel" class="right-panel">

		<!-- Header-->
		<jsp:include page="../topHeader.jsp"></jsp:include>
		<!-- Header-->

		<div class="content mt-3">
			<div class="animated fadeIn">
				<div class="row">
					<div class="col-lg-12">
						<div class="card">
							<div class="card-header">
								<strong>Select Branch</strong>
							</div>
							<div class="card-body card-block">
								<form action="/branch/selectBranchSubmit" method="post"
									class="form-horizontal">
									<div class="col-lg-6">
										<div class="row form-group">
											<div class="col col-md-4">
												<label for="text-input" class=" form-control-label">Name
													<span class="required">*</span>
												</label>
											</div>
											<div class="col-12 col-md-8">
												<select name="branchId" data-placeholder="Select Branch" required
													class="standardSelect" tabindex="1">
													<option value="-1"></option>
													<c:forEach items="${branchList}" var="branch">
														<option value="${branch.id}"><c:out value="${branch.name}"/></option>
													</c:forEach>
												</select>
											</div>
										</div>

										<div class="card-footer">
											<c:if test="${actionMode != 'view'}">
												<button type="submit" class="btn btn-primary btn-sm">
													<i class="fa fa-dot-circle-o"></i> Submit
												</button>
											</c:if>
											<button type="button" class="btn btn-danger btn-sm"
												onClick="javascript:closeBtn();">
												<i class="fa fa-dot-circle-o"></i> Close
											</button>
										</div>
									</div>
								</form>
							</div>

						</div>
					</div>
				</div>
			</div>
			<!-- .animated -->
		</div>
		<!-- .content -->
	</div>
	<!-- 	MAIN CODE END HERE -->
	<jsp:include page="../footer.jsp"></jsp:include>
	<script>
		function closeBtn() {
			window.history.back();
		}
	</script>
</body>
</html>
