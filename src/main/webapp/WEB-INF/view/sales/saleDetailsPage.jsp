<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" lang="">
<!--<![endif]-->

<head>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page isELIgnored="false" %>
<jsp:include page="../header.jsp"></jsp:include>
</head>

<body>
	<!-- 	MAIN CODE START HERE -->
	<div id="right-panel" class="right-panel">

		<!-- Header-->
		<!-- Header-->

		<div class="content mt-3">
			<div class="animated">
				<div class="row">
					<div class="col-lg-12">
						<div class="card">
							<div class="card-header">
								<strong>Sell Details</strong>
							</div>
							<div class="card-body card-block">
								<div class="row form-group">
									<div class="col-lg-6">
										<label for="text-input" class=" form-control-label">Party
											Name <span class="required">*</span>
										</label> <select id="partyId" name="saleMaster.partyId"
											data-placeholder="Select Party" disabled="disabled"
											class="standardSelect" tabindex="1"
											<c:if
											 test="${actionMode == 'view'}">
												readonly
												</c:if>>
											<option value=""></option>
											<c:forEach items="${partyList}" var="party">
												<option value="${party.id}"
													<c:if test="${saleMaster.partyId == party.id}">selected</c:if>>
													${party.name}</option>
											</c:forEach>
										</select>
									</div>
									<div class="col-lg-6">
										<div class="form-group">
											<label for="company" class=" form-control-label">Remark</label>
											<input type="text" id="remarks" name="saleMaster.remarks"
												placeholder="Enter remark" value="${saleMaster.remarks}"
												readonly readonly class="form-control">
										</div>
									</div>

								</div>
								<div class="row form-group">

									<div class="col col-lg-3">
										<label for="text-input" class=" form-control-label">InvoiceNo
											<span class="required">*</span>
										</label> <input type="text" id="invoiceNo"
											name="saleMaster.invoiceNo" placeholder="Invoice No"
											required value="${saleMaster.invoiceNo}" readonly
											class="form-control">
									</div>
									<div class="col col-lg-3">
										<label for="text-input" class=" form-control-label">Sell
											Date <span class="required">*</span>
										</label> <input type="text" id="saleDate"
											name="saleMaster.saleDate" readonly
											placeholder="Purchase Date"
											value="${saleMaster.saleDate}" class="form-control"
											readonly>
									</div>
									<div class="col-lg-3">
										<div class="form-group">
											<label for="company" class=" form-control-label">Terms
												(Days) <span class="required">*</span>
											</label> <input type="text" id="terms"
												value="${saleMaster.termsDays}" readonly required
												placeholder="Enter Terms" class="form-control">
										</div>
									</div>
									<div class="col col-lg-3">
										<label for="text-input" class=" form-control-label">Due
											Date </label> <input type="text" id="dueDate"
											name="saleMaster.dueDate" placeholder="Due Date" readonly
											value="${saleMaster.dueDate}" class="form-control">
									</div>
								</div>
								<div class="row form-group">

									<div class="col-lg-2">
										<div class="form-group">
											<label for="company" class=" form-control-label">Sell
												Currency <span class="required">*</span>
											</label> <select id="saleCurrencyUnit"
												name="saleMaster.saleCurrencyUnit"
												data-placeholder="Select Currency" required
												disabled="disabled"
												class="form-control"
												<c:if test="${actionMode == 'view'}">
														readonly
													</c:if>>
												<c:forEach items="${currencyMap}" var="currency">
													<option value="${currency.key}"
														<c:if test="${saleMaster.saleCurrencyUnit== currency.key}">selected</c:if>>${currency.value}</option>
												</c:forEach>
											</select>
										</div>
									</div>
									<div class="col-lg-2">
										<div class="form-group">
											<label for="company" class=" form-control-label">Convert
												Rate <span class="required">*</span>
											</label> <input type="text" id="conversionRate"
												value="${saleMaster.conversionRate}" required readonly
												placeholder="Convert Rate" class="form-control" value="1">
										</div>
									</div>
									<div class="col-lg-2">
										<div class="form-group">
											<label for="company" class=" form-control-label">My
												Currency <span class="required">*</span>
											</label> <select id="myCurrencyUnit"
												name="saleMaster.myCurrencyUnit" class="form-control"
												data-placeholder="Select Currency" disabled="disabled"
												<c:if test="${actionMode == 'view'}">
														readonly
													</c:if>>
												<c:forEach items="${currencyMap}" var="currency">
													<option value="${currency.key}"
														<c:if test="${saleMaster.myCurrencyUnit== currency.key}">selected</c:if>>${currency.value}</option>
												</c:forEach>
											</select>
										</div>
									</div>
								</div>

								<!-- 		Datatable Start -->
								<div class="content mt-3">
									<div class="animated fadeIn">
										<div class="row">
											<div class="col-md-12">
												<div class="card">
													<div class="card-header">
														<strong class="card-title">Stock Details</strong>
													</div>
													<div class="card-body">
														<table id="dataTable"
															class="table table-striped table-bordered">
															<thead>
																<tr>
																	<th>Lot No</th>
																	<th>Carat</th>
																	<th>Shape</th>
																	<th>Color</th>
																	<th>Clarity</th>
																	<th>Total Price</th>
																	<th></th>
																</tr>
															</thead>
															<tbody>
																<c:forEach items="${stockList}" var="stock">
																	<tr>
																		<td>${stock.lotNo}</td>
																		<td>${stock.carret}</td>
																		<td>${stock.shape}</td>
																		<td>${stock.color}</td>
																		<td>${stock.clarity}</td>
																		<td><fmt:formatNumber type="number"
																				maxFractionDigits="2"
																				value="${stock.totalPrice * saleMaster.conversionRate}" />
																			(${saleMaster.myCurrencyUnit})</td>
																		<td>
																			<button type="button"
																				onClick="viewStock(${stock.id})"
																				class="btn btn-outline-success btn-sm">Details</button>
																		</td>
																	</tr>
																</c:forEach>
															</tbody>
															<tfoot>
																<tr>
																	<th>Lot No</th>
																	<th>Carat</th>
																	<th>Shape</th>
																	<th>Color</th>
																	<th>Clarity</th>
																	<th>Total Price</th>
																	<th></th>
																</tr>
															</tfoot>
														</table>
													</div>
												</div>
											</div>
										</div>
									</div>
									<!-- .animated -->
								</div>

								<!-- 		Datatable End -->

								<!-- 		Bottom Page Start -->
								<div class="content mt-3">
									<div class="animated fadeIn">
										<div class="card">
											<div class="card-body">
												<div class="row form-group">
													<div class="col-lg-3">
														<div class="form-group">
															<label for="company" class=" form-control-label">Net
																Price (Sell Currency) </label>
															<div class="input-group">
																<input type="text" id="netPriceSaleCurrency"
																	value="${saleMaster.netPriceSaleCurrency}" readonly
																	placeholder="Net Price" class="form-control">
																<div id="netPriceSaleCurrentUnit"
																	class="input-group-addon">${saleMaster.saleCurrencyUnit}</div>
															</div>
														</div>
													</div>
													<div class="col-lg-3">
														<div class="form-group">
															<label for="company" class=" form-control-label">Net
																Price (My Currency) </label>
															<div class="input-group">
																<input type="text" id="netPriceMyCurrency"
																	value="${saleMaster.netPriceMyCurrency}" readonly
																	placeholder="Net Price" class="form-control">
																<div id="netPriceMyCurrencyUnit"
																	class="input-group-addon">${saleMaster.myCurrencyUnit}</div>
															</div>
														</div>
													</div>
												</div>
												<div class="row form-group">
													<div class="col-lg-4">
														<div class="form-group">
															<label for="company" class=" form-control-label">Broker
																Name </label> <select name="saleMaster.brokerId"
																data-placeholder="Select Broker" class="standardSelect"
																disabled="disabled" tabindex="1"
																<c:if test="${actionMode == 'view'}">
												readonly
												</c:if>>
																<option value=""></option>
																<c:forEach items="${brokerList}" var="broker">
																	<option value="${broker.id}"
																		<c:if test="${saleMaster.brokerId == broker.id}">selected</c:if>>${broker.name}</option>
																</c:forEach>
															</select>
														</div>
													</div>
													<div class="col-lg-2">
														<div class="form-group">
															<label for="company" class=" form-control-label">Brokerage
															</label>
															<div class="input-group">
																<input type="text" id="brokerage"
																	placeholder="Brokerage" class="form-control"
																	value="${saleMaster.brokeragePercentage}" readonly>
																<div class="input-group-addon">%</div>
															</div>
														</div>
													</div>
													<div class="col-lg-4">
														<div class="form-group">
															<label for="company" class=" form-control-label">Brokerage
																Amount (My Current)</label>
															<div class="input-group">
																<input type="text" id="brokerageAmount"
																	value="${saleMaster.brokerageAmount}"
																	placeholder="Brokerage Amount" class="form-control"
																	readonly>
																<div id="brokerageAmountUnit" class="input-group-addon">${saleMaster.myCurrencyUnit}</div>
															</div>
														</div>
													</div>
												</div>
												<div class="row form-group">
													<div class="col-lg-2">
														<div class="form-group">
															<label for="company" class=" form-control-label">Tax
																Details </label> <select name="saleMaster.taxDetails"
																id="taxDetails" onChange="taxChange()"
																disabled="disabled" class="form-control">
																<option value="0"
																	<c:if test="${saleMaster.taxDetails == 0}">selected</c:if>>Without
																	Tax</option>
																<option value="1"
																	<c:if test="${saleMaster.taxDetails == 1}">selected</c:if>>IGST</option>
																<option value="2"
																	<c:if test="${saleMaster.taxDetails == 2}">selected</c:if>>CGST/SGST</option>
															</select>
														</div>
													</div>
													<c:if test="${saleMaster.IGST != 0}">
														<div class="col-lg-2" id="IGSTDiv">
															<div class="form-group">
																<label for="company" class=" form-control-label">IGST</label>
																<input type="text" id="igst" placeholder="Enter IGST"
																	value="${saleMaster.IGST}" class="form-control"
																	readonly>

															</div>
														</div>
													</c:if>
													<c:if test="${saleMaster.CGST != 0}">
														<div class="col-lg-2" id="CGSTDiv">
															<div class="form-group">
																<label for="company" class=" form-control-label">CGST</label>
																<input type="text" id="cgst" placeholder="Enter CGST"
																	value="${saleMaster.CGST}" class="form-control"
																	readonly>
															</div>
														</div>
													</c:if>
													<c:if test="${saleMaster.SGST != 0}">
														<div class="col-lg-2" id="SGSTDiv">
															<div class="form-group">
																<label for="company" class=" form-control-label">SGST</label>
																<input type="text" id="sgst"
																	value="${saleMaster.SGST}" placeholder="Enter SGST"
																	class="form-control" readonly>
															</div>
														</div>
													</c:if>
												</div>
												<div class="row form-group">
													<div class="col-lg-4">
														<div class="form-group">
															<label for="company" class=" form-control-label">Bill
																Amount (Sale Currency)</label>
															<div class="input-group">
																<input type="text" id="billAmountSaleCurrency"
																	value="${saleMaster.billAmountSaleCurrency}"
																	readonly placeholder="Bill Amount" class="form-control">
																<div id="billAmountSaleCurrencyUnit"
																	class="input-group-addon">${saleMaster.saleCurrencyUnit}</div>
															</div>
														</div>
													</div>
													<div class="col-lg-4">
														<div class="form-group">
															<label for="company" class=" form-control-label">Bill
																Amount (My Currency)</label>
															<div class="input-group">
																<input type="text" id="billAmountMyCurrency"
																	value="${saleMaster.billAmountMyCurrency}" readonly
																	placeholder="Bill Amount" class="form-control">
																<div id="billAmountMyCurrencyUnit"
																	class="input-group-addon">${saleMaster.myCurrencyUnit}</div>
															</div>

														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="card-footer">
									<button type="button" class="btn btn-danger btn-sm"
										onClick="javascript:window.close();">
										<i class="fa fa-dot-circle-o"></i> Close
									</button>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- 		Bottom Page End -->
			<!-- .content -->
		</div>
		<!-- 	MAIN CODE END HERE -->
		<jsp:include page="../footer.jsp"></jsp:include>
		<script>
			var totalStock = 0;
			var table = $('.table').DataTable();
			
			 $('.table tfoot th:nth-last-child(n+2)').each( function () {
			     var title = $(this).text();
			     $(this).html( '<input type="text" size="5" placeholder="Search" />' );
			     //$(this).html( '<input type="text" placeholder="Search '+title+'" />' );
			 } );
		   
		   table.columns().every( function () {
			     var that = this;
			     $( 'input', this.footer() ).on( 'keyup change', function () {
			         if ( that.search() !== this.value ) {
			             that
			                 .search( this.value )
			                 .draw();
			         }
			     } );
			 } );
			
			function viewStock(id)
			{
				window.open("/stock/getStockDetailsById?id=" + id, "_blank");
			}
		</script>
</body>

</html>