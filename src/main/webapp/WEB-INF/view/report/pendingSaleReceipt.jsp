<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" lang="">
<!--<![endif]-->
<head>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix = "fmt" uri = "http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<jsp:include page="../header.jsp"></jsp:include>
<link
	href='https://cdn.datatables.net/buttons/1.5.2/css/buttons.dataTables.min.css'
	rel='stylesheet' type='text/css'>
</head>
<body>
	<jsp:include page="../menu.jsp"></jsp:include>
	<!-- 	MAIN CODE START HERE -->
	<div id="right-panel" class="right-panel">
		<!-- Header-->
		<jsp:include page="../topHeader.jsp"></jsp:include>
		<!-- Header-->
		<div class="breadcrumbs">
			<div class="col-sm-4">
				<div class="page-header float-left">
					<div class="page-title">
						<c:choose>
							<c:when test="${actionMode=='pending'}">
								<h1>Pending Sell Receipt</h1>
							</c:when>
							<c:otherwise>
								<h1>Sell Receipt Done</h1>
							</c:otherwise>
						</c:choose>
					</div>
				</div>
			</div>
		</div>
		<div class="content mt-3">
			<div class="animated fadeIn">
				<div class="row">
					<div class="col-md-12">
						<div class="card">
							<div class="card-body">
								<table id="datatable" class="table table-striped table-bordered">
									<thead>
										<tr>
											<th>Sell Date</th>
											<th>Invoice No</th>
											<th>Due Date</th>
											<th>Party Name</th>
											<th>Bill Amount</th>
											<th>Received Amount</th>
											<th>Remaining Amount</th>
											<th></th>
										</tr>
									</thead>
									<tbody>
										<c:forEach items="${pendingReceipt}" var="pm">
											<tr>
												<td><fmt:formatDate pattern = "dd/MM/yyyy" value = "${pm.saleDate}" /></td>
												<td>${pm.invoiceNo}</td>
												<td>${pm.dueDate}</td>
												<td>${pm.partyName}</td>
												<td>${pm.billAmountMyCurrency} (${pm.myCurrencyUnit})</td>
												<td>${pm.paidAmount} (${pm.myCurrencyUnit})</td>
												<td>${pm.remainingAmount} (${pm.myCurrencyUnit})</td>
												<td>
													<button type="button"
														onClick="viewInvoice('${pm.invoiceNo}',${pm.id})"
														class="btn btn-outline-success btn-sm">Details</button>
												</td>
											</tr>
										</c:forEach>
									</tbody>
									<tfoot>
										<tr>
											
											<th>Purchase Date</th>
											<th>Invoice No</th>
											<th>Due Date</th>
											<th>Party Name</th>
											<th>Bill Amount</th>
											<th>Paid Amount</th>
											<th>Remaining Amount</th>
											<th></th>
										</tr>
									</tfoot>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- .animated -->
		</div>
		<!-- .content -->
	</div>
	<!-- 	MAIN CODE END HERE -->
	<jsp:include page="../footer.jsp"></jsp:include>
	<script src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
	<script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.flash.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
	<script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js"></script>
	<script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.print.min.js"></script>
	<script type="text/javascript">
		//var $j = jQuery.noConflict();
		$(document).ready(function() {
			var table = $('.table').DataTable( {
		        dom: 'Bfrtip',
		        buttons: [
		            'copy', 'csv', 'excel', 'pdf', 'print'
		        ]
		    } );
			
			 $('.table tfoot th:nth-last-child(n+2)').each( function () {
			     var title = $(this).text();
			     $(this).html( '<input type="text" size="5" placeholder="Search" />' );
			     //$(this).html( '<input type="text" placeholder="Search '+title+'" />' );
			 } );
		   
		   table.columns().every( function () {
			     var that = this;
			     $( 'input', this.footer() ).on( 'keyup change', function () {
			         if ( that.search() !== this.value ) {
			             that
			                 .search( this.value )
			                 .draw();
			         }
			     } );
			 } );
		});

		function viewInvoice(invoiceNo,id)
		{
			window.open("/receipt/receiptPage?actionMode=sales&id=" + id + "&invoiceNo=" + encodeURIComponent(invoiceNo), "_blank");
		}
	</script>
</body>
</html>
