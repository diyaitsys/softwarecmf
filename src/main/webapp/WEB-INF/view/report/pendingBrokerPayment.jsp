<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" lang="">
<!--<![endif]-->
<head>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page isELIgnored="false" %>
<jsp:include page="../header.jsp"></jsp:include>
</head>
<body>
	<jsp:include page="../menu.jsp"></jsp:include>
	<!-- 	MAIN CODE START HERE -->
	<div id="right-panel" class="right-panel">
		<!-- Header-->
		<jsp:include page="../topHeader.jsp"></jsp:include>
		<!-- Header-->
		<div class="breadcrumbs">
			<div class="col-sm-4">
				<div class="page-header float-left">
					<div class="page-title">
						<c:choose>
							<c:when test="${actionMode=='pending'}">
								<h1>Pending Broker Payment</h1>
							</c:when>
							<c:otherwise>
								<h1>Broker Payment Done</h1>
							</c:otherwise>
						</c:choose>
					</div>
				</div>
			</div>
		</div>
		<div class="content mt-3">
			<div class="animated fadeIn">
				<div class="row">
					<div class="col-md-12">
						<div class="card">
							<div class="card-body">
								<table id="datatable" class="table table-striped table-bordered">
									<thead>
										<tr>
											
											<th>Purchase Date</th>
											<th>Invoice No</th>
											<th>Due Date</th>
											<th>Broker Name</th>
											<th>Brokerage Amount</th>
											<th>Bill Amount</th>
											<th>Paid Amount</th>
											<th>Remaining Amount</th>
											<th></th>
										</tr>
									</thead>
									<tbody>
										<c:forEach items="${pendingPayments}" var="pm">
											<tr>
												<td>${pm.purchaseDate}</td>
												<td>${pm.invoiceNo}</td>
												<td>${pm.dueDate}</td>
												<td>${pm.partyName}</td>
												<td>${pm.brokerageAmount} (${pm.myCurrencyUnit})</td>
												<td>${pm.billAmountMyCurrency} (${pm.myCurrencyUnit})</td>
												<td>${pm.paidAmount} (${pm.myCurrencyUnit})</td>
												<td>${pm.remainingAmount} (${pm.myCurrencyUnit})</td>
												<td>
													<button type="button"
														onClick="viewInvoice('${pm.invoiceNo}')"
														class="btn btn-outline-success btn-sm">Details</button>
												</td>
											</tr>
										</c:forEach>
									</tbody>
									<tfoot>
										<tr>
											
											<th>Purchase Date</th>
											<th>Invoice No</th>
											<th>Due Date</th>
											<th>Broker Name</th>
											<th>Brokerage Amount</th>
											<th>Bill Amount</th>
											<th>Paid Amount</th>
											<th>Remaining Amount</th>
											<th></th>
										</tr>
									</tfoot>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- .animated -->
		</div>
		<!-- .content -->
	</div>
	<!-- 	MAIN CODE END HERE -->
	<jsp:include page="../footer.jsp"></jsp:include>
	<script type="text/javascript">
		//var $j = jQuery.noConflict();
		$(document).ready(function() {
			var table = $('.table').DataTable();
			
			 $('.table tfoot th:nth-last-child(n+2)').each( function () {
			     var title = $(this).text();
			     $(this).html( '<input type="text" size="5" placeholder="Search" />' );
			     //$(this).html( '<input type="text" placeholder="Search '+title+'" />' );
			 } );
		   
		   table.columns().every( function () {
			     var that = this;
			     $( 'input', this.footer() ).on( 'keyup change', function () {
			         if ( that.search() !== this.value ) {
			             that
			                 .search( this.value )
			                 .draw();
			         }
			     } );
			 } );
		});

		function viewInvoice(id)
		{
			window.open("/payment/paymentPage?actionMode=broker&invoiceNo=" + encodeURIComponent(id), "_blank");
		}
	</script>
</body>
</html>
