package com.cmf.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import com.cmf.dto.StockOnLab;

@Repository
public interface StockOnLabRepository extends CrudRepository<StockOnLab, String>
{

	/**
	 * Find by branch id and status.
	 *
	 * @param sessionBranchId the session branch id
	 * @param active the active
	 * @return the list
	 */

	StockOnLab findByBranchId(long sessionBranchId);

	StockOnLab findById(long id);

	StockOnLab findByStockId(long id);

	long deleteByStockId(long id);

}
