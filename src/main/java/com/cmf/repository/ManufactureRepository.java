package com.cmf.repository;

import java.util.List;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import com.cmf.dto.Manufacture;

@Repository
public interface ManufactureRepository extends CrudRepository<Manufacture, String>
{

	/**
	 * Find by branch id and status.
	 *
	 * @param sessionBranchId the session branch id
	 * @param active the active
	 * @return the list
	 */
	List<Manufacture> findByIdAndStatus(long id, int active);

	List<Manufacture> findByBranchIdAndStatus(long sessionBranchId, int statusActive);

	Manufacture findById(long id);

	List<Manufacture> findByBranchIdAndStatusOrderByNameAsc(long sessionBranchId, int i);

}
