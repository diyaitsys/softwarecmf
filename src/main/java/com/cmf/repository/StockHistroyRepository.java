package com.cmf.repository;

import java.util.List;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import com.cmf.dto.StoneHistroy;

@Repository
public interface StockHistroyRepository extends CrudRepository<StoneHistroy, String>
{
	StoneHistroy findById(long id);

	StoneHistroy findByStockId(long id);

	List<StoneHistroy> findByStockIdOrderById(long id);

	List<StoneHistroy> findByStockIdOrderByIdDesc(long id);
}
