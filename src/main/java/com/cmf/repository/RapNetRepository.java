package com.cmf.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import com.cmf.dto.RapNet;

@Repository
public interface RapNetRepository extends CrudRepository<RapNet, String>
{

	/**
	 * Find by branch id and status.
	 *
	 * @param sessionBranchId the session branch id
	 * @param active the active
	 * @return the list
	 */

	RapNet findByEntityId(long sessionBranchId);

}
