package com.cmf.repository;

import java.util.List;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import com.cmf.dto.Broker;

@Repository
public interface BrokerRepository extends CrudRepository<Broker, String>
{

	/**
	 * Find by id and status.
	 *
	 * @param sessionBranchId the session branch id
	 * @param active the active
	 * @return the list
	 */
	List<Broker> findByBranchIdAndStatusOrderByNameAsc(long sessionBranchId, int active);

	Broker findById(Long valueOf);
}
