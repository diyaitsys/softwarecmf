/**
 * 
 */
package com.cmf.repository;

import java.util.List;
import org.springframework.data.repository.CrudRepository;
import com.cmf.dto.PurchaseMaster;

public interface PurchaseRepository extends CrudRepository<PurchaseMaster, String>
{

	/**
	 * Find by branch id and status.
	 *
	 * @param sessionBranchId the session branch id
	 * @param active the active
	 * @return the list
	 */
	List<PurchaseMaster> findByBranchIdAndStatus(long sessionBranchId, int active);

	List<PurchaseMaster> findByBranchIdAndStatusOrderByPurchaseDateDesc(long sessionBranchId, int active);

	PurchaseMaster findById(Long valueOf);

	PurchaseMaster findByInvoiceNoAndBranchId(String invoiceNo, long sessionBranchId);

	List<PurchaseMaster> findByBranchId(long sessionBranchId);

}
