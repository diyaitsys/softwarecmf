package com.cmf.repository;

import java.util.List;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import com.cmf.dto.Branch;

@Repository
public interface BranchRepository extends CrudRepository<Branch, String>
{

	/**
	 * Find by branch id and status.
	 *
	 * @param sessionBranchId the session branch id
	 * @param active the active
	 * @return the list
	 */
	List<Branch> findByIdAndStatus(long id, int active);

	List<Branch> findByEntityIdAndStatus(long sessionBranchId, int statusActive);

	Branch findById(Long valueOf);

}
