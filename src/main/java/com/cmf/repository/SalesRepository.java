/**
 * 
 */
package com.cmf.repository;

import java.util.List;
import org.springframework.data.repository.CrudRepository;
import com.cmf.dto.SalesMaster;

public interface SalesRepository extends CrudRepository<SalesMaster, String>
{

	/**
	 * Find by branch id and status.
	 *
	 * @param sessionBranchId the session branch id
	 * @param active the active
	 * @return the list
	 */
	List<SalesMaster> findByBranchIdAndStatus(long sessionBranchId, int active);

	List<SalesMaster> findByBranchIdAndStatusOrderBySaleDateDesc(long sessionBranchId, int active);

	SalesMaster findById(Long valueOf);

	SalesMaster findByInvoiceNoAndBranchId(String invoiceNo, long sessionBranchId);

	List<SalesMaster> findByBranchId(long sessionBranchId);

}
