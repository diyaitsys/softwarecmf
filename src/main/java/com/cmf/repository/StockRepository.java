/**
 * 
 */
package com.cmf.repository;

import java.util.List;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import com.cmf.dto.Stock;

public interface StockRepository extends CrudRepository<Stock, String>
{

	/**
	 * Find by branch id and status.
	 *
	 * @param sessionBranchId the session branch id
	 * @param active the active
	 * @return the list
	 */
	List<Stock> findByPurchaseMasterId(long purchaseMasterId);

	Stock findById(long valueOf);

	@Query(value = "SELECT * FROM Stock WHERE (BRANCH_ID = ?2 OR CURRENT_BRANCH_ID = ?2) AND LOT_NO = ?1 AND STOCK_TYPE = ?3", nativeQuery = true)
	Stock findByLotNoAndBranchIdAndStockType(String lotNo, long sessionBranchId, String stockType);

	@Query(value = "SELECT * FROM Stock WHERE (BRANCH_ID = ?2 OR CURRENT_BRANCH_ID = ?2) AND ID = ?1", nativeQuery = true)
	Stock findByIdAndBranchId(long id, long sessionBranchId);

	List<Stock> findByCurrentBranchIdAndTransit(long sessionBranchId, int i);

	List<Stock> findByCurrentBranchIdAndStockStatus(long sessionBranchId, String string);

	Stock findByLotNoAndEntityIdAndStockType(String lotNo, long sessionEntityId, String stockType);

	List<Stock> findBySalesMasterId(long saleId);

	Stock findByIdAndCurrentBranchIdAndStockStatus(long id, long sessionBranchId, String string);

}
