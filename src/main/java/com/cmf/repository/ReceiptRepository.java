package com.cmf.repository;

import java.util.List;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import com.cmf.dto.Receipt;

@Repository
public interface ReceiptRepository extends CrudRepository<Receipt, String>
{
	Receipt findById(long id);

	List<Receipt> findBySaleIdAndBranchId(long id, long sessionBranchId);

	void deleteById(long id);

}
