package com.cmf.repository;

import java.util.Date;
import java.util.List;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import com.cmf.dto.StockOnMemoMaster;

@Repository
public interface StockOnMemoMasterRepository extends CrudRepository<StockOnMemoMaster, String>
{

	List<StockOnMemoMaster> findByBranchIdOrderByMemoDateDesc(long sessionBranchId);

	StockOnMemoMaster findById(long id);

	List<StockOnMemoMaster> findByMemoDate(Date memoDate);

	List<StockOnMemoMaster> findByBrokerIdOrderByMemoDateDesc(long brokerId);

}
