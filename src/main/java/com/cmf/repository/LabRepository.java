package com.cmf.repository;

import java.util.List;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import com.cmf.dto.Lab;

@Repository
public interface LabRepository extends CrudRepository<Lab, String>
{

	/**
	 * Find by branch id and status.
	 *
	 * @param sessionBranchId the session branch id
	 * @param active the active
	 * @return the list
	 */
	List<Lab> findByIdAndStatus(long id, int active);

	List<Lab> findByBranchIdAndStatus(long sessionBranchId, int statusActive);

	Lab findById(long id);

	List<Lab> findByBranchIdAndStatusOrderByNameAsc(long sessionBranchId, int i);

}
