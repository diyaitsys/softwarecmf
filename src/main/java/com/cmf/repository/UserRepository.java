package com.cmf.repository;

import java.util.List;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import com.cmf.dto.User;

@Repository
public interface UserRepository extends CrudRepository<User, String>
{
	/**
	 * Find user by email address.
	 *
	 * @param emailAddress the email address
	 * @return the user
	 */
	User findUserByEmailAddress(String emailAddress);

	/**
	 * @param sessionBranchId
	 * @param statusActive
	 * @return
	 */
	List<User> findByBranchIdAndStatusOrderByFirstNameAsc(long sessionBranchId, int statusActive);

	User findById(long id);

	User findByEmailAddressAndEntityId(String emailAddress, long id);

}
