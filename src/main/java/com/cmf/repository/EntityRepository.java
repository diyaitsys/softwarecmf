/**
 * 
 */
package com.cmf.repository;

import org.springframework.data.repository.CrudRepository;
import com.cmf.dto.EntityMaster;

/**
 * @author manthan.upadhyay
 */
public interface EntityRepository extends CrudRepository<EntityMaster, String>
{

	/**
	 * Find by code.
	 *
	 * @param code the code
	 * @return the entity
	 */
	EntityMaster findByCode(String code);
}
