/**
 * 
 */
package com.cmf.repository;

import java.util.List;
import org.springframework.data.repository.CrudRepository;
import com.cmf.dto.Party;

/**
 * @author manthan.upadhyay
 */
public interface PartyRepository extends CrudRepository<Party, String>
{

	/**
	 * Find by branch id and status.
	 *
	 * @param sessionBranchId the session branch id
	 * @param active the active
	 * @return the list
	 */
	List<Party> findByBranchIdAndStatusOrderByNameAsc(long sessionBranchId, int active);

	Party findById(Long valueOf);

}
