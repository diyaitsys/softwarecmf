package com.cmf.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import com.cmf.dto.StockOnManufacture;

@Repository
public interface StockOnManufactureRepository extends CrudRepository<StockOnManufacture, String>
{

	/**
	 * Find by branch id and status.
	 *
	 * @param sessionBranchId the session branch id
	 * @param active the active
	 * @return the list
	 */

	StockOnManufacture findByBranchId(long sessionBranchId);

	StockOnManufacture findById(long id);

	StockOnManufacture findByStockId(long id);

	long deleteByStockId(long id);

}
