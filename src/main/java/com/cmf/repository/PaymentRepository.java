package com.cmf.repository;

import java.util.List;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import com.cmf.dto.Payment;

@Repository
public interface PaymentRepository extends CrudRepository<Payment, String>
{
	Payment findById(long id);

	List<Payment> findByPurchaseIdAndBranchId(long id, long sessionBranchId);

	void deleteById(long id);

	List<Payment> findByPurchaseIdAndBranchIdAndPaymentFor(long id, long sessionBranchId, String string);

}
