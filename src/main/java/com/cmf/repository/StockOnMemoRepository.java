package com.cmf.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import com.cmf.dto.StockOnMemo;

@Repository
public interface StockOnMemoRepository extends CrudRepository<StockOnMemo, String>
{

	/**
	 * Find by branch id and status.
	 *
	 * @param sessionBranchId the session branch id
	 * @param active the active
	 * @return the list
	 */

	StockOnMemo findByBranchId(long sessionBranchId);

	StockOnMemo findById(long id);

	StockOnMemo findByStockId(long id);

	long deleteByStockId(long id);

}
