package com.cmf.dvo;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import com.cmf.dto.CreatedInfo;
import com.cmf.dto.Receipt;

/**
 * The Class Branch.
 */
public class InvoiceReceiptListDVO extends CreatedInfo implements Serializable
{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	private long id;
	private String invoiceNo;
	private String partyName;
	private Date saleDate;
	private double billAmountMyCurrency;
	private Date dueDate;
	private String myCurrencyUnit;
	private double paidAmount;
	private double remainingAmount;
	private List<Receipt> receiptHistroyList;

	public long getId()
	{
		return id;
	}

	public void setId(long id)
	{
		this.id = id;
	}

	public String getInvoiceNo()
	{
		return invoiceNo;
	}

	public void setInvoiceNo(String invoiceNo)
	{
		this.invoiceNo = invoiceNo;
	}

	public String getPartyName()
	{
		return partyName;
	}

	public void setPartyName(String partyName)
	{
		this.partyName = partyName;
	}

	public Date getSaleDate()
	{
		return saleDate;
	}

	public void setSaleDate(Date saleDate)
	{
		this.saleDate = saleDate;
	}

	public double getBillAmountMyCurrency()
	{
		return billAmountMyCurrency;
	}

	public void setBillAmountMyCurrency(double billAmountMyCurrency)
	{
		this.billAmountMyCurrency = billAmountMyCurrency;
	}

	public Date getDueDate()
	{
		return dueDate;
	}

	public void setDueDate(Date dueDate)
	{
		this.dueDate = dueDate;
	}

	public String getMyCurrencyUnit()
	{
		return myCurrencyUnit;
	}

	public void setMyCurrencyUnit(String myCurrencyUnit)
	{
		this.myCurrencyUnit = myCurrencyUnit;
	}

	public double getPaidAmount()
	{
		return paidAmount;
	}

	public void setPaidAmount(double paidAmount)
	{
		this.paidAmount = paidAmount;
	}

	public double getRemainingAmount()
	{
		return remainingAmount;
	}

	public void setRemainingAmount(double remainingAmount)
	{
		this.remainingAmount = remainingAmount;
	}

	public List<Receipt> getReceiptHistroyList()
	{
		return receiptHistroyList;
	}

	public void setReceiptHistroyList(List<Receipt> receiptHistroyList)
	{
		this.receiptHistroyList = receiptHistroyList;
	}

}
