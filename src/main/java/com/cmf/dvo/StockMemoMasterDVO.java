package com.cmf.dvo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import com.cmf.dto.CreatedInfo;
import com.cmf.dto.StockOnMemo;

/**
 * The Class Branch.
 */
public class StockMemoMasterDVO extends CreatedInfo implements Serializable
{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	private long id;
	private ArrayList<StockOnMemo> stockOnMemoList;
	private String partyName;
	private Date memoDate;
	private Date dueDate;
	private int termsDays;

	public Date getDueDate()
	{
		return dueDate;
	}

	public void setDueDate(Date dueDate)
	{
		this.dueDate = dueDate;
	}

	public int getTermsDays()
	{
		return termsDays;
	}

	public void setTermsDays(int termsDays)
	{
		this.termsDays = termsDays;
	}

	public long getId()
	{
		return id;
	}

	public void setId(long id)
	{
		this.id = id;
	}

	public ArrayList<StockOnMemo> getStockOnMemoList()
	{
		return stockOnMemoList;
	}

	public void setStockOnMemoList(ArrayList<StockOnMemo> stockOnMemoList)
	{
		this.stockOnMemoList = stockOnMemoList;
	}

	public String getPartyName()
	{
		return partyName;
	}

	public void setPartyName(String partyName)
	{
		this.partyName = partyName;
	}

	public Date getMemoDate()
	{
		return memoDate;
	}

	public void setMemoDate(Date memoDate)
	{
		this.memoDate = memoDate;
	}
}
