package com.cmf.dvo;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import com.cmf.dto.CreatedInfo;
import com.cmf.dto.Payment;

/**
 * The Class Branch.
 */
public class InvoiceListDVO extends CreatedInfo implements Serializable
{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	private long id;
	private String invoiceNo;
	private String partyName;
	private Date purchaseDate;
	private Date saleDate;
	private double billAmountMyCurrency;
	private Date dueDate;
	private String myCurrencyUnit;
	private double brokerageAmount;
	private double paidAmount;
	private double remainingAmount;
	private List<Payment> paymentHistroyList;

	/**
	 * @return the saleDate
	 */
	public Date getSaleDate()
	{
		return saleDate;
	}

	/**
	 * @param saleDate the saleDate to set
	 */
	public void setSaleDate(Date saleDate)
	{
		this.saleDate = saleDate;
	}

	public double getBrokerageAmount()
	{
		return brokerageAmount;
	}

	public void setBrokerageAmount(double brokerageAmount)
	{
		this.brokerageAmount = brokerageAmount;
	}

	public double getRemainingAmount()
	{
		return remainingAmount;
	}

	public void setRemainingAmount(double remainingAmount)
	{
		this.remainingAmount = remainingAmount;
	}

	public String getMyCurrencyUnit()
	{
		return myCurrencyUnit;
	}

	public double getPaidAmount()
	{
		return paidAmount;
	}

	public void setPaidAmount(double paidAmount)
	{
		this.paidAmount = paidAmount;
	}

	public List<Payment> getPaymentHistroyList()
	{
		return paymentHistroyList;
	}

	public void setPaymentHistroyList(List<Payment> paymentHistroyList)
	{
		this.paymentHistroyList = paymentHistroyList;
	}

	public void setMyCurrencyUnit(String myCurrencyUnit)
	{
		this.myCurrencyUnit = myCurrencyUnit;
	}

	public long getId()
	{
		return id;
	}

	public void setId(long id)
	{
		this.id = id;
	}

	public String getInvoiceNo()
	{
		return invoiceNo;
	}

	public void setInvoiceNo(String invoiceNo)
	{
		this.invoiceNo = invoiceNo;
	}

	public String getPartyName()
	{
		return partyName;
	}

	public void setPartyName(String partyName)
	{
		this.partyName = partyName;
	}

	public Date getPurchaseDate()
	{
		return purchaseDate;
	}

	public void setPurchaseDate(Date purchaseDate)
	{
		this.purchaseDate = purchaseDate;
	}

	public double getBillAmountMyCurrency()
	{
		return billAmountMyCurrency;
	}

	public void setBillAmountMyCurrency(double billAmountMyCurrency)
	{
		this.billAmountMyCurrency = billAmountMyCurrency;
	}

	public Date getDueDate()
	{
		return dueDate;
	}

	public void setDueDate(Date dueDate)
	{
		this.dueDate = dueDate;
	}

}
