package com.cmf.controller;

import java.util.ArrayList;
import java.util.List;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.system.ApplicationPidFileWriter;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.event.EventListener;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import com.cmf.dto.EntityMaster;
import com.cmf.dto.User;
import com.cmf.repository.EntityRepository;
import com.cmf.repository.UserRepository;
import com.cmf.utils.Constants;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.BasicAuth;
import springfox.documentation.service.Contact;
import springfox.documentation.service.SecurityScheme;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * The Class Application.
 */
@Configuration()
@EnableConfigurationProperties
@ComponentScan("com.cmf")
@EnableAutoConfiguration
@ServletComponentScan(value = "com.cmf.filter")
@EnableJpaRepositories("com.cmf.repository")
@EntityScan("com.cmf.dto")
@EnableCaching
@EnableSwagger2
public class Application extends SpringBootServletInitializer
{

	/** The Constant LOGGER. */
	private static final Log LOGGER = LogFactory.getLog(Application.class);

	@Autowired
	EntityRepository entityRepository;

	@Autowired
	UserRepository userRepository;

	@Value("${apply.release:false}")
	private boolean applyDBRelease;

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application)
	{
		return application.sources(Application.class);
	}

	/**
	 * The main method.
	 *
	 * @param args the arguments
	 */
	public static void main(String[] args)
	{
		LOGGER.info(args);
		SpringApplication springApplication = new SpringApplication(Application.class);
		springApplication.addListeners(new ApplicationPidFileWriter());
		springApplication.run(args);
	}

	/**
	 * Property config in dev.
	 *
	 * @return the property sources placeholder configurer
	 */
	@Bean
	public static PropertySourcesPlaceholderConfigurer propertyConfigInDev()
	{
		return new PropertySourcesPlaceholderConfigurer();
	}

	@SuppressWarnings("deprecation")
	@EventListener(ApplicationReadyEvent.class)
	public void init()
	{
		if (!applyDBRelease)
		{
			return;
		}

		EntityMaster entityMaster = entityRepository.findByCode("demo");
		if (entityMaster == null)
		{
			entityMaster = new EntityMaster();
			entityMaster.setCode("demo");
			entityMaster.setSoftwareTitle("Demo Application");
			entityMaster.setName("Demo Application");
			entityMaster.setStatus(Constants.STATUS_ACTIVE);
			entityMaster = entityRepository.save(entityMaster);
		}

		System.out.println("Entity Release Done With ID : " + entityMaster.getId());
		User user = userRepository.findByEmailAddressAndEntityId("demo@gemsaccountsoft.com", entityMaster.getId());
		if (user == null)
		{
			user = new User();
			user.setEntityId(entityMaster.getId());
			user.setFirstName("Demo");
			user.setLastName("User");
			user.setEmployeeRole(Constants.SUPER_ADMIN);
			user.setEmailAddress("demo@gemsaccountsoft.com");
			user.setPassword("12345");
			user.setStatus(Constants.STATUS_ACTIVE);
			user = userRepository.save(user);
		}
		System.out.println("User Release Done With ID : " + user.getId());

	}

	@Bean
	public Docket api()
	{
		List<SecurityScheme> schemeList = new ArrayList<>();
		schemeList.add(new BasicAuth("basicAuth"));

		return new Docket(DocumentationType.SWAGGER_2)
				.apiInfo(getApiInfo())
				.select()
				.apis(RequestHandlerSelectors.basePackage("com.cmf.controller"))
				.paths(PathSelectors.any())
				.build()
				.securitySchemes(schemeList);
	}

	private ApiInfo getApiInfo()
	{
		Contact contact = new Contact("SoftwareCMF", "", "");
		return new ApiInfoBuilder()
				.title("SoftwareCMF API")
				.description("Example Api Definition (SoftwareCMF)")
				.version("1.0.0")
				.license("Apache 2.0")
				.licenseUrl("http://www.apache.org/licenses/LICENSE-2.0")
				.contact(contact)
				.build();
	}
}
