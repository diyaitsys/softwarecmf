package com.cmf.controller;

import java.util.Date;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import com.cmf.dto.Lab;
import com.cmf.repository.LabRepository;
import com.cmf.repository.UserRepository;
import com.cmf.utils.Constants;

@Controller
@RequestMapping("/lab")
public class LabController
{
	@Autowired
	HttpServletRequest httpServletRequest;

	@Autowired
	LabRepository labRepository;

	@Autowired
	UserRepository userRepository;

	private ModelAndView model;

	public static final String MESSAGE = "message";

	public static final String ERROR = "error";

	public static final String LAB_PAGE = "/lab/labPage";

	public static final String LAB_LIST_PAGE = "/lab/labListPage";

	public static final String DASHBOARD_PAGE = "/dashboard/dashboardHomePage";

	@RequestMapping(value = "/labListPage", method = RequestMethod.GET)
	public ModelAndView labListPage()
	{
		model = new ModelAndView(LAB_LIST_PAGE);
		List<Lab> labList = labRepository.findByBranchIdAndStatus(Constants.getSessionBranchId(httpServletRequest), Constants.STATUS_ACTIVE);
		model.addObject("labList", labList);
		return model;
	}

	@RequestMapping(value = "/labPage", method = RequestMethod.GET)
	public ModelAndView labPage()
	{
		model = new ModelAndView(LAB_PAGE);
		model.addObject("actionMode", "add");
		return model;
	}

	@RequestMapping(value = "/labPage/{actionMode}/{id}", method = RequestMethod.GET)
	public ModelAndView viewBranchPage(@PathVariable String id, @PathVariable String actionMode)
	{
		Lab lab = labRepository.findById(Long.valueOf(id));
		model = new ModelAndView(LAB_PAGE);
		model.addObject("lab", lab);
		model.addObject("actionMode", actionMode);
		return model;
	}

	@RequestMapping(value = "/remove/{id}", method = RequestMethod.GET)
	public ModelAndView removeBranch(@PathVariable String id)
	{
		Lab lab = labRepository.findById(Long.valueOf(id));
		lab.setStatus(Constants.STATUS_INACTIVE);
		labRepository.save(lab);
		model = new ModelAndView("redirect:" + LAB_LIST_PAGE);
		return model;
	}

	@RequestMapping(value = "/addUpdateLab", method = RequestMethod.POST)
	public ModelAndView addBranch(String actionMode, @ModelAttribute("lab") Lab lab)
	{
		lab.setBranchId(Constants.getSessionBranchId(httpServletRequest));
		if (actionMode.equalsIgnoreCase("add"))
		{
			lab.setCreatedBy(Constants.getSessionUserId(httpServletRequest));
			lab.setCreatedAt(new Date());
		}
		else
		{
			lab.setModifiedBy(Constants.getSessionUserId(httpServletRequest));
			lab.setModifiedDate(new Date());
		}
		try
		{
			labRepository.save(lab);
			model = new ModelAndView("redirect:" + LAB_LIST_PAGE);
		}
		catch (Exception e)
		{
			model = new ModelAndView(LAB_PAGE);
			model.addObject("lab", lab);
			model.addObject("actionMode", "edit");
			model.addObject(ERROR, e.getMessage());
		}
		return model;
	}

}
