package com.cmf.controller;

import java.util.Date;
import java.util.Iterator;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import com.cmf.dto.Payment;
import com.cmf.dto.PurchaseMaster;
import com.cmf.dvo.InvoiceListDVO;
import com.cmf.repository.BranchRepository;
import com.cmf.repository.BrokerRepository;
import com.cmf.repository.PartyRepository;
import com.cmf.repository.PaymentRepository;
import com.cmf.repository.PurchaseRepository;
import com.cmf.repository.SalesRepository;
import com.cmf.repository.StockHistroyRepository;
import com.cmf.repository.StockRepository;
import com.cmf.utils.Constants;

@Controller
@RequestMapping("/payment")
public class PaymentController
{
	@Autowired
	HttpServletRequest httpServletRequest;

	@Autowired
	PurchaseRepository purchaseRepository;

	@Autowired
	SalesRepository salesRepository;

	@Autowired
	StockRepository stockRepository;

	@Autowired
	PartyRepository partyRepository;

	@Autowired
	StockHistroyRepository stockHistroyRepository;

	@Autowired
	BranchRepository branchRepository;

	@Autowired
	BrokerRepository brokerRepository;

	@Autowired
	PaymentRepository paymentRepository;

	private ModelAndView model;

	public static final String MESSAGE = "message";

	public static final String ERROR = "error";

	public static final String PAYMENT_REPORT_PAGE = "/payment/paymentReportPage";

	public static final String PAYMENT_JSP_PAGE = "/payment/paymentPage";

	public static final String PAYMENT_HISTROY_AJAX_JSP_PAGE = "/payment/purchasePaymentDetailsPage";

	public static final String PENDING_PURCHASE_REPORT_PAGE = "/report/pendingPurchasePayment";

	public static final String BROKER__PURCHASE_PAYMENT_HISTROY_AJAX_JSP_PAGE = "/payment/brokerPurchasePaymentDetailsPage";

	public static final String BROKER__SALE_PAYMENT_HISTROY_AJAX_JSP_PAGE = "/payment/brokerSalePaymentDetailsPage";

	@RequestMapping(value = "/getPaymentDetailsAjax", method = RequestMethod.GET)
	@ResponseBody
	public ModelAndView getPaymentDetailsAjax(String invoiceNo, String actionMode)
	{
		if (actionMode.equalsIgnoreCase("purchase"))
		{
			model = new ModelAndView(PAYMENT_HISTROY_AJAX_JSP_PAGE);
		}
		else
		{
			model = new ModelAndView(BROKER__PURCHASE_PAYMENT_HISTROY_AJAX_JSP_PAGE);
		}
		try
		{
			PurchaseMaster purchaseMaster = purchaseRepository.findByInvoiceNoAndBranchId(invoiceNo, Constants.getSessionBranchId(httpServletRequest));
			InvoiceListDVO invoiceListDVO = new InvoiceListDVO();

			if (actionMode.equalsIgnoreCase("purchase"))
			{
				invoiceListDVO.setPartyName(partyRepository.findById(purchaseMaster.getPartyId()).getName());
			}
			else
			{
				invoiceListDVO.setPartyName(brokerRepository.findById(purchaseMaster.getBrokerId()).getName());
			}
			invoiceListDVO.setPaymentHistroyList(paymentRepository.findByPurchaseIdAndBranchIdAndPaymentFor(purchaseMaster.getId(), Constants.getSessionBranchId(httpServletRequest), actionMode));

			Iterator<Payment> itr = invoiceListDVO.getPaymentHistroyList().iterator();
			double paidAmount = 0;
			while (itr.hasNext())
			{
				paidAmount += itr.next().getPaymentAmount();
			}
			invoiceListDVO.setPaidAmount(paidAmount);
			if (actionMode.equalsIgnoreCase("purchase"))
			{
				invoiceListDVO.setRemainingAmount(purchaseMaster.getBillAmountMyCurrency() - paidAmount);
			}
			else
			{
				invoiceListDVO.setRemainingAmount(purchaseMaster.getBrokerageAmount() - paidAmount);
			}
			model.addObject("invoiceListDVO", invoiceListDVO);
			model.addObject("purchaseMaster", purchaseMaster);
			return model;
		}
		catch (Exception e)
		{
			e.printStackTrace();
			return null;
		}
	}

	@RequestMapping(value = "/submitPurchasePayment", method = RequestMethod.POST)
	public ModelAndView submitPurchasePayment(String actionMode, long purchaseId, @ModelAttribute("payment") Payment payment, BindingResult result, Model model1, final RedirectAttributes redirectAttributes)
	{
		model = new ModelAndView("redirect:" + PENDING_PURCHASE_REPORT_PAGE);
		redirectAttributes.addAttribute(MESSAGE, "Payment of " + payment.getPaymentAmount() + "/- Added to Invoice No : " + purchaseRepository.findById(purchaseId).getInvoiceNo());
		redirectAttributes.addAttribute("actionMode", "pending");
		payment.setBranchId(Constants.getSessionBranchId(httpServletRequest));
		payment.setCreatedAt(new Date());
		payment.setCreatedBy(Constants.getSessionUserId(httpServletRequest));
		payment.setPaymentFor(actionMode);
		payment.setPurchaseId(purchaseId);
		redirectAttributes.addFlashAttribute(actionMode, "pending");
		paymentRepository.save(payment);
		return model;
	}

	@RequestMapping(value = "/deletePayment", method = RequestMethod.GET)
	public ModelAndView deletePayment(long id, String invoiceNo)
	{
		model = new ModelAndView("redirect:" + PAYMENT_JSP_PAGE + "?invoiceNo=" + invoiceNo + "&message=Payment Deleted");
		paymentRepository.delete(paymentRepository.findById(id));
		return model;
	}

	@RequestMapping(value = "/paymentPage", method = RequestMethod.GET)
	public ModelAndView paymentPage(String invoiceNo, String message, String actionMode, String id)
	{
		model = new ModelAndView(PAYMENT_JSP_PAGE);
		model.addObject("invoiceNo", invoiceNo);
		model.addObject("id", id);
		model.addObject("message", message);
		model.addObject("actionMode", actionMode);
		return model;
	}
}
