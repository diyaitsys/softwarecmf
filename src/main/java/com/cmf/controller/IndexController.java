package com.cmf.controller;

import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import com.cmf.dto.EntityMaster;
import com.cmf.dto.User;
import com.cmf.repository.EntityRepository;
import com.cmf.repository.UserRepository;
import com.cmf.utils.Constants;

@Controller
public class IndexController
{
	@Autowired
	HttpServletRequest httpServletRequest;

	@Autowired
	EntityRepository entityRepository;

	@Autowired
	UserRepository userRepository;

	private ModelAndView model;

	public static final String MESSAGE = "message";

	@RequestMapping(value = "/", method = RequestMethod.GET)
	public ModelAndView indexPage()
	{
		return home();
	}

	@RequestMapping(value = "/index", method = RequestMethod.GET)
	public ModelAndView home()
	{
		model = new ModelAndView("/login/entityLogin");
		return model;
	}

	@RequestMapping(value = "/checkEntityCode", method = RequestMethod.POST)
	public ModelAndView next(String entityCode)
	{
		EntityMaster entity = entityRepository.findByCode(entityCode);
		if (entity == null)
		{
			model = new ModelAndView("/login/entityLogin");
			model.addObject(MESSAGE, "Company Code is not matched!!!!");
		}
		else
		{
			model = new ModelAndView("/login/login");
			httpServletRequest.getSession().setAttribute(Constants.ENTITY_SESSION, entity);
			model.addObject("entityObj", entity);
		}

		return model;
	}

	@RequestMapping(value = "/checkUserLogin", method = RequestMethod.POST)
	public ModelAndView checkLogin(@ModelAttribute("user") User user)
	{
		User userObj = userRepository.findByEmailAddressAndEntityId(user.getEmailAddress(), ((EntityMaster) httpServletRequest.getSession().getAttribute(Constants.ENTITY_SESSION)).getId());
		if (userObj == null)
		{
			model = new ModelAndView("/login/login");
			model.addObject(MESSAGE, "Username / Password Not Matched!!!!");
		}
		else
		{
			if (userObj.getPassword().equals(user.getPassword()))
			{
				model = new ModelAndView("redirect:/branch/selectBranch");
				httpServletRequest.getSession().setAttribute(Constants.USER_SESSION, userObj);
				model.addObject(MESSAGE, userObj.getFirstName() + " is in new page !!");
			}
			else
			{
				model = new ModelAndView("/login/login");
				model.addObject(MESSAGE, "Username / Password Not Matched!!!!");
			}
		}
		return model;
	}

}
