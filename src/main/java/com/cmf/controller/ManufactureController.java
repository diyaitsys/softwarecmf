package com.cmf.controller;

import java.util.Date;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import com.cmf.dto.Manufacture;
import com.cmf.repository.ManufactureRepository;
import com.cmf.repository.UserRepository;
import com.cmf.utils.Constants;

@Controller
@RequestMapping("/manufacture")
public class ManufactureController
{
	@Autowired
	HttpServletRequest httpServletRequest;

	@Autowired
	ManufactureRepository manufactureRepository;

	@Autowired
	UserRepository userRepository;

	private ModelAndView model;

	public static final String MESSAGE = "message";

	public static final String ERROR = "error";

	public static final String LAB_PAGE = "/manufacture/manufacturePage";

	public static final String LAB_LIST_PAGE = "/manufacture/manufactureListPage";

	public static final String DASHBOARD_PAGE = "/dashboard/dashboardHomePage";

	@RequestMapping(value = "/manufactureListPage", method = RequestMethod.GET)
	public ModelAndView manufactureListPage()
	{
		model = new ModelAndView(LAB_LIST_PAGE);
		List<Manufacture> manufactureList = manufactureRepository.findByBranchIdAndStatus(Constants.getSessionBranchId(httpServletRequest), Constants.STATUS_ACTIVE);
		model.addObject("manufactureList", manufactureList);
		return model;
	}

	@RequestMapping(value = "/manufacturePage", method = RequestMethod.GET)
	public ModelAndView manufacturePage()
	{
		model = new ModelAndView(LAB_PAGE);
		model.addObject("actionMode", "add");
		return model;
	}

	@RequestMapping(value = "/manufacturePage/{actionMode}/{id}", method = RequestMethod.GET)
	public ModelAndView viewManufacturePage(@PathVariable String id, @PathVariable String actionMode)
	{
		Manufacture manufacture = manufactureRepository.findById(Long.valueOf(id));
		model = new ModelAndView(LAB_PAGE);
		model.addObject("manufacture", manufacture);
		model.addObject("actionMode", actionMode);
		return model;
	}

	@RequestMapping(value = "/remove/{id}", method = RequestMethod.GET)
	public ModelAndView removeManufacture(@PathVariable String id)
	{
		Manufacture manufacture = manufactureRepository.findById(Long.valueOf(id));
		manufacture.setStatus(Constants.STATUS_INACTIVE);
		manufactureRepository.save(manufacture);
		model = new ModelAndView("redirect:" + LAB_LIST_PAGE);
		return model;
	}

	@RequestMapping(value = "/addUpdateManufacture", method = RequestMethod.POST)
	public ModelAndView addManufacture(String actionMode, @ModelAttribute("manufacture") Manufacture manufacture)
	{
		manufacture.setBranchId(Constants.getSessionBranchId(httpServletRequest));
		if (actionMode.equalsIgnoreCase("add"))
		{
			manufacture.setCreatedBy(Constants.getSessionUserId(httpServletRequest));
			manufacture.setCreatedAt(new Date());
		}
		else
		{
			manufacture.setModifiedBy(Constants.getSessionUserId(httpServletRequest));
			manufacture.setModifiedDate(new Date());
		}
		try
		{
			manufactureRepository.save(manufacture);
			model = new ModelAndView("redirect:" + LAB_LIST_PAGE);
		}
		catch (Exception e)
		{
			model = new ModelAndView(LAB_PAGE);
			model.addObject("lab", manufacture);
			model.addObject("actionMode", "edit");
			model.addObject(ERROR, e.getMessage());
		}
		return model;
	}

}
