package com.cmf.controller;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import com.cmf.dto.Branch;
import com.cmf.dto.Broker;
import com.cmf.dto.InvoiceMaster;
import com.cmf.dto.Party;
import com.cmf.dto.PurchaseMaster;
import com.cmf.dto.Stock;
import com.cmf.dto.StoneHistroy;
import com.cmf.dvo.InvoiceListDVO;
import com.cmf.repository.BranchRepository;
import com.cmf.repository.BrokerRepository;
import com.cmf.repository.PartyRepository;
import com.cmf.repository.PurchaseRepository;
import com.cmf.repository.StockHistroyRepository;
import com.cmf.repository.StockRepository;
import com.cmf.utils.Constants;

@Controller
@RequestMapping("/purchase")
public class PurchaseController
{

	public static String STOCK_STATUS_PURCHASE = "purchase";
	@Autowired
	HttpServletRequest httpServletRequest;

	@Autowired
	PurchaseRepository purchaseRepository;

	@Autowired
	StockRepository stockRepository;

	@Autowired
	PartyRepository partyRepository;

	@Autowired
	StockHistroyRepository stockHistroyRepository;

	@Autowired
	BranchRepository branchRepository;

	@Autowired
	BrokerRepository brokerRepository;

	private ModelAndView model;

	public static final String MESSAGE = "message";

	public static final String ERROR = "error";

	public static final String PURCHASE_PAGE = "/purchase/purchasePage";

	public static final String PURCHASE_LIST_PAGE = "/purchase/purchaseListPage";

	public static final String PURCHASE_DETAILS_PAGE = "/purchase/purchaseDetailsPage";

	public static final String EDIT_PURCHASE_DETAILS_PAGE = "/purchase/editPurchaseDetailsPage";

	public static final String getInvoiceList_LIST_PAGE = "/purchase/getInvoiceList";

	@RequestMapping(value = "/remove/{id}", method = RequestMethod.GET)
	public ModelAndView removeParty(@PathVariable String id)
	{
		List<Stock> stockList = stockRepository.findByPurchaseMasterId(Long.valueOf(id));

		for (Stock stock : stockList)
		{
			stockRepository.delete(stock);
		}

		purchaseRepository.delete(purchaseRepository.findById(Long.valueOf(id)));

		model = new ModelAndView("redirect:" + getInvoiceList_LIST_PAGE);
		return model;
	}

	@RequestMapping(value = "/getInvoiceList", method = RequestMethod.GET)
	public ModelAndView getInvoiceList()
	{
		model = new ModelAndView(PURCHASE_LIST_PAGE);
		try
		{
			List<PurchaseMaster> purchaseMasterList = purchaseRepository.findByBranchIdAndStatusOrderByPurchaseDateDesc(Constants.getSessionBranchId(httpServletRequest), 1);
			List<InvoiceListDVO> invoiceListDVOLst = new ArrayList<InvoiceListDVO>();
			InvoiceListDVO obj = null;
			for (PurchaseMaster pm : purchaseMasterList)
			{
				obj = new InvoiceListDVO();
				obj.setBillAmountMyCurrency(pm.getBillAmountMyCurrency());
				obj.setDueDate(pm.getDueDate());
				obj.setId(pm.getId());
				obj.setInvoiceNo(pm.getInvoiceNo());
				obj.setPartyName(partyRepository.findById(pm.getPartyId()).getName());
				obj.setPurchaseDate(pm.getPurchaseDate());
				obj.setMyCurrencyUnit(pm.getMyCurrencyUnit());
				invoiceListDVOLst.add(obj);
			}

			model.addObject("invoiceList", invoiceListDVOLst);
			return model;
		}
		catch (Exception e)
		{
			e.printStackTrace();
			return null;
		}
	}

	@RequestMapping(value = "/checkInvoiceNoExists", method = RequestMethod.GET)
	@ResponseBody
	public String checkInvoiceNoExists(String invoiceNo)
	{
		try
		{
			PurchaseMaster purchaseMaster = purchaseRepository.findByInvoiceNoAndBranchId(invoiceNo, Constants.getSessionBranchId(httpServletRequest));
			if (purchaseMaster == null)
			{
				return "OK";
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		return "ERROR";
	}

	@RequestMapping(value = "/checkLotNoExists", method = RequestMethod.GET)
	@ResponseBody
	public String checkLotNoExists(String lotNo)
	{
		try
		{
			Stock purchaseMaster = stockRepository.findByLotNoAndBranchIdAndStockType(lotNo, Constants.getSessionBranchId(httpServletRequest), STOCK_STATUS_PURCHASE);
			if (purchaseMaster == null)
			{
				return "OK";
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		return "ERROR";
	}

	@RequestMapping(value = "/purchasePage", method = RequestMethod.GET)
	public ModelAndView purchasePage(String message)
	{
		Branch branchName = ((Branch) httpServletRequest.getSession(false).getAttribute(Constants.BRANCH_SESSION));
		String branchCode = branchName.getName().substring(0, 3);
		LinkedHashMap<String, String> currencyMap = new LinkedHashMap<String, String>();
		currencyMap.put("USD", "USD");
		currencyMap.put("INR", "INR");
		currencyMap.put("HKD", "HKD");

		Calendar cal = Calendar.getInstance();

		model = new ModelAndView(PURCHASE_PAGE);
		PurchaseMaster purchaseMaster = new PurchaseMaster();

		List<PurchaseMaster> purLst = purchaseRepository.findByBranchIdAndStatusOrderByPurchaseDateDesc(Constants.getSessionBranchId(httpServletRequest), 1);
		if (purLst == null || purLst.size() <= 0)
		{
			purchaseMaster.setInvoiceNo("Inv/" + branchCode + "/1/" + cal.get(Calendar.YEAR));
		}
		else
		{
			long lastId = purLst.size();
			purchaseMaster.setInvoiceNo("Inv/" + branchCode + "/" + (lastId + 1) + "/" + cal.get(Calendar.YEAR));
		}
		List<Party> partyList = partyRepository.findByBranchIdAndStatusOrderByNameAsc(Constants.getSessionBranchId(httpServletRequest), 1);
		List<Broker> brokerList = brokerRepository.findByBranchIdAndStatusOrderByNameAsc(Constants.getSessionBranchId(httpServletRequest), 1);
		model.addObject("partyList", partyList);
		model.addObject("brokerList", brokerList);
		model.addObject("currencyMap", currencyMap);
		model.addObject("purchaseMaster", purchaseMaster);

		if (message != null && !message.equals(""))
			model.addObject(MESSAGE, message);

		model.addObject("actionMode", "add");
		return model;
	}

	@RequestMapping(value = "/editPurchaseDetailsPage/{id}", method = RequestMethod.GET)
	public ModelAndView editPurchaseDetailsPage(@PathVariable String id)
	{
		LinkedHashMap<String, String> currencyMap = new LinkedHashMap<String, String>();
		List<Stock> stockList = stockRepository.findByPurchaseMasterId(Long.parseLong(id));
		currencyMap.put("USD", "USD");
		currencyMap.put("INR", "INR");
		currencyMap.put("HKD", "HKD");

		model = new ModelAndView(EDIT_PURCHASE_DETAILS_PAGE);
		PurchaseMaster purchaseMaster = new PurchaseMaster();
		purchaseMaster = purchaseRepository.findById(Long.parseLong(id));

		List<Party> partyList = partyRepository.findByBranchIdAndStatusOrderByNameAsc(Constants.getSessionBranchId(httpServletRequest), 1);
		List<Broker> brokerList = brokerRepository.findByBranchIdAndStatusOrderByNameAsc(Constants.getSessionBranchId(httpServletRequest), 1);
		model.addObject("partyList", partyList);
		model.addObject("brokerList", brokerList);
		model.addObject("currencyMap", currencyMap);
		model.addObject("purchaseMaster", purchaseMaster);
		model.addObject("stockList", stockList);
		return model;
	}

	@RequestMapping(value = "/purchaseDetailsPage", method = RequestMethod.GET)
	public ModelAndView purchaseDetailsPage(long purchaseId)
	{
		LinkedHashMap<String, String> currencyMap = new LinkedHashMap<String, String>();
		List<Stock> stockList = stockRepository.findByPurchaseMasterId(purchaseId);
		currencyMap.put("USD", "USD");
		currencyMap.put("INR", "INR");
		currencyMap.put("HKD", "HKD");

		model = new ModelAndView(PURCHASE_DETAILS_PAGE);
		PurchaseMaster purchaseMaster = new PurchaseMaster();
		purchaseMaster = purchaseRepository.findById(purchaseId);

		List<Party> partyList = partyRepository.findByBranchIdAndStatusOrderByNameAsc(Constants.getSessionBranchId(httpServletRequest), 1);
		List<Broker> brokerList = brokerRepository.findByBranchIdAndStatusOrderByNameAsc(Constants.getSessionBranchId(httpServletRequest), 1);
		model.addObject("partyList", partyList);
		model.addObject("brokerList", brokerList);
		model.addObject("currencyMap", currencyMap);
		model.addObject("purchaseMaster", purchaseMaster);
		model.addObject("stockList", stockList);
		return model;
	}

	@RequestMapping(value = "/addUpdatePurchase", method = RequestMethod.POST)
	public @ResponseBody Object addUpdatePurchase(String actionMode, @ModelAttribute("invoiceMaster") InvoiceMaster invoiceMaster, BindingResult result, Model model1, final RedirectAttributes redirectAttributes)
	{
		PurchaseMaster purchaseMaster = invoiceMaster.getPurchaseMaster();
		List<Stock> stockLst = invoiceMaster.getStockLst();
		purchaseMaster.setBranchId(Constants.getSessionBranchId(httpServletRequest));
		purchaseMaster.setEntityId(Constants.getSessionEntityId(httpServletRequest));
		if (actionMode.equalsIgnoreCase("add"))
		{
			purchaseMaster.setCreatedBy(Constants.getSessionUserId(httpServletRequest));
			purchaseMaster.setCreatedAt(new Date());
		}
		else
		{
			purchaseMaster.setModifiedBy(Constants.getSessionUserId(httpServletRequest));
			purchaseMaster.setModifiedDate(new Date());
		}
		try
		{
			purchaseMaster = purchaseRepository.save(purchaseMaster);
			Iterator<Stock> itr = stockLst.iterator();
			Stock stock = null;
			while (itr.hasNext())
			{
				stock = itr.next();
				if (stock.getLotNo() == null || stock.getLotNo().equals(""))
				{
					// itr.remove();
					continue;
				}
				stock.setPurchaseMasterId(purchaseMaster.getId());
				stock.setBranchId(Constants.getSessionBranchId(httpServletRequest));
				stock.setEntityId(Constants.getSessionEntityId(httpServletRequest));
				stock.setCurrentBranchId(Constants.getSessionBranchId(httpServletRequest));
				stock.setStockStatus("InHouse");
				stock.setStockType("purchase");
				stock = stockRepository.save(stock);

				StoneHistroy stHistory = new StoneHistroy();
				stHistory.setFromBranchId(Constants.getSessionBranchId(httpServletRequest));
				stHistory.setToBranchId(Constants.getSessionBranchId(httpServletRequest));
				stHistory.setHistroyDate(purchaseMaster.getPurchaseDate());
				stHistory.setStockId(stock.getId());
				stHistory.setCreatedAt(new Date());
				stHistory.setCreatedBy(Constants.getSessionUserId(httpServletRequest));
				stHistory.setRemarks("Purchase From : " + branchRepository.findById(Constants.getSessionBranchId(httpServletRequest)).getName());
				stHistory.setRemarks(stHistory.getRemarks() + ", Shape:" + stock.getShape());
				stHistory.setRemarks(stHistory.getRemarks() + ", Carat:" + stock.getShape());
				stHistory.setRemarks(stHistory.getRemarks() + ", Color:" + stock.getShape());
				stHistory.setRemarks(stHistory.getRemarks() + ", Clarity:" + stock.getShape());
				stHistory.setRemarks(stHistory.getRemarks() + ", Cut:" + stock.getShape());
				stHistory.setRemarks(stHistory.getRemarks() + ", Polish:" + stock.getShape());
				stHistory.setRemarks(stHistory.getRemarks() + ", Symmetry:" + stock.getShape());
				stHistory.setRemarks(stHistory.getRemarks() + ", Fl:" + stock.getShape());
				stHistory.setRemarks(stHistory.getRemarks() + ", Fl Color:" + stock.getShape());
				stHistory.setRemarks(stHistory.getRemarks() + ", Diameter:" + stock.getShape());
				stHistory.setRemarks(stHistory.getRemarks() + ", Certificate Type:" + stock.getShape());
				stHistory.setRemarks(stHistory.getRemarks() + ", Certificate No:" + stock.getShape());
				stHistory.setRemarks(stHistory.getRemarks() + ", Rap/Crt:" + stock.getRapPrice());
				stockHistroyRepository.save(stHistory);
			}

			model = new ModelAndView("redirect:" + PURCHASE_PAGE);
			redirectAttributes.addAttribute(MESSAGE, "Invoice Saved Succusfully");
		}
		catch (Exception e)
		{
			model = new ModelAndView(PURCHASE_PAGE);
			model.addObject("purchaseMaster", purchaseMaster);
			model.addObject("stock", stockLst);
			model.addObject("actionMode", "edit");
			model.addObject(ERROR, e.getMessage());
		}
		return model;
	}

	// BELOW ARE OLD THINGS

	/*
	 * @RequestMapping(value = "/partyListPage", method = RequestMethod.GET)
	 * public ModelAndView partyListPage()
	 * {
	 * model = new ModelAndView(PARTY_LIST_PAGE);
	 * List<Party> partyList = partyRepository.findByBranchIdAndStatus(Constants.getSessionBranchId(httpServletRequest), Constants.STATUS_ACTIVE);
	 * model.addObject("partyList", partyList);
	 * return model;
	 * }
	 * @RequestMapping(value = "/partyPage/{actionMode}/{id}", method = RequestMethod.GET)
	 * public ModelAndView viewPartyPage(@PathVariable String actionMode, @PathVariable String id)
	 * {
	 * Party party = partyRepository.findById(Long.valueOf(id));
	 * model = new ModelAndView(PARTY_PAGE);
	 * model.addObject("party", party);
	 * model.addObject("actionMode", actionMode);
	 * return model;
	 * }
	 * @RequestMapping(value = "/remove/{id}", method = RequestMethod.GET)
	 * public ModelAndView removeParty(@PathVariable String id)
	 * {
	 * Party party = partyRepository.findById(Long.valueOf(id));
	 * party.setStatus(Constants.STATUS_INACTIVE);
	 * partyRepository.save(party);
	 * model = new ModelAndView("redirect:" + PARTY_LIST_PAGE);
	 * return model;
	 * }
	 * @RequestMapping(value = "/addUpdateParty", method = RequestMethod.POST)
	 * public ModelAndView editPartyPage(String actionMode, @ModelAttribute("party") Party party)
	 * {
	 * party.setBranchId(Constants.getSessionBranchId(httpServletRequest));
	 * if (actionMode.equalsIgnoreCase("add"))
	 * {
	 * party.setCreatedBy(Constants.getSessionUserId(httpServletRequest));
	 * party.setCreatedAt(new Date());
	 * }
	 * else
	 * {
	 * party.setModifiedBy(Constants.getSessionUserId(httpServletRequest));
	 * party.setModifiedDate(new Date());
	 * }
	 * try
	 * {
	 * partyRepository.save(party);
	 * model = new ModelAndView("redirect:" + PARTY_LIST_PAGE);
	 * }
	 * catch (Exception e)
	 * {
	 * model = new ModelAndView(PARTY_PAGE);
	 * model.addObject("party", party);
	 * model.addObject("actionMode", "edit");
	 * model.addObject(ERROR, e.getMessage());
	 * }
	 * return model;
	 * }
	 */
}
