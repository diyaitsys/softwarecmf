package com.cmf.controller;

import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import com.cmf.dto.RapNet;
import com.cmf.repository.BranchRepository;
import com.cmf.repository.RapNetRepository;
import com.cmf.repository.UserRepository;
import com.cmf.utils.Constants;
import com.cmf.utils.RapaPortUtil;

@Controller
@RequestMapping("/rapnet")
public class RapNetController
{
	@Autowired
	HttpServletRequest httpServletRequest;

	@Autowired
	BranchRepository branchRepository;

	@Autowired
	UserRepository userRepository;

	@Autowired
	RapNetRepository rapNetRepository;

	private ModelAndView model;

	public static final String MESSAGE = "message";

	public static final String ERROR = "error";

	public static final String RAPNET_PAGE = "/rapnet/rapNetPage";

	@RequestMapping(value = "/getRapPriceOnline", method = RequestMethod.GET)
	@ResponseBody
	public String getRapPriceOnline(float size, String shape, String color, String clarity)
	{
		try
		{
			RapNet rapnet = rapNetRepository.findByEntityId(Constants.getSessionEntityId(httpServletRequest));
			RapaPortUtil rapaPortUtil = new RapaPortUtil();
			String authTicket = rapaPortUtil.login(rapnet.getRapNetUserName(), rapnet.getRapNetpassword());
			double finalResult = rapaPortUtil.getPrice(authTicket, shape, size, color, clarity);
			return String.valueOf(finalResult);
		}
		catch (Exception e)
		{
			e.printStackTrace();
			return "ERROR";
		}
	}

	@RequestMapping(value = "/rapNetPage", method = RequestMethod.GET)
	public ModelAndView rapNetPage()
	{
		model = new ModelAndView(RAPNET_PAGE);
		RapNet rapnet = rapNetRepository.findByEntityId(Constants.getSessionEntityId(httpServletRequest));
		model.addObject("rapnet", rapnet);
		return model;
	}

	@RequestMapping(value = "/rapNetPageInsert", method = RequestMethod.POST)
	public ModelAndView rapNetPageInsert(@ModelAttribute("rapnet") RapNet rapnet)
	{
		model = new ModelAndView(RAPNET_PAGE);
		try
		{
			rapnet.setEntityId(Constants.getSessionEntityId(httpServletRequest));
			rapNetRepository.save(rapnet);
			model.addObject("rapnet", rapnet);
			model.addObject(MESSAGE, "RapNet Username/Password Saved Succusfully");
		}
		catch (Exception e)
		{
			model.addObject(ERROR, "ERROR In RapNet Username/Password");
		}
		return model;
	}
}
