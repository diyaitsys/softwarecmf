package com.cmf.controller;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import com.cmf.dto.Payment;
import com.cmf.dto.PurchaseMaster;
import com.cmf.dto.Receipt;
import com.cmf.dto.SalesMaster;
import com.cmf.dto.StockOnMemo;
import com.cmf.dto.StockOnMemoMaster;
import com.cmf.dvo.InvoiceListDVO;
import com.cmf.dvo.InvoiceReceiptListDVO;
import com.cmf.dvo.StockMemoMasterDVO;
import com.cmf.repository.BrokerRepository;
import com.cmf.repository.PartyRepository;
import com.cmf.repository.PaymentRepository;
import com.cmf.repository.PurchaseRepository;
import com.cmf.repository.ReceiptRepository;
import com.cmf.repository.SalesRepository;
import com.cmf.repository.StockOnMemoMasterRepository;
import com.cmf.repository.StockOnMemoRepository;
import com.cmf.repository.UserRepository;
import com.cmf.utils.Constants;

@Controller
@RequestMapping("/report")
public class ReportController
{
	@Autowired
	HttpServletRequest httpServletRequest;

	@Autowired
	PartyRepository partyRepository;

	@Autowired
	UserRepository userRepository;

	@Autowired
	PurchaseRepository purchaseRepository;

	@Autowired
	SalesRepository salesRepository;

	@Autowired
	PaymentRepository paymentRepository;

	@Autowired
	ReceiptRepository receiptRepository;

	@Autowired
	BrokerRepository brokerRepository;

	@Autowired
	StockOnMemoRepository stockOnMemoRepository;

	private ModelAndView model;

	@Autowired
	StockOnMemoMasterRepository stockOnMemoMasterRepository;

	public static final String MESSAGE = "message";

	public static final String ERROR = "error";

	public static final String PENDING_PURCHASE_REPORT_PAGE = "/report/pendingPurchasePayment";

	public static final String PENDING_SELL_REPORT_PAGE = "/report/pendingSaleReceipt";

	public static final String PENDING_BROKER_REPORT_PAGE = "/report/pendingBrokerPayment";

	public static final String MEMO_PRINT_TABLE_PAGE = "/report/memoPrintTable";

	@RequestMapping(value = "/memoPrintTable", method = RequestMethod.GET)
	public ModelAndView memoPrintTable()
	{
		model = new ModelAndView(MEMO_PRINT_TABLE_PAGE);
		List<StockOnMemoMaster> objLst = stockOnMemoMasterRepository.findByBranchIdOrderByMemoDateDesc(Constants.getSessionBranchId(httpServletRequest));

		StockMemoMasterDVO stockMemoMasterDVO = null;
		ArrayList<StockMemoMasterDVO> stockMemoMasterDVOLst = new ArrayList<StockMemoMasterDVO>();

		for (StockOnMemoMaster obj : objLst)
		{
			stockMemoMasterDVO = new StockMemoMasterDVO();

			stockMemoMasterDVO.setId(obj.getId());
			stockMemoMasterDVO.setMemoDate(obj.getMemoDate());
			stockMemoMasterDVO.setPartyName(brokerRepository.findById(obj.getBrokerId()).getName());
			stockMemoMasterDVO.setStockOnMemoList(new ArrayList<StockOnMemo>());
			stockMemoMasterDVO.setTermsDays(obj.getTermsDays());
			stockMemoMasterDVO.setDueDate(obj.getDueDate());

			List<String> lst = Arrays.asList(obj.getStockOnMemoId().split(","));
			for (String id : lst)
			{
				stockMemoMasterDVO.getStockOnMemoList().add(stockOnMemoRepository.findById(Long.valueOf(id)));
			}
			stockMemoMasterDVOLst.add(stockMemoMasterDVO);
		}

		model.addObject("stockMemoMasterDVOLst", stockMemoMasterDVOLst);
		return model;
	}

	@RequestMapping(value = "/pendingPurchasePayment", method = RequestMethod.GET)
	public ModelAndView pendingPurchasePayment(String actionMode)
	{
		boolean flag;
		double remainingAmount = 0;
		model = new ModelAndView(PENDING_PURCHASE_REPORT_PAGE);
		List<InvoiceListDVO> pendingPayments = new ArrayList<>();
		List<PurchaseMaster> purchaseList = purchaseRepository.findByBranchIdAndStatusOrderByPurchaseDateDesc(Constants.getSessionBranchId(httpServletRequest), 1);

		for (PurchaseMaster purchase : purchaseList)
		{
			flag = false;
			InvoiceListDVO invoiceListDVO = new InvoiceListDVO();
			invoiceListDVO.setPaymentHistroyList(paymentRepository.findByPurchaseIdAndBranchIdAndPaymentFor(purchase.getId(), Constants.getSessionBranchId(httpServletRequest), "purchase"));

			Iterator<Payment> itr = invoiceListDVO.getPaymentHistroyList().iterator();
			double paidAmount = 0;
			while (itr.hasNext())
			{
				paidAmount += itr.next().getPaymentAmount();
			}

			if (actionMode.equalsIgnoreCase("pending"))
			{
				if ((purchase.getBillAmountMyCurrency() - paidAmount) > 0)
				{
					flag = true;
					remainingAmount = purchase.getBillAmountMyCurrency() - paidAmount;
				}
			}
			else
			{
				if ((purchase.getBillAmountMyCurrency() - paidAmount) <= 0)
				{
					flag = true;
					remainingAmount = purchase.getBillAmountMyCurrency() - paidAmount;
				}
			}
			if (flag)
			{
				invoiceListDVO.setPurchaseDate(purchase.getPurchaseDate());
				invoiceListDVO.setId(purchase.getId());
				invoiceListDVO.setInvoiceNo(purchase.getInvoiceNo());
				invoiceListDVO.setDueDate(purchase.getDueDate());
				invoiceListDVO.setPartyName(partyRepository.findById(purchase.getPartyId()).getName());
				invoiceListDVO.setBillAmountMyCurrency(purchase.getBillAmountMyCurrency());
				invoiceListDVO.setPaidAmount(paidAmount);
				invoiceListDVO.setRemainingAmount(remainingAmount);
				invoiceListDVO.setMyCurrencyUnit(purchase.getMyCurrencyUnit());

				pendingPayments.add(invoiceListDVO);
			}
		}

		model.addObject("pendingPayments", pendingPayments);
		model.addObject("actionMode", actionMode);
		return model;
	}

	@RequestMapping(value = "/pendingSaleReceipt", method = RequestMethod.GET)
	public ModelAndView pendingSaleReceipt(String actionMode)
	{
		// chirag
		boolean flag;
		double remainingAmount = 0;
		model = new ModelAndView(PENDING_SELL_REPORT_PAGE);
		List<InvoiceReceiptListDVO> pendingReceipt = new ArrayList<>();
		List<SalesMaster> saleList = salesRepository.findByBranchIdAndStatusOrderBySaleDateDesc(Constants.getSessionBranchId(httpServletRequest), 1);

		for (SalesMaster sale : saleList)
		{
			flag = false;
			InvoiceReceiptListDVO invoiceListDVO = new InvoiceReceiptListDVO();
			invoiceListDVO.setReceiptHistroyList(receiptRepository.findBySaleIdAndBranchId(sale.getId(), Constants.getSessionBranchId(httpServletRequest)));

			Iterator<Receipt> itr = invoiceListDVO.getReceiptHistroyList().iterator();
			double paidAmount = 0;
			while (itr.hasNext())
			{
				paidAmount += itr.next().getReceiptAmount();
			}

			if (actionMode.equalsIgnoreCase("pending"))
			{
				if ((sale.getBillAmountMyCurrency() - paidAmount) > 0)
				{
					flag = true;
					remainingAmount = sale.getBillAmountMyCurrency() - paidAmount;
				}
			}
			else
			{
				if ((sale.getBillAmountMyCurrency() - paidAmount) <= 0)
				{
					flag = true;
					remainingAmount = sale.getBillAmountMyCurrency() - paidAmount;
				}
			}
			if (flag)
			{
				invoiceListDVO.setSaleDate(sale.getSaleDate());
				invoiceListDVO.setId(sale.getId());
				invoiceListDVO.setInvoiceNo(sale.getInvoiceNo());
				invoiceListDVO.setDueDate(sale.getDueDate());
				invoiceListDVO.setPartyName(partyRepository.findById(sale.getPartyId()).getName());
				invoiceListDVO.setBillAmountMyCurrency(sale.getBillAmountMyCurrency());
				invoiceListDVO.setPaidAmount(paidAmount);
				invoiceListDVO.setRemainingAmount(remainingAmount);
				invoiceListDVO.setMyCurrencyUnit(sale.getMyCurrencyUnit());

				pendingReceipt.add(invoiceListDVO);
			}
		}

		model.addObject("pendingReceipt", pendingReceipt);
		model.addObject("actionMode", actionMode);
		return model;
	}

	@RequestMapping(value = "/pendingBrokerPayment", method = RequestMethod.GET)
	public ModelAndView pendingBrokerPayment(String actionMode)
	{
		boolean flag;
		double remainingAmount = 0;
		model = new ModelAndView(PENDING_BROKER_REPORT_PAGE);
		List<InvoiceListDVO> pendingPayments = new ArrayList<>();
		List<PurchaseMaster> purchaseList = purchaseRepository.findByBranchIdAndStatusOrderByPurchaseDateDesc(Constants.getSessionBranchId(httpServletRequest), 1);

		for (PurchaseMaster purchase : purchaseList)
		{
			if (purchase.getBrokerId() == 0)
			{
				continue;
			}
			flag = false;
			InvoiceListDVO invoiceListDVO = new InvoiceListDVO();
			invoiceListDVO.setPaymentHistroyList(paymentRepository.findByPurchaseIdAndBranchIdAndPaymentFor(purchase.getId(), Constants.getSessionBranchId(httpServletRequest), "broker"));

			Iterator<Payment> itr = invoiceListDVO.getPaymentHistroyList().iterator();
			double paidAmount = 0;
			while (itr.hasNext())
			{
				paidAmount += itr.next().getPaymentAmount();
			}

			if (actionMode.equalsIgnoreCase("pending"))
			{
				if ((purchase.getBrokerageAmount() - paidAmount) > 0)
				{
					flag = true;
					remainingAmount = purchase.getBrokerageAmount() - paidAmount;
				}
			}
			else
			{
				if ((purchase.getBrokerageAmount() - paidAmount) <= 0)
				{
					flag = true;
					remainingAmount = purchase.getBrokerageAmount() - paidAmount;
				}
			}
			if (flag)
			{
				invoiceListDVO.setPurchaseDate(purchase.getPurchaseDate());
				invoiceListDVO.setInvoiceNo(purchase.getInvoiceNo());
				invoiceListDVO.setDueDate(purchase.getDueDate());
				invoiceListDVO.setPartyName(brokerRepository.findById(purchase.getBrokerId()).getName());
				invoiceListDVO.setBrokerageAmount(purchase.getBrokerageAmount());
				invoiceListDVO.setBillAmountMyCurrency(purchase.getBillAmountMyCurrency());
				invoiceListDVO.setPaidAmount(paidAmount);
				invoiceListDVO.setRemainingAmount(remainingAmount);
				invoiceListDVO.setMyCurrencyUnit(purchase.getMyCurrencyUnit());

				pendingPayments.add(invoiceListDVO);
			}
		}

		model.addObject("pendingPayments", pendingPayments);
		model.addObject("actionMode", actionMode);
		return model;
	}

}
