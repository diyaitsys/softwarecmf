package com.cmf.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import com.cmf.dto.Broker;
import com.cmf.dto.InvoiceMaster;
import com.cmf.dto.Party;
import com.cmf.dto.SalesMaster;
import com.cmf.dto.Stock;
import com.cmf.dto.StoneHistroy;
import com.cmf.dvo.InvoiceListDVO;
import com.cmf.repository.BranchRepository;
import com.cmf.repository.BrokerRepository;
import com.cmf.repository.PartyRepository;
import com.cmf.repository.PurchaseRepository;
import com.cmf.repository.SalesRepository;
import com.cmf.repository.StockHistroyRepository;
import com.cmf.repository.StockRepository;
import com.cmf.utils.Constants;

@Controller
@RequestMapping("/sales")
public class SalesController
{
	@Autowired
	HttpServletRequest httpServletRequest;

	@Autowired
	PurchaseRepository purchaseRepository;

	@Autowired
	SalesRepository salesRepository;

	@Autowired
	StockRepository stockRepository;

	@Autowired
	PartyRepository partyRepository;

	@Autowired
	StockHistroyRepository stockHistroyRepository;

	@Autowired
	BranchRepository branchRepository;

	@Autowired
	BrokerRepository brokerRepository;

	private ModelAndView model;

	public static final String MESSAGE = "message";

	public static final String ERROR = "error";

	public static final String SALES_PAGE = "/sales/salesPage";

	public static final String SALE_LIST_PAGE = "/sales/saleListPage";

	public static final String SALE_getInvoiceList_LIST_PAGE = "/sales/getInvoiceList";

	public static final String SALE_DETAILS_PAGE = "/sales/saleDetailsPage";

	@RequestMapping(value = "/remove/{id}", method = RequestMethod.GET)
	public ModelAndView removeParty(@PathVariable String id)
	{
		List<Stock> stockList = stockRepository.findByPurchaseMasterId(Long.valueOf(id));

		for (Stock stock : stockList)
		{
			stock.setStockStatus("InHouse");
			stockRepository.save(stock);
		}

		salesRepository.delete(salesRepository.findById(Long.valueOf(id)));

		model = new ModelAndView("redirect:" + SALE_getInvoiceList_LIST_PAGE);
		return model;
	}

	@RequestMapping(value = "/salesPage", method = RequestMethod.GET)
	public ModelAndView purchasePage(String message)
	{
		LinkedHashMap<String, String> currencyMap = new LinkedHashMap<String, String>();
		currencyMap.put("USD", "USD");
		currencyMap.put("INR", "INR");
		currencyMap.put("HKD", "HKD");

		model = new ModelAndView(SALES_PAGE);

		List<Party> partyList = partyRepository.findByBranchIdAndStatusOrderByNameAsc(Constants.getSessionBranchId(httpServletRequest), 1);
		List<Broker> brokerList = brokerRepository.findByBranchIdAndStatusOrderByNameAsc(Constants.getSessionBranchId(httpServletRequest), 1);
		model.addObject("partyList", partyList);
		model.addObject("brokerList", brokerList);
		model.addObject("currencyMap", currencyMap);

		if (message != null && !message.equals(""))
			model.addObject(MESSAGE, message);

		model.addObject("actionMode", "add");
		return model;
	}

	@RequestMapping(value = "/addUpdateSales", method = RequestMethod.POST)
	public @ResponseBody Object addUpdatePurchase(String actionMode, @ModelAttribute("invoiceMaster") InvoiceMaster invoiceMaster, BindingResult result, Model model1, final RedirectAttributes redirectAttributes)
	{
		SalesMaster salesMaster = invoiceMaster.getSalesMaster();
		List<Stock> stockLst = invoiceMaster.getStockLst();
		salesMaster.setBranchId(Constants.getSessionBranchId(httpServletRequest));
		salesMaster.setEntityId(Constants.getSessionEntityId(httpServletRequest));
		if (actionMode.equalsIgnoreCase("add"))
		{
			salesMaster.setCreatedBy(Constants.getSessionUserId(httpServletRequest));
			salesMaster.setCreatedAt(new Date());
		}
		else
		{
			salesMaster.setModifiedBy(Constants.getSessionUserId(httpServletRequest));
			salesMaster.setModifiedDate(new Date());
		}
		try
		{
			salesMaster = salesRepository.save(salesMaster);
			Iterator<Stock> itr = stockLst.iterator();
			Stock stock = null;
			Stock stockOld = null;
			while (itr.hasNext())
			{
				stock = itr.next();
				if (stock.getLotNo() == null || stock.getLotNo().equals(""))
				{
					continue;
				}
				stockOld = stockRepository.findByLotNoAndBranchIdAndStockType(stock.getLotNo(), Constants.getSessionBranchId(httpServletRequest), "purchase");

				stock.setSalesMasterId(salesMaster.getId());
				stock.setBranchId(Constants.getSessionBranchId(httpServletRequest));
				stock.setEntityId(Constants.getSessionEntityId(httpServletRequest));
				stock.setCurrentBranchId(Constants.getSessionBranchId(httpServletRequest));
				stock.setStockStatus("Sold");
				stock.setStockType("sale");
				stock = stockRepository.save(stock);

				stockOld.setStockStatus("Sold");
				stockRepository.save(stockOld);

				StoneHistroy stHistory = new StoneHistroy();
				stHistory.setFromBranchId(Constants.getSessionBranchId(httpServletRequest));
				stHistory.setToBranchId(Constants.getSessionBranchId(httpServletRequest));
				stHistory.setHistroyDate(salesMaster.getSaleDate());
				stHistory.setStockId(stockOld.getId());
				stHistory.setCreatedAt(new Date());
				stHistory.setCreatedBy(Constants.getSessionUserId(httpServletRequest));
				stHistory.setRemarks("Sold From : " + branchRepository.findById(Constants.getSessionBranchId(httpServletRequest)).getName());
				// stHistory.setRemarks(stHistory.getRemarks() + ", Shape:" + stock.getShape());
				// stHistory.setRemarks(stHistory.getRemarks() + ", Carat:" + stock.getShape());
				// stHistory.setRemarks(stHistory.getRemarks() + ", Color:" + stock.getShape());
				// stHistory.setRemarks(stHistory.getRemarks() + ", Clarity:" + stock.getShape());
				// stHistory.setRemarks(stHistory.getRemarks() + ", Cut:" + stock.getShape());
				// stHistory.setRemarks(stHistory.getRemarks() + ", Polish:" + stock.getShape());
				// stHistory.setRemarks(stHistory.getRemarks() + ", Symmetry:" + stock.getShape());
				// stHistory.setRemarks(stHistory.getRemarks() + ", Fl:" + stock.getShape());
				// stHistory.setRemarks(stHistory.getRemarks() + ", Fl Color:" + stock.getShape());
				// stHistory.setRemarks(stHistory.getRemarks() + ", Diameter:" + stock.getShape());
				// stHistory.setRemarks(stHistory.getRemarks() + ", Certificate Type:" + stock.getShape());
				// stHistory.setRemarks(stHistory.getRemarks() + ", Certificate No:" + stock.getShape());
				// stHistory.setRemarks(stHistory.getRemarks() + ", Rap/Crt:" + stock.getRapPrice());
				stockHistroyRepository.save(stHistory);
			}

			model = new ModelAndView("redirect:" + SALES_PAGE);
			redirectAttributes.addAttribute(MESSAGE, "Invoice Saved Succusfully");
		}
		catch (Exception e)
		{
			model = new ModelAndView(SALES_PAGE);
			model.addObject("salesMaster", salesMaster);
			model.addObject("stock", stockLst);
			model.addObject("actionMode", "edit");
			model.addObject(ERROR, e.getMessage());
		}
		return model;
	}

	@RequestMapping(value = "/checkInvoiceNoExists", method = RequestMethod.GET)
	@ResponseBody
	public String checkInvoiceNoExists(String invoiceNo)
	{
		try
		{
			SalesMaster salesMaster = salesRepository.findByInvoiceNoAndBranchId(invoiceNo, Constants.getSessionBranchId(httpServletRequest));
			if (salesMaster == null)
			{
				return "OK";
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		return "ERROR";
	}

	@RequestMapping(value = "/getInvoiceList", method = RequestMethod.GET)
	public ModelAndView getInvoiceList()
	{
		model = new ModelAndView(SALE_LIST_PAGE);
		try
		{
			List<SalesMaster> saleMasterList = salesRepository.findByBranchIdAndStatusOrderBySaleDateDesc(Constants.getSessionBranchId(httpServletRequest), 1);
			List<InvoiceListDVO> invoiceListDVOLst = new ArrayList<InvoiceListDVO>();
			InvoiceListDVO obj = null;
			for (SalesMaster pm : saleMasterList)
			{
				obj = new InvoiceListDVO();
				obj.setBillAmountMyCurrency(pm.getBillAmountMyCurrency());
				obj.setDueDate(pm.getDueDate());
				obj.setId(pm.getId());
				obj.setInvoiceNo(pm.getInvoiceNo());
				obj.setPartyName(partyRepository.findById(pm.getPartyId()).getName());
				obj.setSaleDate(pm.getSaleDate());
				obj.setMyCurrencyUnit(pm.getMyCurrencyUnit());
				invoiceListDVOLst.add(obj);
			}

			model.addObject("invoiceList", invoiceListDVOLst);
			return model;
		}
		catch (Exception e)
		{
			e.printStackTrace();
			return null;
		}
	}

	@RequestMapping(value = "/saleDetailsPage", method = RequestMethod.GET)
	public ModelAndView saleDetailsPage(long saleId)
	{
		LinkedHashMap<String, String> currencyMap = new LinkedHashMap<String, String>();
		List<Stock> stockList = stockRepository.findBySalesMasterId(saleId);
		currencyMap.put("USD", "USD");
		currencyMap.put("INR", "INR");
		currencyMap.put("HKD", "HKD");

		model = new ModelAndView(SALE_DETAILS_PAGE);
		SalesMaster saleMaster = new SalesMaster();
		saleMaster = salesRepository.findById(saleId);

		List<Party> partyList = partyRepository.findByBranchIdAndStatusOrderByNameAsc(Constants.getSessionBranchId(httpServletRequest), 1);
		List<Broker> brokerList = brokerRepository.findByBranchIdAndStatusOrderByNameAsc(Constants.getSessionBranchId(httpServletRequest), 1);
		model.addObject("partyList", partyList);
		model.addObject("brokerList", brokerList);
		model.addObject("currencyMap", currencyMap);
		model.addObject("saleMaster", saleMaster);
		model.addObject("stockList", stockList);
		return model;
	}
}
