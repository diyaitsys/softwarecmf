package com.cmf.controller;

import java.util.Date;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import com.cmf.dto.Branch;
import com.cmf.repository.BranchRepository;
import com.cmf.repository.UserRepository;
import com.cmf.utils.Constants;

@Controller
@RequestMapping("/branch")
public class BranchController
{
	@Autowired
	HttpServletRequest httpServletRequest;

	@Autowired
	BranchRepository branchRepository;

	@Autowired
	UserRepository userRepository;

	private ModelAndView model;

	public static final String MESSAGE = "message";

	public static final String ERROR = "error";

	public static final String BRANCH_PAGE = "/branch/branchPage";

	public static final String BRANCH_LIST_PAGE = "/branch/branchListPage";

	public static final String DASHBOARD_PAGE = "/dashboard/dashboardHomePage";

	@RequestMapping(value = "/branchListPage", method = RequestMethod.GET)
	public ModelAndView branchListPage()
	{
		model = new ModelAndView(BRANCH_LIST_PAGE);
		List<Branch> branchList = branchRepository.findByEntityIdAndStatus(Constants.getSessionEntityId(httpServletRequest), Constants.STATUS_ACTIVE);
		model.addObject("branchList", branchList);
		return model;
	}

	@RequestMapping(value = "/selectBranchSubmit", method = RequestMethod.POST)
	public ModelAndView selectBranchSubmit(String branchId)
	{
		model = new ModelAndView("redirect:" + DASHBOARD_PAGE);
		Branch branch = branchRepository.findById(Long.valueOf(branchId));
		httpServletRequest.getSession().setAttribute(Constants.BRANCH_SESSION, branch);
		return model;
	}

	@RequestMapping(value = "/selectBranch", method = RequestMethod.GET)
	public ModelAndView selectBranch()
	{
		model = new ModelAndView("/branch/selectBranchPage");
		List<Branch> branchList = branchRepository.findByEntityIdAndStatus(Constants.getSessionEntityId(httpServletRequest), Constants.STATUS_ACTIVE);
		model.addObject("branchList", branchList);
		return model;
	}

	@RequestMapping(value = "/branchPage", method = RequestMethod.GET)
	public ModelAndView branchPage()
	{
		model = new ModelAndView(BRANCH_PAGE);
		model.addObject("actionMode", "add");
		return model;
	}

	@RequestMapping(value = "/branchPage/{actionMode}/{id}", method = RequestMethod.GET)
	public ModelAndView viewBranchPage(@PathVariable String id, @PathVariable String actionMode)
	{
		Branch branch = branchRepository.findById(Long.valueOf(id));
		model = new ModelAndView(BRANCH_PAGE);
		model.addObject("branch", branch);
		model.addObject("actionMode", actionMode);
		return model;
	}

	@RequestMapping(value = "/remove/{id}", method = RequestMethod.GET)
	public ModelAndView removeBranch(@PathVariable String id)
	{
		Branch branch = branchRepository.findById(Long.valueOf(id));
		branch.setStatus(Constants.STATUS_INACTIVE);
		branchRepository.save(branch);
		model = new ModelAndView("redirect:" + BRANCH_LIST_PAGE);
		return model;
	}

	@RequestMapping(value = "/addUpdateBranch", method = RequestMethod.POST)
	public ModelAndView addBranch(String actionMode, @ModelAttribute("branch") Branch branch)
	{
		branch.setEntityId(Constants.getSessionEntityId(httpServletRequest));
		if (actionMode.equalsIgnoreCase("add"))
		{
			branch.setCreatedBy(Constants.getSessionUserId(httpServletRequest));
			branch.setCreatedAt(new Date());
		}
		else
		{
			branch.setModifiedBy(Constants.getSessionUserId(httpServletRequest));
			branch.setModifiedDate(new Date());
		}
		try
		{
			branchRepository.save(branch);
			model = new ModelAndView("redirect:" + BRANCH_LIST_PAGE);
		}
		catch (Exception e)
		{
			model = new ModelAndView(BRANCH_PAGE);
			model.addObject("branch", branch);
			model.addObject("actionMode", "edit");
			model.addObject(ERROR, e.getMessage());
		}
		return model;
	}

}
