package com.cmf.controller;

import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import com.cmf.dto.Stock;
import com.cmf.repository.BranchRepository;
import com.cmf.repository.StockRepository;
import com.cmf.repository.UserRepository;
import com.cmf.utils.Constants;

@Controller
@RequestMapping("/dashboard")
public class DashBoardController
{
	@Autowired
	HttpServletRequest httpServletRequest;

	@Autowired
	BranchRepository branchRepository;

	@Autowired
	UserRepository userRepository;

	@Autowired
	StockRepository stockRepository;

	private ModelAndView model;

	public static final String MESSAGE = "message";

	public static final String ERROR = "error";

	public static final String DASHBOARD_PAGE = "/dashboard/dashboard";

	@RequestMapping(value = "/dashboardHomePage", method = RequestMethod.GET)
	public ModelAndView rapNetPage()
	{
		model = new ModelAndView(DASHBOARD_PAGE);
		List<Stock> stockList = stockRepository.findByCurrentBranchIdAndStockStatus(Constants.getSessionBranchId(httpServletRequest),
				"InHouse");
		model.addObject("stockInHouse", stockList.size());

		stockList = stockRepository.findByCurrentBranchIdAndStockStatus(Constants.getSessionBranchId(httpServletRequest),
				"OnMemo");
		model.addObject("stockOnMemo", stockList.size());

		int stockNotReceived = stockRepository.findByCurrentBranchIdAndTransit(Constants.getSessionBranchId(httpServletRequest), 1).size();
		model.addObject("stockNotReceived", stockNotReceived);

		return model;
	}
}
