package com.cmf.controller;

import java.util.Date;
import java.util.Iterator;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import com.cmf.dto.Receipt;
import com.cmf.dto.SalesMaster;
import com.cmf.dvo.InvoiceReceiptListDVO;
import com.cmf.repository.BranchRepository;
import com.cmf.repository.BrokerRepository;
import com.cmf.repository.PartyRepository;
import com.cmf.repository.ReceiptRepository;
import com.cmf.repository.SalesRepository;
import com.cmf.repository.StockHistroyRepository;
import com.cmf.repository.StockRepository;
import com.cmf.utils.Constants;

@Controller
@RequestMapping("/receipt")
public class ReceiptController
{
	@Autowired
	HttpServletRequest httpServletRequest;

	@Autowired
	SalesRepository salesRepository;

	@Autowired
	StockRepository stockRepository;

	@Autowired
	PartyRepository partyRepository;

	@Autowired
	StockHistroyRepository stockHistroyRepository;

	@Autowired
	BranchRepository branchRepository;

	@Autowired
	BrokerRepository brokerRepository;

	@Autowired
	ReceiptRepository receiptRepository;

	private ModelAndView model;

	public static final String MESSAGE = "message";

	public static final String ERROR = "error";

	// public static final String receipt_REPORT_PAGE = "/receipt/receiptReportPage";

	public static final String receipt_JSP_PAGE = "/receipt/receiptPage";

	public static final String receipt_HISTROY_AJAX_JSP_PAGE = "/receipt/saleReceiptDetailsPage";

	public static final String PENDING_receipt_REPORT_PAGE = "/report/pendingSaleReceipt";

	@RequestMapping(value = "/getReceiptDetailsAjax", method = RequestMethod.GET)
	@ResponseBody
	public ModelAndView getReceiptDetailsAjax(String invoiceNo, String actionMode)
	{
		model = new ModelAndView(receipt_HISTROY_AJAX_JSP_PAGE);
		try
		{
			SalesMaster salesMaster = salesRepository.findByInvoiceNoAndBranchId(invoiceNo, Constants.getSessionBranchId(httpServletRequest));
			InvoiceReceiptListDVO invoiceListDVO = new InvoiceReceiptListDVO();

			invoiceListDVO.setPartyName(partyRepository.findById(salesMaster.getPartyId()).getName());
			invoiceListDVO.setReceiptHistroyList(receiptRepository.findBySaleIdAndBranchId(salesMaster.getId(), Constants.getSessionBranchId(httpServletRequest)));

			Iterator<Receipt> itr = invoiceListDVO.getReceiptHistroyList().iterator();
			double paidAmount = 0;
			while (itr.hasNext())
			{
				paidAmount += itr.next().getReceiptAmount();
			}
			invoiceListDVO.setPaidAmount(paidAmount);
			invoiceListDVO.setRemainingAmount(salesMaster.getBillAmountMyCurrency() - paidAmount);
			model.addObject("invoiceListDVO", invoiceListDVO);
			model.addObject("salesMaster", salesMaster);
			return model;
		}
		catch (Exception e)
		{
			e.printStackTrace();
			return null;
		}
	}

	@RequestMapping(value = "/submitSaleReceipt", method = RequestMethod.POST)
	public ModelAndView submitSaleReceipt(String actionMode, long saleId, @ModelAttribute("receipt") Receipt receipt, BindingResult result, Model model1, final RedirectAttributes redirectAttributes)
	{
		model = new ModelAndView("redirect:" + PENDING_receipt_REPORT_PAGE);
		redirectAttributes.addAttribute(MESSAGE, "Receipt of " + receipt.getReceiptAmount() + "/- Added to Invoice No : " + salesRepository.findById(saleId).getInvoiceNo());
		redirectAttributes.addAttribute("actionMode", "pending");
		receipt.setBranchId(Constants.getSessionBranchId(httpServletRequest));
		receipt.setCreatedAt(new Date());
		receipt.setCreatedBy(Constants.getSessionUserId(httpServletRequest));
		receipt.setSaleId(saleId);
		redirectAttributes.addFlashAttribute(actionMode, "pending");
		receiptRepository.save(receipt);
		return model;
	}

	@RequestMapping(value = "/deleteReceipt", method = RequestMethod.GET)
	public ModelAndView deleteReceipt(long id, String invoiceNo)
	{
		model = new ModelAndView("redirect:" + receipt_JSP_PAGE + "?invoiceNo=" + invoiceNo + "&message=Receipt Deleted");
		receiptRepository.delete(receiptRepository.findById(id));
		return model;
	}

	@RequestMapping(value = "/receiptPage", method = RequestMethod.GET)
	public ModelAndView receiptPage(String invoiceNo, String message, String actionMode, String id)
	{
		model = new ModelAndView(receipt_JSP_PAGE);
		model.addObject("invoiceNo", invoiceNo);
		model.addObject("id", id);
		model.addObject("message", message);
		model.addObject("actionMode", actionMode);
		return model;
	}
}
