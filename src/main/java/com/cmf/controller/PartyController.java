package com.cmf.controller;

import java.util.Date;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import com.cmf.dto.Party;
import com.cmf.repository.PartyRepository;
import com.cmf.repository.UserRepository;
import com.cmf.utils.Constants;

@Controller
@RequestMapping("/party")
public class PartyController
{
	@Autowired
	HttpServletRequest httpServletRequest;

	@Autowired
	PartyRepository partyRepository;

	@Autowired
	UserRepository userRepository;

	private ModelAndView model;

	public static final String MESSAGE = "message";

	public static final String ERROR = "error";

	public static final String PARTY_PAGE = "/party/partyPage";

	public static final String PARTY_LIST_PAGE = "/party/partyListPage";

	@RequestMapping(value = "/partyListPage", method = RequestMethod.GET)
	public ModelAndView partyListPage()
	{
		model = new ModelAndView(PARTY_LIST_PAGE);
		List<Party> partyList = partyRepository.findByBranchIdAndStatusOrderByNameAsc(Constants.getSessionBranchId(httpServletRequest), Constants.STATUS_ACTIVE);
		model.addObject("partyList", partyList);
		return model;
	}

	@RequestMapping(value = "/partyPage", method = RequestMethod.GET)
	public ModelAndView partyPage()
	{
		model = new ModelAndView(PARTY_PAGE);
		model.addObject("actionMode", "add");
		return model;
	}

	@RequestMapping(value = "/partyPage/{actionMode}/{id}", method = RequestMethod.GET)
	public ModelAndView viewPartyPage(@PathVariable String actionMode, @PathVariable String id)
	{
		Party party = partyRepository.findById(Long.valueOf(id));
		model = new ModelAndView(PARTY_PAGE);
		model.addObject("party", party);
		model.addObject("actionMode", actionMode);
		return model;
	}

	@RequestMapping(value = "/remove/{id}", method = RequestMethod.GET)
	public ModelAndView removeParty(@PathVariable String id)
	{
		Party party = partyRepository.findById(Long.valueOf(id));
		party.setStatus(Constants.STATUS_INACTIVE);
		partyRepository.save(party);
		model = new ModelAndView("redirect:" + PARTY_LIST_PAGE);
		return model;
	}

	@RequestMapping(value = "/addUpdateParty", method = RequestMethod.POST)
	public ModelAndView editPartyPage(String actionMode, @ModelAttribute("party") Party party)
	{
		party.setBranchId(Constants.getSessionBranchId(httpServletRequest));
		if (actionMode.equalsIgnoreCase("add"))
		{
			party.setCreatedBy(Constants.getSessionUserId(httpServletRequest));
			party.setCreatedAt(new Date());
		}
		else
		{
			party.setModifiedBy(Constants.getSessionUserId(httpServletRequest));
			party.setModifiedDate(new Date());
		}
		try
		{
			partyRepository.save(party);
			model = new ModelAndView("redirect:" + PARTY_LIST_PAGE);
		}
		catch (Exception e)
		{
			model = new ModelAndView(PARTY_PAGE);
			model.addObject("party", party);
			model.addObject("actionMode", "edit");
			model.addObject(ERROR, e.getMessage());
		}
		return model;
	}
}
