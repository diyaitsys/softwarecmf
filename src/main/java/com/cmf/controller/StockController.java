package com.cmf.controller;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import com.cmf.dto.Branch;
import com.cmf.dto.Broker;
import com.cmf.dto.Lab;
import com.cmf.dto.Manufacture;
import com.cmf.dto.PurchaseMaster;
import com.cmf.dto.Stock;
import com.cmf.dto.StockOnLab;
import com.cmf.dto.StockOnManufacture;
import com.cmf.dto.StockOnMemo;
import com.cmf.dto.StockOnMemoMaster;
import com.cmf.dto.StoneHistroy;
import com.cmf.repository.BranchRepository;
import com.cmf.repository.BrokerRepository;
import com.cmf.repository.LabRepository;
import com.cmf.repository.ManufactureRepository;
import com.cmf.repository.PurchaseRepository;
import com.cmf.repository.StockHistroyRepository;
import com.cmf.repository.StockOnLabRepository;
import com.cmf.repository.StockOnManufactureRepository;
import com.cmf.repository.StockOnMemoMasterRepository;
import com.cmf.repository.StockOnMemoRepository;
import com.cmf.repository.StockRepository;
import com.cmf.repository.UserRepository;
import com.cmf.utils.Constants;
import com.google.gson.Gson;

@Controller
@RequestMapping("/stock")
public class StockController
{
	@Autowired
	HttpServletRequest httpServletRequest;

	@Autowired
	BranchRepository branchRepository;

	@Autowired
	UserRepository userRepository;

	@Autowired
	StockRepository stockRepository;

	@Autowired
	PurchaseRepository purchaseRepository;

	@Autowired
	StockOnMemoRepository stockOnMemoRepository;

	@Autowired
	StockOnLabRepository stockOnLabRepository;

	@Autowired
	StockOnManufactureRepository stockOnManufactureRepository;

	@Autowired
	StockHistroyRepository stockHistroyRepository;

	@Autowired
	LabRepository labRepository;

	@Autowired
	ManufactureRepository manufactureRepository;

	@Autowired
	BrokerRepository brokerRepository;

	@Autowired
	StockOnMemoMasterRepository stockOnMemoMasterRepository;

	private ModelAndView model;

	public static final String MESSAGE = "message";

	public static final String ERROR = "error";

	public static final String STOCK_DETAILS_PAGE = "/stock/stockDetailPage";

	public static final String STOCK_LIST_PAGE = "/stock/stockListPage";

	public static final String STOCK_LIST_FOR_MEMO_PAGE = "/stock/stockListForMemoPage";

	public static final String STOCK_LIST_FOR_LAB_PAGE = "/stock/stockListForLabPage";

	public static final String RETURN_FROM_LAB_PAGE = "/stock/returnFromLabPage";

	public static final String STOCK_LIST_FOR_MANUFACTURE_PAGE = "/stock/stockListForManufacturePage";

	public static final String RETURN_FROM_MANUFACTURE_PAGE = "/stock/returnFromManufacturePage";

	public static final String STOCK_LIST_FOR_TRANSFER_PAGE = "/stock/stockListForTransferPage";

	public static final String STOCK_LIST_FOR_RECIVED_PAGE = "/stock/stockListForRecivedPage";

	public static final String STONE_HISTROY_JSP_PAGE = "/stock/stoneHistroyPage";

	@RequestMapping(value = "/stoneHistroyPage", method = RequestMethod.GET)
	public ModelAndView stoneHistroyPage()
	{
		model = new ModelAndView(STONE_HISTROY_JSP_PAGE);
		try
		{
			return model;
		}
		catch (Exception e)
		{
			e.printStackTrace();
			return null;
		}
	}

	@RequestMapping(value = "/stoneHistroyPageDetails", method = RequestMethod.POST)
	public ModelAndView stoneHistroyPageDetails(String lotNo)
	{
		model = new ModelAndView(STONE_HISTROY_JSP_PAGE);
		try
		{
			List<StoneHistroy> stoneHistroyList = stockHistroyRepository.findByStockIdOrderByIdDesc(
					stockRepository.findByLotNoAndEntityIdAndStockType(
							lotNo, Constants.getSessionEntityId(httpServletRequest), "purchase")
							.getId());

			try
			{
				stoneHistroyList.addAll(stockHistroyRepository.findByStockIdOrderByIdDesc(
						stockRepository.findByLotNoAndEntityIdAndStockType(
								lotNo, Constants.getSessionEntityId(httpServletRequest), "sale")
								.getId()));
			}
			catch (Exception e)
			{
			}
			model.addObject("stoneHistroyList", stoneHistroyList);
			return model;
		}
		catch (Exception e)
		{
			e.printStackTrace();
			return null;
		}
	}

	@RequestMapping(value = "/stockListForReceivedPage", method = RequestMethod.GET)
	public ModelAndView stockListForReceivedPage()
	{
		model = new ModelAndView(STOCK_LIST_FOR_RECIVED_PAGE);
		try
		{
			List<Stock> stockList = stockRepository.findByCurrentBranchIdAndTransit(Constants.getSessionBranchId(httpServletRequest), 1);
			model.addObject("stockList", stockList);
			return model;
		}
		catch (Exception e)
		{
			e.printStackTrace();
			return null;
		}
	}

	@RequestMapping(value = "/receivedStock", method = RequestMethod.GET)
	public ModelAndView receivedStock(long id)
	{
		model = new ModelAndView("redirect:" + "/stock/stockListForReceivedPage");
		try
		{
			Stock stock = stockRepository.findById(id);
			stock.setTransit(0);
			stock.setStockStatus("InHouse");
			stockRepository.save(stock);

			StoneHistroy stHistory = new StoneHistroy();
			stHistory.setFromBranchId(Constants.getSessionBranchId(httpServletRequest));
			stHistory.setToBranchId(Constants.getSessionBranchId(httpServletRequest));
			stHistory.setHistroyDate(new Date());
			stHistory.setStockId(id);
			stHistory.setCreatedAt(new Date());
			stHistory.setCreatedBy(Constants.getSessionUserId(httpServletRequest));
			stHistory.setRemarks("Stock Received By : " + branchRepository.findById(Constants.getSessionBranchId(httpServletRequest)).getName());
			stockHistroyRepository.save(stHistory);

			return model;
		}
		catch (Exception e)
		{
			e.printStackTrace();
			return null;
		}
	}

	@RequestMapping(value = "/receivedMultipleStock", method = RequestMethod.POST)
	public ModelAndView receivedMultipleStock(@RequestParam(value = "selectedIds[]") String[] selectedIds, String branchTrnDate, final RedirectAttributes redirectAttributes)
	{
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-mm-dd");

		model = new ModelAndView("redirect:" + "/stock/stockListForReceivedPage");
		for (int i = 0; i < selectedIds.length; i++)
		{
			try
			{
				Stock stock = stockRepository.findById(Long.valueOf(selectedIds[i]));
				stock.setTransit(0);
				stock.setStockStatus("InHouse");
				stockRepository.save(stock);

				StoneHistroy stHistory = new StoneHistroy();
				stHistory.setFromBranchId(Constants.getSessionBranchId(httpServletRequest));
				stHistory.setToBranchId(Constants.getSessionBranchId(httpServletRequest));
				stHistory.setHistroyDate(formatter.parse(branchTrnDate));
				stHistory.setStockId(Long.valueOf(selectedIds[i]));
				stHistory.setCreatedAt(new Date());
				stHistory.setCreatedBy(Constants.getSessionUserId(httpServletRequest));
				stHistory.setRemarks("Stock Received By : " + branchRepository.findById(Constants.getSessionBranchId(httpServletRequest)).getName());
				stockHistroyRepository.save(stHistory);
			}
			catch (Exception e)
			{
				e.printStackTrace();
				// return null;
			}
		}
		return model;
	}

	@RequestMapping(value = "/getStockDetailsByLotNo", method = RequestMethod.GET)
	public ModelAndView getStockDetailsByLotNo(String lotNo)
	{
		model = new ModelAndView(STOCK_DETAILS_PAGE);
		try
		{
			Stock stock = stockRepository.findByLotNoAndBranchIdAndStockType(lotNo, Constants.getSessionBranchId(httpServletRequest), "purchase");
			model.addObject("stock", stock);
			return model;
		}
		catch (Exception e)
		{
			e.printStackTrace();
			return null;
		}
	}

	@RequestMapping(value = "/getStockDetailsById", method = RequestMethod.GET)
	public ModelAndView getStockDetailsById(long id)
	{
		model = new ModelAndView(STOCK_DETAILS_PAGE);
		try
		{
			Stock stock = stockRepository.findByIdAndBranchId(id, Constants.getSessionBranchId(httpServletRequest));

			PurchaseMaster purchaseMaster = purchaseRepository.findById(stock.getPurchaseMasterId());
			model.addObject("actionMode", "view");
			model.addObject("stock", stock);
			model.addObject("purchaseMaster", purchaseMaster);
			return model;
		}
		catch (Exception e)
		{
			e.printStackTrace();
			return null;
		}
	}

	@RequestMapping(value = "/getStockList", method = RequestMethod.GET)
	public ModelAndView getStockList(String status, Model thisModel, RedirectAttributes redirectAttrs)
	{
		model = new ModelAndView(STOCK_LIST_PAGE);
		try
		{
			List<Stock> stockList = stockRepository.findByCurrentBranchIdAndStockStatus(Constants.getSessionBranchId(httpServletRequest),
					status);

			HashMap<Long, String> labMap = new HashMap<Long, String>();

			Iterator<Lab> itr = labRepository.findAll().iterator();
			Lab lab;
			while (itr.hasNext())
			{
				lab = itr.next();
				labMap.put(lab.getId(), lab.getName());
			}

			HashMap<Long, String> manufactureMap = new HashMap<Long, String>();
			Iterator<Manufacture> itr1 = manufactureRepository.findAll().iterator();
			Manufacture manufacture;
			while (itr1.hasNext())
			{
				manufacture = itr1.next();
				manufactureMap.put(manufacture.getId(), manufacture.getName());
			}

			model.addObject("labMap", labMap);
			model.addObject("manufactureMap", manufactureMap);
			model.addObject("stockList", stockList);
			model.addObject("status", status);
			model.addObject(MESSAGE, thisModel.asMap().get(MESSAGE));
			return model;
		}
		catch (Exception e)
		{
			e.printStackTrace();
			return null;
		}
	}

	@RequestMapping(value = "/getStockListForMemo", method = RequestMethod.GET)
	public ModelAndView getStockListForMemo(Model thisModel, RedirectAttributes redirectAttrs)
	{
		model = new ModelAndView(STOCK_LIST_FOR_MEMO_PAGE);
		try
		{
			List<Stock> stockList = stockRepository.findByCurrentBranchIdAndStockStatus(Constants.getSessionBranchId(httpServletRequest),
					"InHouse");
			List<Broker> brokerList = brokerRepository.findByBranchIdAndStatusOrderByNameAsc(Constants.getSessionBranchId(httpServletRequest), 1);
			model.addObject("brokerList", brokerList);
			model.addObject("stockList", stockList);
			model.addObject(MESSAGE, thisModel.asMap().get(MESSAGE));
			return model;
		}
		catch (Exception e)
		{
			e.printStackTrace();
			return null;
		}
	}

	@RequestMapping(value = "/getStockListForLab", method = RequestMethod.GET)
	public ModelAndView getStockListForLab(Model thisModel, RedirectAttributes redirectAttrs)
	{
		model = new ModelAndView(STOCK_LIST_FOR_LAB_PAGE);
		try
		{
			List<Stock> stockList = stockRepository.findByCurrentBranchIdAndStockStatus(Constants.getSessionBranchId(httpServletRequest),
					"InHouse");
			List<Lab> labList = labRepository.findByBranchIdAndStatusOrderByNameAsc(Constants.getSessionBranchId(httpServletRequest), 1);
			model.addObject("labList", labList);
			model.addObject("stockList", stockList);
			model.addObject(MESSAGE, thisModel.asMap().get(MESSAGE));
			return model;
		}
		catch (Exception e)
		{
			e.printStackTrace();
			return null;
		}
	}

	@RequestMapping(value = "/returnMemoById", method = RequestMethod.GET)
	public ModelAndView returnMemoById(long id, final RedirectAttributes redirectAttributes)
	{
		StockOnMemo obj = stockOnMemoRepository.findByStockId(id);
		stockOnMemoRepository.delete(obj);

		Stock stock = stockRepository.findById(id);
		stock.setStockStatus("InHouse");
		stockRepository.save(stock);

		StoneHistroy stHistory = new StoneHistroy();
		stHistory.setFromBranchId(Constants.getSessionBranchId(httpServletRequest));
		stHistory.setToBranchId(Constants.getSessionBranchId(httpServletRequest));
		stHistory.setHistroyDate(new Date());
		stHistory.setStockId(id);
		stHistory.setCreatedAt(new Date());
		stHistory.setCreatedBy(Constants.getSessionUserId(httpServletRequest));
		stHistory.setRemarks("Memo Return From Broker");
		stockHistroyRepository.save(stHistory);

		model = new ModelAndView("redirect:" + "/stock/getStockList?status=OnMemo");
		redirectAttributes.addFlashAttribute(MESSAGE, "Stock Return From Memo To InHouse Succussfully!!!");
		return model;
	}

	@RequestMapping(value = "/returnMemoByMultipleId", method = RequestMethod.POST)
	public ModelAndView returnMemoByMultipleId(@RequestParam(value = "selectedIds[]") String[] selectedIds, String memoRtnDate, final RedirectAttributes redirectAttributes)
	{
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-mm-dd");
		for (int id = 0; id < selectedIds.length; id++)
		{
			StockOnMemo obj = stockOnMemoRepository.findByStockId(Long.parseLong(selectedIds[id]));
			stockOnMemoRepository.delete(obj);

			Stock stock = stockRepository.findById(Long.parseLong(selectedIds[id]));
			stock.setStockStatus("InHouse");
			stockRepository.save(stock);

			StoneHistroy stHistory = new StoneHistroy();
			stHistory.setFromBranchId(Constants.getSessionBranchId(httpServletRequest));
			stHistory.setToBranchId(Constants.getSessionBranchId(httpServletRequest));
			try
			{
				stHistory.setHistroyDate(formatter.parse(memoRtnDate));
			}
			catch (ParseException e)
			{
				e.printStackTrace();
			}
			stHistory.setStockId(Long.parseLong(selectedIds[id]));
			stHistory.setCreatedAt(new Date());
			stHistory.setCreatedBy(Constants.getSessionUserId(httpServletRequest));
			stHistory.setRemarks("Memo Return From Broker");
			stockHistroyRepository.save(stHistory);
		}
		model = new ModelAndView("redirect:" + "/stock/getStockList?status=OnMemo");
		redirectAttributes.addFlashAttribute(MESSAGE, "Stock Return From Memo To InHouse Succussfully!!!");
		return model;
	}

	@RequestMapping(value = "/returnLabByIdStep1", method = RequestMethod.GET)
	public ModelAndView returnLabByIdStep1(int id)
	{
		model = new ModelAndView(RETURN_FROM_LAB_PAGE);
		try
		{
			Stock stock = stockRepository.findByIdAndCurrentBranchIdAndStockStatus((long) id, Constants.getSessionBranchId(httpServletRequest),
					"OnLab");

			model.addObject("stock", stock);
			return model;
		}
		catch (Exception e)
		{
			e.printStackTrace();
			return null;
		}
	}

	@RequestMapping(value = "/returnLabById", method = RequestMethod.POST)
	public ModelAndView returnLabById(@ModelAttribute("stock") Stock stock, Model model1, final RedirectAttributes redirectAttributes)
	{

		Stock stockOriginal = stockRepository.findById(stock.getId());
		String originalCoreDetails = stockOriginal.printStockCoreDetails();
		stockOriginal.setColor(stock.getColor());
		stockOriginal.setCarret(stock.getCarret());
		stockOriginal.setCertificateNo(stock.getCertificateNo());
		stockOriginal.setCertificateType(stock.getCertificateType());
		stockOriginal.setClarity(stock.getClarity());
		stockOriginal.setCut(stock.getCut());
		stockOriginal.setDiameter(stock.getDiameter());
		stockOriginal.setFl(stock.getFl());
		stockOriginal.setFlColor(stock.getFlColor());
		stockOriginal.setPolish(stock.getPolish());
		stockOriginal.setShape(stock.getShape());
		stockOriginal.setSymmetry(stock.getSymmetry());
		long labId = stockOriginal.getLabMasterId();
		stockOriginal.setLabMasterId(0);

		stockOriginal.setStockStatus("InHouse");
		stockRepository.save(stockOriginal);

		// stockOnLabRepository.delete(String.valueOf(stockOnLabRepository.findByStockId(stockOriginal.getId()).getId()));

		StoneHistroy stHistory = new StoneHistroy();
		stHistory.setFromBranchId(Constants.getSessionBranchId(httpServletRequest));
		stHistory.setToBranchId(Constants.getSessionBranchId(httpServletRequest));
		stHistory.setHistroyDate(new Date());
		stHistory.setStockId(stock.getId());
		stHistory.setCreatedAt(new Date());
		stHistory.setCreatedBy(Constants.getSessionUserId(httpServletRequest));
		stHistory.setRemarks("Return From Lab - " + labRepository.findById(labId).getName() + "<br/>Stone Details Before Send To Lab : " + originalCoreDetails + "<br/>Stone Details After Return From Lab : " + stockOriginal.printStockCoreDetails());
		stockHistroyRepository.save(stHistory);

		model = new ModelAndView("redirect:" + "/stock/getStockListForLab");
		redirectAttributes.addFlashAttribute(MESSAGE, "Stock Return From Lab To InHouse Succussfully!!!");
		return model;
	}

	//
	@RequestMapping(value = "/stockToManufacture", method = RequestMethod.POST)
	public ModelAndView stockToManufacture(long manufactureId, @RequestParam(value = "selectedIds[]") String[] selectedIds, String manufactureDate, final RedirectAttributes redirectAttributes)
	{
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-mm-dd");

		StockOnManufacture stockOnManufacture = null;
		Stock stock = null;
		for (int i = 0; i < selectedIds.length; i++)
		{
			stockOnManufacture = new StockOnManufacture();
			stockOnManufacture.setBranchId(Constants.getSessionBranchId(httpServletRequest));
			stockOnManufacture.setManufactureId(manufactureId);
			try
			{
				stockOnManufacture.setManufactureDate(formatter.parse(manufactureDate));
			}
			catch (ParseException e)
			{
				e.printStackTrace();
			}
			stockOnManufacture.setStockId(Long.valueOf(selectedIds[i]));
			stockOnManufactureRepository.save(stockOnManufacture);

			stock = stockRepository.findById(Long.valueOf(selectedIds[i]));
			stock.setStockStatus("OnManufacture");
			stock.setManufactureMasterId(manufactureId);
			stockRepository.save(stock);

			StoneHistroy stHistory = new StoneHistroy();
			stHistory.setFromBranchId(Constants.getSessionBranchId(httpServletRequest));
			stHistory.setToBranchId(Constants.getSessionBranchId(httpServletRequest));
			stHistory.setHistroyDate(new Date());
			stHistory.setStockId(Long.valueOf(selectedIds[i]));
			stHistory.setCreatedAt(new Date());
			stHistory.setCreatedBy(Constants.getSessionUserId(httpServletRequest));
			stHistory.setRemarks("To Manufacture : " + branchRepository.findById(Constants.getSessionBranchId(httpServletRequest)).getName()
					+ manufactureRepository.findById(manufactureId).getName());
			stockHistroyRepository.save(stHistory);

		}
		model = new ModelAndView("redirect:" + "/stock/getStockListForManufacture");
		redirectAttributes.addFlashAttribute(MESSAGE, "Stock Moved To Manufacture Succussfully!!!");
		return model;
	}

	@RequestMapping(value = "/returnManufactureByIdStep1", method = RequestMethod.GET)
	public ModelAndView returnManufactureByIdStep1(int id)
	{
		model = new ModelAndView(RETURN_FROM_MANUFACTURE_PAGE);
		try
		{
			Stock stock = stockRepository.findByIdAndCurrentBranchIdAndStockStatus((long) id, Constants.getSessionBranchId(httpServletRequest),
					"OnManufacture");

			model.addObject("stock", stock);
			return model;
		}
		catch (Exception e)
		{
			e.printStackTrace();
			return null;
		}
	}

	@RequestMapping(value = "/returnManufactureById", method = RequestMethod.POST)
	public ModelAndView returnManufactureById(@ModelAttribute("stock") Stock stock, Model model1, final RedirectAttributes redirectAttributes)
	{
		Stock stockOriginal = stockRepository.findById(stock.getId());
		String originalCoreDetails = stockOriginal.printStockCoreDetails();
		stockOriginal.setColor(stock.getColor());
		stockOriginal.setCarret(stock.getCarret());
		stockOriginal.setCertificateNo(stock.getCertificateNo());
		stockOriginal.setCertificateType(stock.getCertificateType());
		stockOriginal.setClarity(stock.getClarity());
		stockOriginal.setCut(stock.getCut());
		stockOriginal.setDiameter(stock.getDiameter());
		stockOriginal.setFl(stock.getFl());
		stockOriginal.setFlColor(stock.getFlColor());
		stockOriginal.setPolish(stock.getPolish());
		stockOriginal.setShape(stock.getShape());
		stockOriginal.setSymmetry(stock.getSymmetry());
		long manufactureId = stockOriginal.getManufactureMasterId();
		stockOriginal.setManufactureMasterId(0);

		stockOriginal.setStockStatus("InHouse");
		stockRepository.save(stockOriginal);

		// stockOnManufactureRepository.delete(String.valueOf(stockOnManufactureRepository.findByStockId(stockOriginal.getId()).getId()));

		StoneHistroy stHistory = new StoneHistroy();
		stHistory.setFromBranchId(Constants.getSessionBranchId(httpServletRequest));
		stHistory.setToBranchId(Constants.getSessionBranchId(httpServletRequest));
		stHistory.setHistroyDate(new Date());
		stHistory.setStockId(stock.getId());
		stHistory.setCreatedAt(new Date());
		stHistory.setCreatedBy(Constants.getSessionUserId(httpServletRequest));
		stHistory.setRemarks("Return From Manufacture - " + manufactureRepository.findById(manufactureId).getName() + "<br/>Stone Details Before Send To Manufacture : " + originalCoreDetails + "<br/>Stone Details After Return From Manufacture : " + stockOriginal.printStockCoreDetails());
		stockHistroyRepository.save(stHistory);

		model = new ModelAndView("redirect:" + "/stock/getStockListForManufacture");
		redirectAttributes.addFlashAttribute(MESSAGE, "Stock Return From Manufacture To InHouse Succussfully!!!");
		return model;
	}

	@RequestMapping(value = "/getStockListForManufacture", method = RequestMethod.GET)
	public ModelAndView getStockListForManufacture(Model thisModel, RedirectAttributes redirectAttrs)
	{
		model = new ModelAndView(STOCK_LIST_FOR_MANUFACTURE_PAGE);
		try
		{
			List<Stock> stockList = stockRepository.findByCurrentBranchIdAndStockStatus(Constants.getSessionBranchId(httpServletRequest),
					"InHouse");
			List<Manufacture> manufactureList = manufactureRepository.findByBranchIdAndStatusOrderByNameAsc(Constants.getSessionBranchId(httpServletRequest), 1);
			model.addObject("manufactureList", manufactureList);
			model.addObject("stockList", stockList);
			model.addObject(MESSAGE, thisModel.asMap().get(MESSAGE));
			return model;
		}
		catch (Exception e)
		{
			e.printStackTrace();
			return null;
		}
	}
	//

	@RequestMapping(value = "/stockToMemo", method = RequestMethod.POST)
	public ModelAndView stockToMemo(long brokerId, @RequestParam(value = "selectedIds[]") String[] selectedIds, String printNow, String memoDate, String dueDate, int termsDays, final RedirectAttributes redirectAttributes)
	{
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-mm-dd");

		StockOnMemo stockOnMemo = null;
		Stock stock = null;

		StockOnMemoMaster stockOnMemoMaster = new StockOnMemoMaster();
		stockOnMemoMaster.setBranchId(Constants.getSessionBranchId(httpServletRequest));
		stockOnMemoMaster.setBrokerId(brokerId);
		stockOnMemoMaster.setStockOnMemoId(String.join(",", selectedIds));
		stockOnMemoMaster.setTermsDays(termsDays);
		try
		{
			stockOnMemoMaster.setMemoDate(formatter.parse(memoDate));
			stockOnMemoMaster.setDueDate(formatter.parse(dueDate));
		}
		catch (ParseException e1)
		{
			e1.printStackTrace();
		}

		for (int i = 0; i < selectedIds.length; i++)
		{
			stockOnMemo = new StockOnMemo();
			stockOnMemo.setBranchId(Constants.getSessionBranchId(httpServletRequest));
			stockOnMemo.setBrokerId(brokerId);
			try
			{
				stockOnMemo.setMemoDate(formatter.parse(memoDate));
			}
			catch (ParseException e)
			{
				e.printStackTrace();
			}
			stockOnMemo.setStockId(Long.valueOf(selectedIds[i]));
			stockOnMemoRepository.save(stockOnMemo);

			stock = stockRepository.findById(Long.valueOf(selectedIds[i]));
			stock.setStockStatus("OnMemo");
			stockRepository.save(stock);

			StoneHistroy stHistory = new StoneHistroy();
			stHistory.setFromBranchId(Constants.getSessionBranchId(httpServletRequest));
			stHistory.setToBranchId(Constants.getSessionBranchId(httpServletRequest));
			stHistory.setHistroyDate(new Date());
			stHistory.setStockId(Long.valueOf(selectedIds[i]));
			stHistory.setCreatedAt(new Date());
			stHistory.setCreatedBy(Constants.getSessionUserId(httpServletRequest));
			stHistory.setRemarks("To Memo : " + branchRepository.findById(Constants.getSessionBranchId(httpServletRequest)).getName()
					+ " , To Broker : " + brokerRepository.findById(brokerId).getName());
			stockHistroyRepository.save(stHistory);

		}
		stockOnMemoMasterRepository.save(stockOnMemoMaster);
		model = new ModelAndView("redirect:" + "/stock/getStockListForMemo");
		redirectAttributes.addFlashAttribute(MESSAGE, "Stock Moved To Memo Succussfully!!!");
		return model;
	}

	@RequestMapping(value = "/stockToLab", method = RequestMethod.POST)
	public ModelAndView stockToLab(long labId, @RequestParam(value = "selectedIds[]") String[] selectedIds, String labDate, final RedirectAttributes redirectAttributes)
	{
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-mm-dd");

		StockOnLab stockOnLab = null;
		Stock stock = null;
		for (int i = 0; i < selectedIds.length; i++)
		{
			stockOnLab = new StockOnLab();
			stockOnLab.setBranchId(Constants.getSessionBranchId(httpServletRequest));
			stockOnLab.setLabId(labId);
			try
			{
				stockOnLab.setLabDate(formatter.parse(labDate));
			}
			catch (ParseException e)
			{
				e.printStackTrace();
			}
			stockOnLab.setStockId(Long.valueOf(selectedIds[i]));
			stockOnLabRepository.save(stockOnLab);

			stock = stockRepository.findById(Long.valueOf(selectedIds[i]));
			stock.setStockStatus("OnLab");
			stock.setLabMasterId(labId);
			stockRepository.save(stock);

			StoneHistroy stHistory = new StoneHistroy();
			stHistory.setFromBranchId(Constants.getSessionBranchId(httpServletRequest));
			stHistory.setToBranchId(Constants.getSessionBranchId(httpServletRequest));
			stHistory.setHistroyDate(new Date());
			stHistory.setStockId(Long.valueOf(selectedIds[i]));
			stHistory.setCreatedAt(new Date());
			stHistory.setCreatedBy(Constants.getSessionUserId(httpServletRequest));
			stHistory.setRemarks("To Lab : " + branchRepository.findById(Constants.getSessionBranchId(httpServletRequest)).getName()
					+ " , To Lab : " + labRepository.findById(labId).getName());
			stockHistroyRepository.save(stHistory);

		}
		model = new ModelAndView("redirect:" + "/stock/getStockListForLab");
		redirectAttributes.addFlashAttribute(MESSAGE, "Stock Moved To Lab Succussfully!!!");
		return model;
	}

	@RequestMapping(value = "/getStockListForTranfer", method = RequestMethod.GET)
	public ModelAndView getStockListForTranfer(Model thisModel, RedirectAttributes redirectAttrs)
	{
		model = new ModelAndView(STOCK_LIST_FOR_TRANSFER_PAGE);
		try
		{
			List<Stock> stockList = stockRepository.findByCurrentBranchIdAndStockStatus(Constants.getSessionBranchId(httpServletRequest),
					"InHouse");
			List<Branch> branchList = branchRepository.findByEntityIdAndStatus(Constants.getSessionEntityId(httpServletRequest), 1);
			model.addObject("branchList", branchList);
			model.addObject("stockList", stockList);
			model.addObject(MESSAGE, thisModel.asMap().get(MESSAGE));
			return model;
		}
		catch (Exception e)
		{
			e.printStackTrace();
			return null;
		}
	}

	@RequestMapping(value = "/tranferToBranch", method = RequestMethod.POST)
	public ModelAndView tranferToBranch(long branchId, @RequestParam(value = "selectedIds[]") String[] selectedIds, String branchTrnDate, final RedirectAttributes redirectAttributes)
	{
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-mm-dd");
		StoneHistroy stHistory = null;
		Stock stock = null;
		for (int i = 0; i < selectedIds.length; i++)
		{
			stHistory = new StoneHistroy();
			stHistory.setFromBranchId(Constants.getSessionBranchId(httpServletRequest));
			stHistory.setToBranchId(branchId);
			try
			{
				stHistory.setHistroyDate(formatter.parse(branchTrnDate));
			}
			catch (ParseException e)
			{
				e.printStackTrace();
			}
			stHistory.setStockId(Long.valueOf(selectedIds[i]));
			stHistory.setRemarks("Transfer From : " + branchRepository.findById(Constants.getSessionBranchId(httpServletRequest)).getName()
					+ " , To : " + branchRepository.findById(branchId).getName());
			stockHistroyRepository.save(stHistory);

			stock = stockRepository.findById(Long.valueOf(selectedIds[i]));
			stock.setCurrentBranchId(branchId);
			stock.setTransit(1);
			stock.setStockStatus("Transfered");
			stockRepository.save(stock);
		}
		model = new ModelAndView("redirect:" + "/stock/getStockList?status=InHouse");
		redirectAttributes.addFlashAttribute(MESSAGE, "Stock Transfer Succussfully!!!");
		return model;
	}

	@RequestMapping(value = "/getStoneDetailsByLotNoJson", method = RequestMethod.GET)
	@ResponseBody
	public String getStoneDetailsByLotNoJson(String lotNo)
	{
		try
		{
			Stock stock = stockRepository.findByLotNoAndBranchIdAndStockType(lotNo, Constants.getSessionBranchId(httpServletRequest), "purchase");
			if (stock == null || stock.getStockStatus().equalsIgnoreCase("Sold"))
			{
				return "ERROR";
			}
			else
			{
				Map<String, String> data = new HashMap<String, String>();
				data.put("color", stock.getColor());
				data.put("clarity", stock.getClarity());
				data.put("certificateType", stock.getCertificateType());
				data.put("cut", stock.getCut());
				data.put("fl", stock.getFl());
				data.put("flColor", stock.getFlColor());
				data.put("polish", stock.getPolish());
				data.put("shape", stock.getShape());
				data.put("symmetry", stock.getSymmetry());
				data.put("carat", stock.getCarret());
				data.put("diameter", stock.getDiameter());
				data.put("certificateNo", stock.getCertificateNo());

				Gson gson = new Gson();
				return gson.toJson(data);
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		return "ERROR";
	}

}
