package com.cmf.controller;

import java.util.Date;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import com.cmf.dto.Broker;
import com.cmf.dto.PurchaseMaster;
import com.cmf.repository.BrokerRepository;
import com.cmf.repository.UserRepository;
import com.cmf.utils.Constants;

@Controller
@RequestMapping("/broker")
public class BrokerController
{
	@Autowired
	HttpServletRequest httpServletRequest;

	@Autowired
	BrokerRepository brokerRepository;

	@Autowired
	UserRepository userRepository;

	private ModelAndView model;

	public static final String MESSAGE = "message";

	public static final String ERROR = "error";

	public static final String BROKER_PAGE = "/broker/brokerPage";

	public static final String BROKER_LIST_PAGE = "/broker/brokerListPage";

	@RequestMapping(value = "/brokerListPage", method = RequestMethod.GET)
	public ModelAndView brokerListPage()
	{
		model = new ModelAndView(BROKER_LIST_PAGE);
		List<Broker> brokerList = brokerRepository.findByBranchIdAndStatusOrderByNameAsc(Constants.getSessionBranchId(httpServletRequest), Constants.STATUS_ACTIVE);
		model.addObject("brokerList", brokerList);
		return model;
	}

	@RequestMapping(value = "/brokerPage", method = RequestMethod.GET)
	public ModelAndView brokerPage()
	{
		model = new ModelAndView(BROKER_PAGE);
		model.addObject("actionMode", "add");
		return model;
	}

	@RequestMapping(value = "/brokerPage/{actionMode}/{id}", method = RequestMethod.GET)
	public ModelAndView viewBrokerPage(@PathVariable String actionMode, @PathVariable String id)
	{
		Broker broker = brokerRepository.findById(Long.valueOf(id));
		model = new ModelAndView(BROKER_PAGE);
		model.addObject("broker", broker);
		model.addObject("actionMode", actionMode);
		return model;
	}

	@RequestMapping(value = "/remove/{id}", method = RequestMethod.GET)
	public ModelAndView removeBroker(@PathVariable String id)
	{
		Broker broker = brokerRepository.findById(Long.valueOf(id));
		broker.setStatus(Constants.STATUS_INACTIVE);
		brokerRepository.save(broker);
		model = new ModelAndView("redirect:" + BROKER_LIST_PAGE);
		return model;
	}

	@RequestMapping(value = "/addUpdateBroker", method = RequestMethod.POST)
	public ModelAndView addUpdateBroker(String actionMode, @ModelAttribute("broker") Broker broker)
	{
		broker.setBranchId(Constants.getSessionBranchId(httpServletRequest));
		if (actionMode.equalsIgnoreCase("add"))
		{
			broker.setCreatedBy(Constants.getSessionUserId(httpServletRequest));
			broker.setCreatedAt(new Date());
		}
		else
		{
			broker.setModifiedBy(Constants.getSessionUserId(httpServletRequest));
			broker.setModifiedDate(new Date());
		}
		try
		{
			brokerRepository.save(broker);
			model = new ModelAndView("redirect:" + BROKER_LIST_PAGE);
		}
		catch (Exception e)
		{
			model = new ModelAndView(BROKER_PAGE);
			model.addObject("broker", broker);
			model.addObject("actionMode", "edit");
			model.addObject(ERROR, e.getMessage());
		}
		return model;
	}

	@RequestMapping(value = "/addBroker", method = RequestMethod.POST)
	public ModelAndView addBroker(@ModelAttribute("broker") Broker broker)
	{
		broker.setBranchId(Constants.getSessionBranchId(httpServletRequest));
		broker.setCreatedBy(Constants.getSessionUserId(httpServletRequest));
		broker.setCreatedAt(new Date());
		try
		{
			brokerRepository.save(broker);
			brokerListPage();
		}
		catch (Exception e)
		{
			model = new ModelAndView(BROKER_PAGE);
			model.addObject(ERROR, e.getMessage());
		}
		return model;
	}

}
