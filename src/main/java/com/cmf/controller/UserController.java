package com.cmf.controller;

import java.util.Date;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import com.cmf.dto.User;
import com.cmf.repository.UserRepository;
import com.cmf.utils.Constants;

@Controller
@RequestMapping("/user")
public class UserController
{
	@Autowired
	HttpServletRequest httpServletRequest;

	@Autowired
	UserRepository userRepository;

	private ModelAndView model;

	public static final String MESSAGE = "message";

	public static final String ERROR = "error";

	public static final String USER_PAGE = "/user/userPage";

	public static final String USER_LIST_PAGE = "/user/userListPage";

	@RequestMapping(value = "/userListPage", method = RequestMethod.GET)
	public ModelAndView userListPage()
	{
		model = new ModelAndView(USER_LIST_PAGE);
		List<User> userList = userRepository.findByBranchIdAndStatusOrderByFirstNameAsc(Constants.getSessionBranchId(httpServletRequest), Constants.STATUS_ACTIVE);
		model.addObject("userList", userList);
		return model;
	}

	@RequestMapping(value = "/userPage", method = RequestMethod.GET)
	public ModelAndView partyPage()
	{
		model = new ModelAndView(USER_PAGE);
		model.addObject("actionMode", "add");
		return model;
	}

	@RequestMapping(value = "/userPage/{actionMode}/{id}", method = RequestMethod.GET)
	public ModelAndView viewUserPage(@PathVariable String actionMode, @PathVariable String id)
	{
		User user = userRepository.findById(Long.valueOf(id));
		model = new ModelAndView(USER_PAGE);
		model.addObject("user", user);
		model.addObject("actionMode", actionMode);
		return model;
	}

	@RequestMapping(value = "/remove/{id}", method = RequestMethod.GET)
	public ModelAndView removeUser(@PathVariable String id)
	{
		User user = userRepository.findById(Long.valueOf(id));
		user.setStatus(Constants.STATUS_INACTIVE);
		userRepository.save(user);
		model = new ModelAndView("redirect:" + USER_LIST_PAGE);
		return model;
	}

	@RequestMapping(value = "/addUpdateUser", method = RequestMethod.POST)
	public ModelAndView editUserPage(String actionMode, @ModelAttribute("user") User user, BindingResult result, Model model1)
	{
		user.setBranchId(Constants.getSessionBranchId(httpServletRequest));
		user.setEntityId(Constants.getSessionEntityId(httpServletRequest));
		if (actionMode.equalsIgnoreCase("add"))
		{
			user.setPassword("12345");
			user.setCreatedBy(Constants.getSessionUserId(httpServletRequest));
			user.setCreatedAt(new Date());
		}
		else
		{
			user.setPassword(userRepository.findById(user.getId()).getPassword());
			user.setModifiedBy(Constants.getSessionUserId(httpServletRequest));
			user.setModifiedDate(new Date());
		}
		try
		{
			userRepository.save(user);
			model = new ModelAndView("redirect:" + USER_LIST_PAGE);
		}
		catch (Exception e)
		{
			model = new ModelAndView(USER_PAGE);
			model.addObject("user", user);
			model.addObject("actionMode", "edit");
			model.addObject(ERROR, e.getMessage());
		}
		return model;
	}
}
