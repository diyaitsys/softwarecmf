package com.cmf.dto;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import org.springframework.format.annotation.DateTimeFormat;

/**
 * The Class Party.
 */
@Entity
@Table(name = "Party")
public class Party extends CreatedInfo implements Serializable
{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The id. */
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;

	/** The branch id. */
	@NotNull
	private long branchId;

	/** The name. */
	@NotNull
	private String name;

	/** The address. */
	private String address;

	/** The country. */
	private String country;

	/** The mobile number. */
	private String mobileNumber;

	/** The email address. */
	private String emailAddress;

	/** The website. */
	private String website;

	/** The gst number. */
	private String gstNumber;

	/** The pan number. */
	private String panNumber;

	/** The year of joining. */
	@Temporal(TemporalType.DATE)
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date yearOfJoining;

	/** The status. */
	@NotNull
	@Column(name = "status", columnDefinition = "int default 1")
	private int status = 1;

	/** The remarks. */
	private String remarks;

	/** The contant person name. */
	private String contantPersonName;

	/** The bank account. */
	private String bankAccount;

	/** The bank name. */
	private String bankName;

	/** The swift code. */
	private String swiftCode;

	/** The ifcs code. */
	private String ifcsCode;

	public long getId()
	{
		return id;
	}

	public void setId(long id)
	{
		this.id = id;
	}

	public long getBranchId()
	{
		return branchId;
	}

	public void setBranchId(long branchId)
	{
		this.branchId = branchId;
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public String getAddress()
	{
		return address;
	}

	public void setAddress(String address)
	{
		this.address = address;
	}

	public String getCountry()
	{
		return country;
	}

	public void setCountry(String country)
	{
		this.country = country;
	}

	public String getMobileNumber()
	{
		return mobileNumber;
	}

	public void setMobileNumber(String mobileNumber)
	{
		this.mobileNumber = mobileNumber;
	}

	public String getEmailAddress()
	{
		return emailAddress;
	}

	public void setEmailAddress(String emailAddress)
	{
		this.emailAddress = emailAddress;
	}

	public String getWebsite()
	{
		return website;
	}

	public void setWebsite(String website)
	{
		this.website = website;
	}

	public String getGstNumber()
	{
		return gstNumber;
	}

	public void setGstNumber(String gstNumber)
	{
		this.gstNumber = gstNumber;
	}

	public String getPanNumber()
	{
		return panNumber;
	}

	public void setPanNumber(String panNumber)
	{
		this.panNumber = panNumber;
	}

	public Date getYearOfJoining()
	{
		return yearOfJoining;
	}

	public void setYearOfJoining(Date yearOfJoining)
	{
		this.yearOfJoining = yearOfJoining;
	}

	public int getStatus()
	{
		return status;
	}

	public void setStatus(int status)
	{
		this.status = status;
	}

	public String getRemarks()
	{
		return remarks;
	}

	public void setRemarks(String remarks)
	{
		this.remarks = remarks;
	}

	public String getContantPersonName()
	{
		return contantPersonName;
	}

	public void setContantPersonName(String contantPersonName)
	{
		this.contantPersonName = contantPersonName;
	}

	public String getBankAccount()
	{
		return bankAccount;
	}

	public void setBankAccount(String bankAccount)
	{
		this.bankAccount = bankAccount;
	}

	public String getBankName()
	{
		return bankName;
	}

	public void setBankName(String bankName)
	{
		this.bankName = bankName;
	}

	public String getSwiftCode()
	{
		return swiftCode;
	}

	public void setSwiftCode(String swiftCode)
	{
		this.swiftCode = swiftCode;
	}

	public String getIfcsCode()
	{
		return ifcsCode;
	}

	public void setIfcsCode(String ifcsCode)
	{
		this.ifcsCode = ifcsCode;
	}
}
