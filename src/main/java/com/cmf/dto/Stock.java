package com.cmf.dto;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 * The Class Branch.
 */
@Entity
@Table(name = "stock")
public class Stock extends CreatedInfo implements Serializable
{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	/** The id. */
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;

	@Column(name = "purchaseMasterId", columnDefinition = "int default 0")
	private long purchaseMasterId = 0;

	@Column(name = "salesMasterId", columnDefinition = "int default 0")
	private long salesMasterId = 0;

	@Column(name = "labMasterId", columnDefinition = "int default 0")
	private long labMasterId = 0;

	@Column(name = "manufactureMasterId", columnDefinition = "int default 0")
	private long manufactureMasterId = 0;

	@NotNull
	private long branchId;

	@NotNull
	private long entityId;

	@NotNull
	private long currentBranchId;

	@Column(name = "lotNo")
	private String lotNo;

	@Column(name = "diameter")
	private String diameter = "";

	@Column(name = "carret")
	private String carret;

	@Column(name = "shape")
	private String shape;

	@Column(name = "color")
	private String color;

	@Column(name = "clarity")
	private String clarity;

	@Column(name = "cut")
	private String cut = "";

	@Column(name = "polish")
	private String polish = "";

	@Column(name = "symmetry")
	private String symmetry = "";

	@Column(name = "flColor")
	private String flColor = "";

	@Column(name = "fl")
	private String fl = "";

	@Column(name = "certificateType")
	private String certificateType = "";

	@Column(name = "certificateNo")
	private String certificateNo = "";

	@Column(name = "withRap")
	private int withRap;

	@Column(name = "rapPrice")
	private double rapPrice;

	@Column(name = "withPremium")
	private int withPremium = 0;

	@Column(name = "backPrice")
	private double backPrice = 0;

	@Column(name = "addDiscount")
	private double addDiscount = 0;

	@Column(name = "volDiscount")
	private double volDiscount = 0;

	@Column(name = "cashDiscount")
	private double cashDiscount = 0;

	@Column(name = "blindDiscount")
	private double blindDiscount = 0;

	@Column(name = "noBrokerDiscount")
	private double noBrokerDiscount = 0;

	@Column(name = "finalPricePerCrt")
	private double finalPricePerCrt;

	@Column(name = "totalPrice")
	private double totalPrice;

	@Column(name = "transit")
	private int transit = 0;

	@NotNull
	private String stockStatus = "InHouse";

	@NotNull
	private String stockType = "purchase";

	public String getStockType()
	{
		return stockType;
	}

	public void setStockType(String stockType)
	{
		this.stockType = stockType;
	}

	public long getEntityId()
	{
		return entityId;
	}

	public void setEntityId(long entityId)
	{
		this.entityId = entityId;
	}

	public int getTransit()
	{
		return transit;
	}

	public void setTransit(int transit)
	{
		this.transit = transit;
	}

	public long getCurrentBranchId()
	{
		return currentBranchId;
	}

	public void setCurrentBranchId(long currentBranchId)
	{
		this.currentBranchId = currentBranchId;
	}

	public String getStockStatus()
	{
		return stockStatus;
	}

	public void setStockStatus(String stockStatus)
	{
		this.stockStatus = stockStatus;
	}

	public long getBranchId()
	{
		return branchId;
	}

	public void setBranchId(long branchId)
	{
		this.branchId = branchId;
	}

	public double getBlindDiscount()
	{
		return blindDiscount;
	}

	public void setBlindDiscount(double blindDiscount)
	{
		this.blindDiscount = blindDiscount;
	}

	public double getNoBrokerDiscount()
	{
		return noBrokerDiscount;
	}

	public void setNoBrokerDiscount(double noBrokerDiscount)
	{
		this.noBrokerDiscount = noBrokerDiscount;
	}

	public long getId()
	{
		return id;
	}

	public void setId(long id)
	{
		this.id = id;
	}

	public long getPurchaseMasterId()
	{
		return purchaseMasterId;
	}

	public void setPurchaseMasterId(long purchaseMasterId)
	{
		this.purchaseMasterId = purchaseMasterId;
	}

	public String getLotNo()
	{
		return lotNo;
	}

	public void setLotNo(String lotNo)
	{
		this.lotNo = lotNo;
	}

	public String getDiameter()
	{
		return diameter;
	}

	public void setDiameter(String diameter)
	{
		this.diameter = diameter;
	}

	public String getCarret()
	{
		return carret;
	}

	public void setCarret(String carret)
	{
		this.carret = carret;
	}

	public String getShape()
	{
		return shape;
	}

	public void setShape(String shape)
	{
		this.shape = shape;
	}

	public String getColor()
	{
		return color;
	}

	public void setColor(String color)
	{
		this.color = color;
	}

	public String getClarity()
	{
		return clarity;
	}

	public void setClarity(String clarity)
	{
		this.clarity = clarity;
	}

	public String getCut()
	{
		return cut;
	}

	public void setCut(String cut)
	{
		this.cut = cut;
	}

	public String getPolish()
	{
		return polish;
	}

	public void setPolish(String polish)
	{
		this.polish = polish;
	}

	public String getSymmetry()
	{
		return symmetry;
	}

	public void setSymmetry(String symmetry)
	{
		this.symmetry = symmetry;
	}

	public String getFlColor()
	{
		return flColor;
	}

	public void setFlColor(String flColor)
	{
		this.flColor = flColor;
	}

	public String getFl()
	{
		return fl;
	}

	public void setFl(String fl)
	{
		this.fl = fl;
	}

	public String getCertificateType()
	{
		return certificateType;
	}

	public void setCertificateType(String certificateType)
	{
		this.certificateType = certificateType;
	}

	public String getCertificateNo()
	{
		return certificateNo;
	}

	public void setCertificateNo(String certificateNo)
	{
		this.certificateNo = certificateNo;
	}

	public int getWithRap()
	{
		return withRap;
	}

	public void setWithRap(int withRap)
	{
		this.withRap = withRap;
	}

	public double getRapPrice()
	{
		return rapPrice;
	}

	public void setRapPrice(double rapPrice)
	{
		this.rapPrice = rapPrice;
	}

	public int getWithPremium()
	{
		return withPremium;
	}

	public void setWithPremium(int withPremium)
	{
		this.withPremium = withPremium;
	}

	public double getBackPrice()
	{
		return backPrice;
	}

	public void setBackPrice(double backPrice)
	{
		this.backPrice = backPrice;
	}

	public double getAddDiscount()
	{
		return addDiscount;
	}

	public void setAddDiscount(double addDiscount)
	{
		this.addDiscount = addDiscount;
	}

	public double getVolDiscount()
	{
		return volDiscount;
	}

	public void setVolDiscount(double volDiscount)
	{
		this.volDiscount = volDiscount;
	}

	public double getCashDiscount()
	{
		return cashDiscount;
	}

	public void setCashDiscount(double cashDiscount)
	{
		this.cashDiscount = cashDiscount;
	}

	public double getFinalPricePerCrt()
	{
		return finalPricePerCrt;
	}

	public void setFinalPricePerCrt(double finalPricePerCrt)
	{
		this.finalPricePerCrt = finalPricePerCrt;
	}

	public double getTotalPrice()
	{
		return totalPrice;
	}

	public void setTotalPrice(double totalPrice)
	{
		this.totalPrice = totalPrice;
	}

	public long getSalesMasterId()
	{
		return salesMasterId;
	}

	public void setSalesMasterId(long salesMasterId)
	{
		this.salesMasterId = salesMasterId;
	}

	public long getLabMasterId()
	{
		return labMasterId;
	}

	public void setLabMasterId(long labMasterId)
	{
		this.labMasterId = labMasterId;
	}

	public long getManufactureMasterId()
	{
		return manufactureMasterId;
	}

	public void setManufactureMasterId(long manufactureMasterId)
	{
		this.manufactureMasterId = manufactureMasterId;
	}

	public String printStockCoreDetails()
	{
		return "[lotNo=" + lotNo + ", diameter=" + diameter + ", carat=" + carret + ", shape=" + shape + ", color=" + color + ", clarity=" + clarity + ", cut=" + cut + ", polish=" + polish + ", symmetry=" + symmetry + ", flColor=" + flColor + ", fl=" + fl + ", certificateType=" + certificateType + ", certificateNo=" + certificateNo + "]";
	}

}
