/*
 * 
 */
package com.cmf.dto;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import org.springframework.format.annotation.DateTimeFormat;

/**
 * The Class Broker.
 */
@Entity
@Table(name = "receipt")
public class Receipt extends CreatedInfo implements Serializable
{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The id. */
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;

	/** The branch id. */
	@NotNull
	private long branchId;

	@NotNull
	private long saleId = 0;

	@NotNull
	@Temporal(TemporalType.DATE)
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date receiptDate;

	private String receiptMode;

	private double receiptAmount;

	private String remarks;

	public long getId()
	{
		return id;
	}

	public void setId(long id)
	{
		this.id = id;
	}

	public long getBranchId()
	{
		return branchId;
	}

	public void setBranchId(long branchId)
	{
		this.branchId = branchId;
	}

	public long getSaleId()
	{
		return saleId;
	}

	public void setSaleId(long saleId)
	{
		this.saleId = saleId;
	}

	public Date getReceiptDate()
	{
		return receiptDate;
	}

	public void setReceiptDate(Date receiptDate)
	{
		this.receiptDate = receiptDate;
	}

	public String getReceiptMode()
	{
		return receiptMode;
	}

	public void setReceiptMode(String receiptMode)
	{
		this.receiptMode = receiptMode;
	}

	public double getReceiptAmount()
	{
		return receiptAmount;
	}

	public void setReceiptAmount(double receiptAmount)
	{
		this.receiptAmount = receiptAmount;
	}

	public String getRemarks()
	{
		return remarks;
	}

	public void setRemarks(String remarks)
	{
		this.remarks = remarks;
	}

}
