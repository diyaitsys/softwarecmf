/*
 * 
 */
package com.cmf.dto;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 * The Class Lab.
 */
@Entity
@Table(name = "manufacture")
public class Manufacture extends CreatedInfo implements Serializable
{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The id. */
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;

	/** The branch id. */
	@NotNull
	private long branchId;

	/** The name. */
	@NotNull
	private String name;

	/** The address. */
	private String address;

	/** The country. */
	private String country;

	/** The mobile number. */
	private String mobileNumber;

	/** The email address. */
	private String emailAddress;

	/** The website. */
	private String website;

	/** The gst number. */
	private String gstNumber;

	/** The pan number. */
	private String panNumber;

	/** The status. */
	@NotNull
	@Column(name = "status", columnDefinition = "int default 1")
	private int status = 1;

	/** The remarks. */
	private String remarks;

	/** The contant person name. */
	private String contantPersonName;

	/** The bank account. */
	private String bankAccount;

	/** The bank name. */
	private String bankName;

	/** The swift code. */
	private String swiftCode;

	/** The ifcs code. */
	private String ifcsCode;

	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	public String getName()
	{
		return name;
	}

	/**
	 * Sets the name.
	 *
	 * @param name the name to set
	 */
	public void setName(String name)
	{
		this.name = name;
	}

	/**
	 * Gets the address.
	 *
	 * @return the address
	 */
	public String getAddress()
	{
		return address;
	}

	/**
	 * Sets the address.
	 *
	 * @param address the address to set
	 */
	public void setAddress(String address)
	{
		this.address = address;
	}

	/**
	 * Gets the country.
	 *
	 * @return the country
	 */
	public String getCountry()
	{
		return country;
	}

	/**
	 * Sets the country.
	 *
	 * @param country the country to set
	 */
	public void setCountry(String country)
	{
		this.country = country;
	}

	/**
	 * Gets the mobile number.
	 *
	 * @return the mobileNumber
	 */
	public String getMobileNumber()
	{
		return mobileNumber;
	}

	/**
	 * Sets the mobile number.
	 *
	 * @param mobileNumber the mobileNumber to set
	 */
	public void setMobileNumber(String mobileNumber)
	{
		this.mobileNumber = mobileNumber;
	}

	/**
	 * Gets the email address.
	 *
	 * @return the emailAddress
	 */
	public String getEmailAddress()
	{
		return emailAddress;
	}

	/**
	 * Sets the email address.
	 *
	 * @param emailAddress the emailAddress to set
	 */
	public void setEmailAddress(String emailAddress)
	{
		this.emailAddress = emailAddress;
	}

	/**
	 * Gets the website.
	 *
	 * @return the website
	 */
	public String getWebsite()
	{
		return website;
	}

	/**
	 * Sets the website.
	 *
	 * @param website the website to set
	 */
	public void setWebsite(String website)
	{
		this.website = website;
	}

	/**
	 * Gets the gst number.
	 *
	 * @return the gstNumber
	 */
	public String getGstNumber()
	{
		return gstNumber;
	}

	/**
	 * Sets the gst number.
	 *
	 * @param gstNumber the gstNumber to set
	 */
	public void setGstNumber(String gstNumber)
	{
		this.gstNumber = gstNumber;
	}

	/**
	 * Gets the pan number.
	 *
	 * @return the panNumber
	 */
	public String getPanNumber()
	{
		return panNumber;
	}

	/**
	 * Sets the pan number.
	 *
	 * @param panNumber the panNumber to set
	 */
	public void setPanNumber(String panNumber)
	{
		this.panNumber = panNumber;
	}

	/**
	 * Gets the remarks.
	 *
	 * @return the remarks
	 */
	public String getRemarks()
	{
		return remarks;
	}

	/**
	 * Sets the remarks.
	 *
	 * @param remarks the remarks to set
	 */
	public void setRemarks(String remarks)
	{
		this.remarks = remarks;
	}

	/**
	 * Gets the contant person name.
	 *
	 * @return the contantPersonName
	 */
	public String getContantPersonName()
	{
		return contantPersonName;
	}

	/**
	 * Sets the contant person name.
	 *
	 * @param contantPersonName the contantPersonName to set
	 */
	public void setContantPersonName(String contantPersonName)
	{
		this.contantPersonName = contantPersonName;
	}

	/**
	 * Gets the bank account.
	 *
	 * @return the bankAccount
	 */
	public String getBankAccount()
	{
		return bankAccount;
	}

	/**
	 * Sets the bank account.
	 *
	 * @param bankAccount the bankAccount to set
	 */
	public void setBankAccount(String bankAccount)
	{
		this.bankAccount = bankAccount;
	}

	/**
	 * Gets the bank name.
	 *
	 * @return the bankName
	 */
	public String getBankName()
	{
		return bankName;
	}

	/**
	 * Sets the bank name.
	 *
	 * @param bankName the bankName to set
	 */
	public void setBankName(String bankName)
	{
		this.bankName = bankName;
	}

	/**
	 * Gets the swift code.
	 *
	 * @return the swiftCode
	 */
	public String getSwiftCode()
	{
		return swiftCode;
	}

	/**
	 * Sets the swift code.
	 *
	 * @param swiftCode the swiftCode to set
	 */
	public void setSwiftCode(String swiftCode)
	{
		this.swiftCode = swiftCode;
	}

	/**
	 * Gets the ifcs code.
	 *
	 * @return the ifcsCode
	 */
	public String getIfcsCode()
	{
		return ifcsCode;
	}

	/**
	 * Sets the ifcs code.
	 *
	 * @param ifcsCode the ifcsCode to set
	 */
	public void setIfcsCode(String ifcsCode)
	{
		this.ifcsCode = ifcsCode;
	}

	/**
	 * @return the id
	 */
	public long getId()
	{
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(long id)
	{
		this.id = id;
	}

	/**
	 * @return the branchId
	 */
	public long getBranchId()
	{
		return branchId;
	}

	/**
	 * @param branchId the branchId to set
	 */
	public void setBranchId(long branchId)
	{
		this.branchId = branchId;
	}

	/**
	 * @return the status
	 */
	public int getStatus()
	{
		return status;
	}

	/**
	 * @param status the status to set
	 */
	public void setStatus(int status)
	{
		this.status = status;
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString()
	{
		return "Broker [id=" + id + ", branchId=" + branchId + ", name=" + name + ", address=" + address + ", country=" + country + ", mobileNumber=" + mobileNumber + ", emailAddress=" + emailAddress + ", website=" + website + ", gstNumber=" + gstNumber + ", panNumber=" + panNumber + ", status=" + status + ", remarks=" + remarks + ", contantPersonName=" + contantPersonName + ", bankAccount=" + bankAccount + ", bankName=" + bankName + ", swiftCode=" + swiftCode + ", ifcsCode=" + ifcsCode + "]";
	}

}
