package com.cmf.dto;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 * The Class Branch.
 */
@Entity
@Table(name = "RapNet")
public class RapNet extends CreatedInfo implements Serializable
{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The id. */
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;

	/** The entity id. */
	@NotNull
	private long entityId;

	private String rapNetUserName;

	private String rapNetpassword;

	public long getId()
	{
		return id;
	}

	public void setId(long id)
	{
		this.id = id;
	}

	public long getEntityId()
	{
		return entityId;
	}

	public void setEntityId(long entityId)
	{
		this.entityId = entityId;
	}

	public String getRapNetUserName()
	{
		return rapNetUserName;
	}

	public void setRapNetUserName(String rapNetUserName)
	{
		this.rapNetUserName = rapNetUserName;
	}

	public String getRapNetpassword()
	{
		return rapNetpassword;
	}

	public void setRapNetpassword(String rapNetpassword)
	{
		this.rapNetpassword = rapNetpassword;
	}

}
