package com.cmf.dto;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "Entity")
public class EntityMaster extends CreatedInfo implements Serializable
{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The id. */
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;

	/** The name. */
	@NotNull
	private String name;

	/** The code. */
	@NotNull
	private String code;

	/** The address. */
	private String address;

	/** The software title. */
	@NotNull
	private String softwareTitle;

	/** The contact person 1. */
	private String contactPerson1;

	/** The mobile number 1. */
	private String mobileNumber1;

	/** The email address 1. */
	private String emailAddress1;

	/** The contact person 2. */
	private String contactPerson2;

	/** The mobile number 2. */
	private String mobileNumber2;

	/** The email address 2. */
	private String emailAddress2;

	/** The status. */
	@NotNull
	@Column(name = "status", columnDefinition = "int default 1")
	private int status = 1;

	/** The logo. */
	private String logo;

	/** The announcement. */
	private String announcement;

	/** The valid upto. */
	private Date validUpto;

	public long getId()
	{
		return id;
	}

	public void setId(long id)
	{
		this.id = id;
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public String getCode()
	{
		return code;
	}

	public void setCode(String code)
	{
		this.code = code;
	}

	public String getAddress()
	{
		return address;
	}

	public void setAddress(String address)
	{
		this.address = address;
	}

	public String getSoftwareTitle()
	{
		return softwareTitle;
	}

	public void setSoftwareTitle(String softwareTitle)
	{
		this.softwareTitle = softwareTitle;
	}

	public String getContactPerson1()
	{
		return contactPerson1;
	}

	public void setContactPerson1(String contactPerson1)
	{
		this.contactPerson1 = contactPerson1;
	}

	public String getMobileNumber1()
	{
		return mobileNumber1;
	}

	public void setMobileNumber1(String mobileNumber1)
	{
		this.mobileNumber1 = mobileNumber1;
	}

	public String getEmailAddress1()
	{
		return emailAddress1;
	}

	public void setEmailAddress1(String emailAddress1)
	{
		this.emailAddress1 = emailAddress1;
	}

	public String getContactPerson2()
	{
		return contactPerson2;
	}

	public void setContactPerson2(String contactPerson2)
	{
		this.contactPerson2 = contactPerson2;
	}

	public String getMobileNumber2()
	{
		return mobileNumber2;
	}

	public void setMobileNumber2(String mobileNumber2)
	{
		this.mobileNumber2 = mobileNumber2;
	}

	public String getEmailAddress2()
	{
		return emailAddress2;
	}

	public void setEmailAddress2(String emailAddress2)
	{
		this.emailAddress2 = emailAddress2;
	}

	public int getStatus()
	{
		return status;
	}

	public void setStatus(int status)
	{
		this.status = status;
	}

	public String getLogo()
	{
		return logo;
	}

	public void setLogo(String logo)
	{
		this.logo = logo;
	}

	public String getAnnouncement()
	{
		return announcement;
	}

	public void setAnnouncement(String announcement)
	{
		this.announcement = announcement;
	}

	public Date getValidUpto()
	{
		return validUpto;
	}

	public void setValidUpto(Date validUpto)
	{
		this.validUpto = validUpto;
	}
}
