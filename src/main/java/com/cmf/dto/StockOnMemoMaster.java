package com.cmf.dto;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import org.springframework.format.annotation.DateTimeFormat;

/**
 * The Class Branch.
 */
@Entity
@Table(name = "StockOnMemoMaster")
public class StockOnMemoMaster extends CreatedInfo implements Serializable
{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The id. */
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;

	/** The entity id. */
	@NotNull
	private long branchId;

	@NotNull
	private String stockOnMemoId;

	@NotNull
	private long brokerId;

	public int getTermsDays()
	{
		return termsDays;
	}

	public void setTermsDays(int termsDays)
	{
		this.termsDays = termsDays;
	}

	public Date getDueDate()
	{
		return dueDate;
	}

	public void setDueDate(Date dueDate)
	{
		this.dueDate = dueDate;
	}

	@NotNull
	private int termsDays;

	@NotNull
	@Temporal(TemporalType.DATE)
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date memoDate;

	@NotNull
	@Temporal(TemporalType.DATE)
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date dueDate;

	public long getId()
	{
		return id;
	}

	public void setId(long id)
	{
		this.id = id;
	}

	public long getBranchId()
	{
		return branchId;
	}

	public void setBranchId(long branchId)
	{
		this.branchId = branchId;
	}

	public String getStockOnMemoId()
	{
		return stockOnMemoId;
	}

	public void setStockOnMemoId(String stockOnMemoId)
	{
		this.stockOnMemoId = stockOnMemoId;
	}

	public long getBrokerId()
	{
		return brokerId;
	}

	public void setBrokerId(long brokerId)
	{
		this.brokerId = brokerId;
	}

	public Date getMemoDate()
	{
		return memoDate;
	}

	public void setMemoDate(Date memoDate)
	{
		this.memoDate = memoDate;
	}
}
