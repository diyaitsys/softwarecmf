/*
 * 
 */
package com.cmf.dto;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import org.springframework.format.annotation.DateTimeFormat;

/**
 * The Class Broker.
 */
@Entity
@Table(name = "payment")
public class Payment extends CreatedInfo implements Serializable
{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The id. */
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;

	/** The branch id. */
	@NotNull
	private long branchId;

	/** The name. */
	@NotNull
	private long purchaseId = 0;

	private long saleId = 0;

	@NotNull
	@Temporal(TemporalType.DATE)
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date paymentDate;

	private String paymentMode;

	private double paymentAmount;

	private String remarks;

	private String paymentFor;// (broker,purchase)

	public long getSaleId()
	{
		return saleId;
	}

	public void setSaleId(long saleId)
	{
		this.saleId = saleId;
	}

	public double getPaymentAmount()
	{
		return paymentAmount;
	}

	public void setPaymentAmount(double paymentAmount)
	{
		this.paymentAmount = paymentAmount;
	}

	public long getId()
	{
		return id;
	}

	public void setId(long id)
	{
		this.id = id;
	}

	public long getBranchId()
	{
		return branchId;
	}

	public void setBranchId(long branchId)
	{
		this.branchId = branchId;
	}

	public long getPurchaseId()
	{
		return purchaseId;
	}

	public void setPurchaseId(long purchaseId)
	{
		this.purchaseId = purchaseId;
	}

	public Date getPaymentDate()
	{
		return paymentDate;
	}

	public void setPaymentDate(Date paymentDate)
	{
		this.paymentDate = paymentDate;
	}

	public String getPaymentMode()
	{
		return paymentMode;
	}

	public void setPaymentMode(String paymentMode)
	{
		this.paymentMode = paymentMode;
	}

	public String getRemarks()
	{
		return remarks;
	}

	public void setRemarks(String remarks)
	{
		this.remarks = remarks;
	}

	public String getPaymentFor()
	{
		return paymentFor;
	}

	public void setPaymentFor(String paymentFor)
	{
		this.paymentFor = paymentFor;
	}
}
