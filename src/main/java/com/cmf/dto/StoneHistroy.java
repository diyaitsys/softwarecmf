package com.cmf.dto;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Table(name = "StoneHistroy")
public class StoneHistroy extends CreatedInfo implements Serializable
{
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	/** The id. */
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;

	@NotNull
	private long stockId;

	@NotNull
	private long fromBranchId;

	@NotNull
	private long toBranchId;

	@NotNull
	private long entityId;

	@NotNull
	@Temporal(TemporalType.DATE)
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date histroyDate;

	private String remarks;

	public long getId()
	{
		return id;
	}

	public void setId(long id)
	{
		this.id = id;
	}

	public long getStockId()
	{
		return stockId;
	}

	public void setStockId(long stockId)
	{
		this.stockId = stockId;
	}

	public long getFromBranchId()
	{
		return fromBranchId;
	}

	public void setFromBranchId(long fromBranchId)
	{
		this.fromBranchId = fromBranchId;
	}

	public long getToBranchId()
	{
		return toBranchId;
	}

	public void setToBranchId(long toBranchId)
	{
		this.toBranchId = toBranchId;
	}

	public Date getHistroyDate()
	{
		return histroyDate;
	}

	public void setHistroyDate(Date histroyDate)
	{
		this.histroyDate = histroyDate;
	}

	public String getRemarks()
	{
		return remarks;
	}

	public void setRemarks(String remarks)
	{
		this.remarks = remarks;
	}

	public long getEntityId()
	{
		return entityId;
	}

	public void setEntityId(long entityId)
	{
		this.entityId = entityId;
	}
}
