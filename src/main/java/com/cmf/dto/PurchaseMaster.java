package com.cmf.dto;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import org.springframework.format.annotation.DateTimeFormat;

/**
 * The Class Branch.
 */
@Entity
@Table(name = "PurchaseMaster")
public class PurchaseMaster extends CreatedInfo implements Serializable
{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	/** The id. */
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;

	@NotNull
	private String invoiceNo;

	@NotNull
	private long partyId;

	@NotNull
	private long branchId;

	@NotNull
	@Temporal(TemporalType.DATE)
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date purchaseDate;

	/** The other details. */
	private long brokerId;

	private double brokeragePercentage;

	private double brokerageAmount;

	@NotNull
	private double conversionRate;

	@NotNull
	private String buyCurrencyUnit;

	@NotNull
	private String myCurrencyUnit;

	@NotNull
	private double netPriceBuyCurrency;

	@NotNull
	private double netPriceMyCurrency;

	@NotNull
	private int taxDetails;

	private double IGST = 0.0;

	private double SGST = 0.0;

	private double CGST = 0.0;

	@NotNull
	private double billAmountBuyCurrency;

	@NotNull
	private double billAmountMyCurrency;

	@NotNull
	private int termsDays;

	@NotNull
	@Temporal(TemporalType.DATE)
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date dueDate;

	private String remarks;

	/** The status. */
	@Column(name = "status", columnDefinition = "int default 1")
	private int status = 1;

	/** The entity id. */
	@NotNull
	private long entityId;

	public long getId()
	{
		return id;
	}

	public void setId(long id)
	{
		this.id = id;
	}

	public String getInvoiceNo()
	{
		return invoiceNo;
	}

	public void setInvoiceNo(String invoiceNo)
	{
		this.invoiceNo = invoiceNo;
	}

	public long getPartyId()
	{
		return partyId;
	}

	public void setPartyId(long partyId)
	{
		this.partyId = partyId;
	}

	public long getBranchId()
	{
		return branchId;
	}

	public void setBranchId(long branchId)
	{
		this.branchId = branchId;
	}

	public Date getPurchaseDate()
	{
		return purchaseDate;
	}

	public void setPurchaseDate(Date purchaseDate)
	{
		this.purchaseDate = purchaseDate;
	}

	public long getBrokerId()
	{
		return brokerId;
	}

	public void setBrokerId(long brokerId)
	{
		this.brokerId = brokerId;
	}

	public double getBrokeragePercentage()
	{
		return brokeragePercentage;
	}

	public void setBrokeragePercentage(double brokeragePercentage)
	{
		this.brokeragePercentage = brokeragePercentage;
	}

	public double getBrokerageAmount()
	{
		return brokerageAmount;
	}

	public void setBrokerageAmount(double brokerageAmount)
	{
		this.brokerageAmount = brokerageAmount;
	}

	public double getConversionRate()
	{
		return conversionRate;
	}

	public void setConversionRate(double conversionRate)
	{
		this.conversionRate = conversionRate;
	}

	public String getBuyCurrencyUnit()
	{
		return buyCurrencyUnit;
	}

	public void setBuyCurrencyUnit(String buyCurrencyUnit)
	{
		this.buyCurrencyUnit = buyCurrencyUnit;
	}

	public String getMyCurrencyUnit()
	{
		return myCurrencyUnit;
	}

	public void setMyCurrencyUnit(String myCurrencyUnit)
	{
		this.myCurrencyUnit = myCurrencyUnit;
	}

	public double getNetPriceBuyCurrency()
	{
		return netPriceBuyCurrency;
	}

	public void setNetPriceBuyCurrency(double netPriceBuyCurrency)
	{
		this.netPriceBuyCurrency = netPriceBuyCurrency;
	}

	public double getNetPriceMyCurrency()
	{
		return netPriceMyCurrency;
	}

	public void setNetPriceMyCurrency(double netPriceMyCurrency)
	{
		this.netPriceMyCurrency = netPriceMyCurrency;
	}

	public int getTaxDetails()
	{
		return taxDetails;
	}

	public void setTaxDetails(int taxDetails)
	{
		this.taxDetails = taxDetails;
	}

	public double getIGST()
	{
		return IGST;
	}

	public void setIGST(double iGST)
	{
		IGST = iGST;
	}

	public double getSGST()
	{
		return SGST;
	}

	public void setSGST(double sGST)
	{
		SGST = sGST;
	}

	public double getCGST()
	{
		return CGST;
	}

	public void setCGST(double cGST)
	{
		CGST = cGST;
	}

	public double getBillAmountBuyCurrency()
	{
		return billAmountBuyCurrency;
	}

	public void setBillAmountBuyCurrency(double billAmountBuyCurrency)
	{
		this.billAmountBuyCurrency = billAmountBuyCurrency;
	}

	public double getBillAmountMyCurrency()
	{
		return billAmountMyCurrency;
	}

	public void setBillAmountMyCurrency(double billAmountMyCurrency)
	{
		this.billAmountMyCurrency = billAmountMyCurrency;
	}

	public int getTermsDays()
	{
		return termsDays;
	}

	public void setTermsDays(int termsDays)
	{
		this.termsDays = termsDays;
	}

	public Date getDueDate()
	{
		return dueDate;
	}

	public void setDueDate(Date dueDate)
	{
		this.dueDate = dueDate;
	}

	public String getRemarks()
	{
		return remarks;
	}

	public void setRemarks(String remarks)
	{
		this.remarks = remarks;
	}

	public int getStatus()
	{
		return status;
	}

	public void setStatus(int status)
	{
		this.status = status;
	}

	public long getEntityId()
	{
		return entityId;
	}

	public void setEntityId(long entityId)
	{
		this.entityId = entityId;
	}
}
