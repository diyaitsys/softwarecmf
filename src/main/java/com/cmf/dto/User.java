/**
 * 
 */
package com.cmf.dto;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 * The Class User.
 *
 * @author manthan.upadhyay
 */
@Entity
@Table(name = "User")
public class User extends CreatedInfo implements Serializable
{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The id. */
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;

	/** The branch id. */
	@NotNull
	private long branchId;

	@NotNull
	private long entityId;

	/**
	 * @return the entityId
	 */
	public long getEntityId()
	{
		return entityId;
	}

	/**
	 * @param entityId the entityId to set
	 */
	public void setEntityId(long entityId)
	{
		this.entityId = entityId;
	}

	/** The first name. */
	@NotNull
	private String firstName;

	/** The middle name. */
	private String middleName;

	/** The last name. */
	@NotNull
	private String lastName;

	/** The email address. */
	@NotNull
	private String emailAddress;

	/** The password. */
	@NotNull
	private String password;

	/** The employee role. */
	@NotNull
	private String employeeRole;

	/** The date of birth. */
	private Date dateOfBirth;

	/** The date of joining. */
	private Date dateOfJoining;

	/** The mobile no. */
	private String mobileNo;

	/** The alt mobile no. */
	private String altMobileNo;

	/** The address. */
	private String address;

	/** The pan no. */
	private String panNo;

	/** The last login date. */
	private Date lastLoginDate;

	/** The status. */
	@NotNull
	@Column(name = "status", columnDefinition = "int default 1")
	private int status = 1;

	public long getId()
	{
		return id;
	}

	public void setId(long id)
	{
		this.id = id;
	}

	public String getFirstName()
	{
		return firstName;
	}

	/**
	 * @return the branchId
	 */
	public long getBranchId()
	{
		return branchId;
	}

	/**
	 * @param branchId the branchId to set
	 */
	public void setBranchId(long branchId)
	{
		this.branchId = branchId;
	}

	public void setFirstName(String firstName)
	{
		this.firstName = firstName;
	}

	public String getMiddleName()
	{
		return middleName;
	}

	public void setMiddleName(String middleName)
	{
		this.middleName = middleName;
	}

	public String getLastName()
	{
		return lastName;
	}

	public void setLastName(String lastName)
	{
		this.lastName = lastName;
	}

	public String getEmailAddress()
	{
		return emailAddress;
	}

	public void setEmailAddress(String emailAddress)
	{
		this.emailAddress = emailAddress;
	}

	public String getPassword()
	{
		return password;
	}

	public void setPassword(String password)
	{
		this.password = password;
	}

	public String getEmployeeRole()
	{
		return employeeRole;
	}

	public void setEmployeeRole(String employeeRole)
	{
		this.employeeRole = employeeRole;
	}

	public Date getDateOfBirth()
	{
		return dateOfBirth;
	}

	public void setDateOfBirth(Date dateOfBirth)
	{
		this.dateOfBirth = dateOfBirth;
	}

	public Date getDateOfJoining()
	{
		return dateOfJoining;
	}

	public void setDateOfJoining(Date dateOfJoining)
	{
		this.dateOfJoining = dateOfJoining;
	}

	public String getMobileNo()
	{
		return mobileNo;
	}

	public void setMobileNo(String mobileNo)
	{
		this.mobileNo = mobileNo;
	}

	public String getAltMobileNo()
	{
		return altMobileNo;
	}

	public void setAltMobileNo(String altMobileNo)
	{
		this.altMobileNo = altMobileNo;
	}

	public String getAddress()
	{
		return address;
	}

	public void setAddress(String address)
	{
		this.address = address;
	}

	public String getPanNo()
	{
		return panNo;
	}

	public void setPanNo(String panNo)
	{
		this.panNo = panNo;
	}

	public Date getLastLoginDate()
	{
		return lastLoginDate;
	}

	public void setLastLoginDate(Date lastLoginDate)
	{
		this.lastLoginDate = lastLoginDate;
	}

	public int getStatus()
	{
		return status;
	}

	public void setStatus(int status)
	{
		this.status = status;
	}
}
