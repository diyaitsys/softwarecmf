package com.cmf.dto;

import java.io.Serializable;
import java.util.List;

public class InvoiceMaster implements Serializable
{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	private PurchaseMaster purchaseMaster;
	private List<Stock> stockLst;
	private SalesMaster salesMaster;

	public PurchaseMaster getPurchaseMaster()
	{
		return purchaseMaster;
	}

	public void setPurchaseMaster(PurchaseMaster purchaseMaster)
	{
		this.purchaseMaster = purchaseMaster;
	}

	public List<Stock> getStockLst()
	{
		return stockLst;
	}

	public void setStockLst(List<Stock> stockLst)
	{
		this.stockLst = stockLst;
	}

	public SalesMaster getSalesMaster()
	{
		return salesMaster;
	}

	public void setSalesMaster(SalesMaster salesMaster)
	{
		this.salesMaster = salesMaster;
	}

}
