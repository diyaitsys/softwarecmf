/**
 * 
 */
package com.cmf.dto;

import java.util.Date;
import javax.persistence.Column;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @author manthan.upadhyay
 */
public class CreatedInfo
{
	@Autowired
	HttpServletRequest httpServletRequest;

	/** The created at. */
	@Column(name = "createdAt", updatable = false)
	protected Date createdAt;

	/** The created by. */
	@Column(name = "createdBy", updatable = false)
	protected long createdBy;

	/** The modified by. */
	protected long modifiedBy;

	/** The modofied date. */
	protected Date modifiedDate;

	public Date getCreatedAt()
	{
		return createdAt;
	}

	public void setCreatedAt(Date createdAt)
	{
		this.createdAt = createdAt;
	}

	public long getCreatedBy()
	{
		return createdBy;
	}

	public void setCreatedBy(long createdBy)
	{
		this.createdBy = createdBy;
	}

	public long getModifiedBy()
	{
		return modifiedBy;
	}

	public void setModifiedBy(long modifiedBy)
	{
		this.modifiedBy = modifiedBy;
	}

	public Date getModifiedDate()
	{
		return modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate)
	{
		this.modifiedDate = modifiedDate;
	}
}
