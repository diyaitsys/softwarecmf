package com.cmf.dto;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import org.springframework.format.annotation.DateTimeFormat;

/**
 * The Class StockOnLab.
 */
@Entity
@Table(name = "StockOnLab")
public class StockOnLab extends CreatedInfo implements Serializable
{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The id. */
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;

	/** The entity id. */
	@NotNull
	private long branchId;

	@NotNull
	private long stockId;

	@NotNull
	private long labId;

	@NotNull
	@Temporal(TemporalType.DATE)
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date labDate;

	public long getId()
	{
		return id;
	}

	public void setId(long id)
	{
		this.id = id;
	}

	public long getBranchId()
	{
		return branchId;
	}

	public void setBranchId(long branchId)
	{
		this.branchId = branchId;
	}

	public long getStockId()
	{
		return stockId;
	}

	public void setStockId(long stockId)
	{
		this.stockId = stockId;
	}

	/**
	 * @return the labId
	 */
	public long getLabId()
	{
		return labId;
	}

	/**
	 * @param labId the labId to set
	 */
	public void setLabId(long labId)
	{
		this.labId = labId;
	}

	/**
	 * @return the labDate
	 */
	public Date getLabDate()
	{
		return labDate;
	}

	/**
	 * @param labDate the labDate to set
	 */
	public void setLabDate(Date labDate)
	{
		this.labDate = labDate;
	}

}
