package com.cmf.dto;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 * The Class Branch.
 */
@Entity
@Table(name = "branch")
public class Branch extends CreatedInfo implements Serializable
{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The id. */
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;

	/** The name. */
	@NotNull
	private String name;

	/** The address. */
	private String address;

	/** The other details. */
	private String otherDetails;

	/** The status. */
	@Column(name = "status", columnDefinition = "int default 1")
	private int status = 1;

	/** The entity id. */
	@NotNull
	private long entityId;

	/**
	 * @return the entityId
	 */
	public long getEntityId()
	{
		return entityId;
	}

	/**
	 * @param entityId the entityId to set
	 */
	public void setEntityId(long entityId)
	{
		this.entityId = entityId;
	}

	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	public long getId()
	{
		return id;
	}

	/**
	 * Sets the id.
	 *
	 * @param id the new id
	 */
	public void setId(long id)
	{
		this.id = id;
	}

	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	public String getName()
	{
		return name;
	}

	/**
	 * Sets the name.
	 *
	 * @param name the new name
	 */
	public void setName(String name)
	{
		this.name = name;
	}

	/**
	 * Gets the address.
	 *
	 * @return the address
	 */
	public String getAddress()
	{
		return address;
	}

	/**
	 * Sets the address.
	 *
	 * @param address the new address
	 */
	public void setAddress(String address)
	{
		this.address = address;
	}

	/**
	 * Gets the other details.
	 *
	 * @return the other details
	 */
	public String getOtherDetails()
	{
		return otherDetails;
	}

	/**
	 * Sets the other details.
	 *
	 * @param otherDetails the new other details
	 */
	public void setOtherDetails(String otherDetails)
	{
		this.otherDetails = otherDetails;
	}

	/**
	 * @return the status
	 */
	public int getStatus()
	{
		return status;
	}

	/**
	 * @param status the status to set
	 */
	public void setStatus(int status)
	{
		this.status = status;
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString()
	{
		return "Branch [id=" + id + ", name=" + name + ", address=" + address + ", otherDetails=" + otherDetails + ", status=" + status + ", entityId=" + entityId + "]";
	}

}
