package com.cmf.filter;

import java.io.IOException;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import com.cmf.utils.Constants;

@WebFilter(urlPatterns = {"/*"}, description = "Session Checker Filter")
public class SessionCheckFilter implements Filter
{
	private final String ENTITY_LOGIN_PAGE = "/index";

	private static final Set<String> ALLOWED_PATHS = Collections.unmodifiableSet(new HashSet<>(
			Arrays.asList("/index", "/logout", "/checkEntityCode", "/assets/", "/images/", "/favicon.ico/",
					"/swagger", "/swagger", "/webjars", "/v2", "/purchase")));

	@Override
	public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain) throws IOException, ServletException
	{
		// System.out.println("In Side Filter=================>>>>>>>>>>>>>>>>>>>>>>");

		HttpServletRequest request = (HttpServletRequest) req;
		HttpServletResponse response = (HttpServletResponse) res;
		HttpSession session = request.getSession(false);
		String path = request.getRequestURI().substring(request.getContextPath().length()).replaceAll("[/]+$", "");
		// System.out.println("PATH : >>>>>>>>>>>>>>>>>>>>>>>>> " + path);
		boolean returnToIndexPage = false;

		// boolean loggedIn = (session != null && session.getAttribute("Id") != null);
		// boolean allowedPath = ALLOWED_PATHS.contains(path);
		boolean allowedPath = false;
		for (String s : ALLOWED_PATHS)
		{
			if (path.contains(s))
			{
				allowedPath = true;
				break;
			}
		}

		if (allowedPath)
		{
			chain.doFilter(req, res);
		}
		else
		{
			if (session == null || session.getAttribute(Constants.ENTITY_SESSION) == null)
			{
				returnToIndexPage = true;
				response.sendRedirect(request.getContextPath() + ENTITY_LOGIN_PAGE);
				return;
			}
			if (!path.contains("checkUserLogin"))
			{
				if (session.getAttribute(Constants.USER_SESSION) == null)
				{
					response.sendRedirect(request.getContextPath() + ENTITY_LOGIN_PAGE);
					returnToIndexPage = true;
				}
				else
				{
					chain.doFilter(req, res);
				}
			}
			else
			{
				if (session.getAttribute(Constants.ENTITY_SESSION) == null)
				{
					response.sendRedirect(request.getContextPath() + ENTITY_LOGIN_PAGE);
					returnToIndexPage = true;
				}
				else
				{
					chain.doFilter(req, res);
				}
			}
		}

		if (returnToIndexPage)
		{
			response.sendRedirect(request.getContextPath() + ENTITY_LOGIN_PAGE);
		}
	}

	@Override
	public void destroy()
	{
		// TODO Auto-generated method stub

	}

	@Override
	public void init(FilterConfig arg0) throws ServletException
	{
		// TODO Auto-generated method stub

	}

}
