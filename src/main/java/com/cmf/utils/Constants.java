package com.cmf.utils;

import javax.servlet.http.HttpServletRequest;
import com.cmf.dto.Branch;
import com.cmf.dto.EntityMaster;
import com.cmf.dto.User;

public class Constants
{
	public static final String ENTITY_SESSION = "ENTITY_SESSION";
	public static final String USER_SESSION = "USER_SESSION";
	public static final String BRANCH_SESSION = "BRANCH_SESSION";

	public static final String SUPER_ADMIN = "SUPER_ADMIN";

	public static final String ENTITY_ADMIN = "ENTITY_ADMIN";

	public static final String BRANCH_ADMIN = "BRANCH_ADMIN";

	public static final String REPORT_ADMIN = "REPORT_ADMIN";

	public static final String STOCK_ADMIN = "STOCK_ADMIN";

	public static final int STATUS_ACTIVE = 1;
	public static final int STATUS_INACTIVE = 2;

	public static long getSessionBranchId(HttpServletRequest httpServletRequest)
	{
		return ((Branch) httpServletRequest.getSession(false).getAttribute(BRANCH_SESSION)).getId();
	}

	public static long getSessionUserId(HttpServletRequest httpServletRequest)
	{
		return ((User) httpServletRequest.getSession(false).getAttribute(USER_SESSION)).getId();
	}

	public static long getSessionEntityId(HttpServletRequest httpServletRequest)
	{
		return ((EntityMaster) httpServletRequest.getSession(false).getAttribute(ENTITY_SESSION)).getId();
	}
}
